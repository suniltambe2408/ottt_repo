﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using OTTTBO;
using OTTTDAL;
using Common;

#endregion " Additional Namespaces "

namespace OTTTBLL
{
    public class MachineMasterBLL
    {
        #region " Class Level Variable "

        int iDBOperationCode = 0;
        int MachineNumber = 0;
        MachineMasterDAL oMachineMasterDAL = null;

        List<MachineMasterBO> lstMachineMaster = null;
        List<MonitoringBO> lstMonitoringBO = null;


        #endregion " Class Level Variable "

        #region " Public Functions "

        /// -------------------------------------------------------
        /// <summary>
        /// Inserts Machine No, Machine Name, Machine Type, Room Name
        /// </summary>
        /// <param name=""></param>
        /// <returns>DB Operation status Code</returns>
        /// -------------------------------------------------------

        public int SaveMachine(MachineMasterBO oMachineMasterBO)
        {
            oMachineMasterDAL = new MachineMasterDAL();

            try
            {
                iDBOperationCode = oMachineMasterDAL.SaveMachine(oMachineMasterBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oMachineMasterDAL = null;
            }
            return iDBOperationCode;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Gets all Machine
        /// </summary>
        /// <returns>List of Machines</returns>
        /// -------------------------------------------------------

        public List<MachineMasterBO> GetAllMachineDetails()
        {
            oMachineMasterDAL = new MachineMasterDAL();
            lstMachineMaster = new List<MachineMasterBO>();

            try
            {
                lstMachineMaster = oMachineMasterDAL.GetAllMachineDetails();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oMachineMasterDAL = null;
            }
            return lstMachineMaster;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Search machine based on LIMS_NO
        /// </summary>
        /// <returns>Single record</returns>
        /// -------------------------------------------------------

        public List<MachineMasterBO> SearchMachine(string Keyword)
        {
            oMachineMasterDAL = new MachineMasterDAL();
            lstMachineMaster = new List<MachineMasterBO>();

            try
            {
                lstMachineMaster = oMachineMasterDAL.SearchMachine(Keyword);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oMachineMasterDAL = null;
            }
            return lstMachineMaster;
        }
        
        public int GetMachineNoByLimsNo(string LIMSNO)
        {
            oMachineMasterDAL = new MachineMasterDAL();


            try
            {
                MachineNumber = oMachineMasterDAL.GetMachineNoByLimsNo(LIMSNO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oMachineMasterDAL = null;
            }
            return MachineNumber;
        }


        public int CheckDuplicateMachineNO(string MachineNoLong)
        {
            oMachineMasterDAL = new MachineMasterDAL();


            try
            {
                MachineNumber = oMachineMasterDAL.CheckDuplicateMachineNO(MachineNoLong);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {

            }
            return MachineNumber;
        }

        public List<MonitoringBO> GetAllMachineNumbers()
        {
            lstMonitoringBO = new List<MonitoringBO>();
            oMachineMasterDAL = new MachineMasterDAL();
            try
            {
                lstMonitoringBO = oMachineMasterDAL.GetAllMachineNumbers();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
            return lstMonitoringBO;
        }

        #endregion
    }
}
