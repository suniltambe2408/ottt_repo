﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using OTTTBO;
using OTTTDAL;
using Common;

#endregion " Additional Namespaces "

namespace OTTTBLL
{
   public class AllMachineTestReportBLL
    {

        #region " Class Level Variable "

        int iDBOperationCode = 0;
        AllMachineTestReportDAL oAllMachineTestReportDAL = null;
        List<AllMachineReportBO> lstAllMachineReportBO = null;
        List<MachineMasterBO> lstMachineMasterBO = null;

        #endregion " Class Level Variable "

        #region " Public Functions "

        /// -------------------------------------------------------
        /// <summary>
        /// Get Distinct Machine Numbers
        /// </summary>
        /// <returns>List of Machines</returns>
        /// -------------------------------------------------------
        public List<MachineMasterBO> GetDistinctMachineNumbers()
        {
            lstMachineMasterBO = new List<MachineMasterBO>();
            oAllMachineTestReportDAL = new AllMachineTestReportDAL();

            try
            {
                lstMachineMasterBO = oAllMachineTestReportDAL.GetDistinctMachineNumbers();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oAllMachineTestReportDAL = null;
            }
            return lstMachineMasterBO;

        }

        /// -------------------------------------------------------
        /// <summary>
        /// Get Distinct Test Owner Name
        /// </summary>
        /// <returns>List of Machines</returns>
        /// -------------------------------------------------------
        public List<AllMachineReportBO> GetDistinctTestOwnerName()
        {
            lstAllMachineReportBO = new List<AllMachineReportBO>();
            oAllMachineTestReportDAL = new AllMachineTestReportDAL();

            try
            {
                lstAllMachineReportBO = oAllMachineTestReportDAL.GetDistinctTestOwnerName();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oAllMachineTestReportDAL = null;
            }
            return lstAllMachineReportBO;

        }

        /// -------------------------------------------------------
        /// <summary>
        /// Get Distinct Machine Status
        /// </summary>
        /// <returns>List of Machines</returns>
        /// -------------------------------------------------------
        public List<AllMachineReportBO> GetDistinctMachineStatus()
        {
            lstAllMachineReportBO = new List<AllMachineReportBO>();
            oAllMachineTestReportDAL = new AllMachineTestReportDAL();

            try
            {
                lstAllMachineReportBO = oAllMachineTestReportDAL.GetDistinctMachineStatus();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oAllMachineTestReportDAL = null;
            }
            return lstAllMachineReportBO;

        }

        /// -------------------------------------------------------
        /// <summary>
        /// Get Distinct Department
        /// </summary>
        /// <returns>List of Machines</returns>
        /// -------------------------------------------------------
        public List<AllMachineReportBO> GetDistinctDepartment()
        {
            lstAllMachineReportBO = new List<AllMachineReportBO>();
            oAllMachineTestReportDAL = new AllMachineTestReportDAL();

            try
            {
                lstAllMachineReportBO = oAllMachineTestReportDAL.GetDistinctDepartment();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oAllMachineTestReportDAL = null;
            }
            return lstAllMachineReportBO;

        }

       
        #endregion

    }
}
