﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using OTTTBO;
using OTTTDAL;
using Common;

#endregion " Additional Namespaces "

namespace OTTTBLL
{
    public class DashboardBLL
    {

        #region " Class Level Variable "

        int iDBOperationCode = 0;

        DashboardDAL oDashboardDAL = null;
        List<TestBO> lstTestBO = null;
        List<MachineStatusBO> lstMachineStatusBO = null;

        #endregion " Class Level Variable "

        #region " Public Functions "

        /// -------------------------------------------------------
        /// <summary>
        /// Gets all Recent Test data
        /// </summary>
        /// <returns>List of Recent Test</returns>
        /// -------------------------------------------------------

        public List<TestBO> GetAllRecentTest()
        {
            oDashboardDAL = new DashboardDAL();
            lstTestBO = new List<TestBO>();

            try
            {
                lstTestBO = oDashboardDAL.GetAllRecentTest();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                oDashboardDAL = null;
            }
            return lstTestBO;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Gets all Scheduled Test data
        /// </summary>
        /// <returns>List of Scheduled Test</returns>
        /// -------------------------------------------------------

        public List<TestBO> GetAllScheduledTest()
        {
            oDashboardDAL = new DashboardDAL();
            lstTestBO = new List<TestBO>();

            try
            {
                lstTestBO = oDashboardDAL.GetAllScheduledTest();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oDashboardDAL = null;
            }
            return lstTestBO;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Gets all Machine Status data
        /// </summary>
        /// <returns>List of Machine</returns>
        /// -------------------------------------------------------

        public List<MachineStatusBO> GetAllMachineStatus()
        {
            oDashboardDAL = new DashboardDAL();
            lstMachineStatusBO = new List<MachineStatusBO>();

            try
            {
                lstMachineStatusBO = oDashboardDAL.GetAllMachineStatus();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                oDashboardDAL = null;
            }
            return lstMachineStatusBO;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Update Scheduled status to Started based on TestID
        /// </summary>
        /// <returns></returns>
        /// -------------------------------------------------------

        public int UpdateStatusToStarted(int TestID)
        {
            oDashboardDAL = new DashboardDAL();

            try
            {
                iDBOperationCode = oDashboardDAL.UpdateStatusToStarted(TestID);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                oDashboardDAL = null;
            }
            return iDBOperationCode;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Update status to Complete based on TestID
        /// </summary>
        /// <returns></returns>
        /// -------------------------------------------------------

        public int UpdateStatusToComplete(int TestID)
        {
            oDashboardDAL = new DashboardDAL();

            try
            {
                iDBOperationCode = oDashboardDAL.UpdateStatusToComplete(TestID);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                oDashboardDAL = null;
            }
            return iDBOperationCode;
        }


        /// -------------------------------------------------------
        /// <summary>
        /// Update status to Pause based on TestID
        /// </summary>
        /// <returns></returns>
        /// -------------------------------------------------------

        public int UpdateStatusToPause(int TestID)
        {
            oDashboardDAL = new DashboardDAL();

            try
            {
                iDBOperationCode = oDashboardDAL.UpdateStatusToPause(TestID);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                oDashboardDAL = null;
            }
            return iDBOperationCode;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// This method update the scheduled test based on TestID
        /// </summary>
        /// <returns></returns>
        /// -------------------------------------------------------

        public int UpdateScheduledTest(TestBO oTestBO)
        {
            oDashboardDAL = new DashboardDAL();

            try
            {
                iDBOperationCode = oDashboardDAL.UpdateScheduledTest(oTestBO);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                oDashboardDAL = null;
            }
            return iDBOperationCode;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Delete the scheduled test based on TestID
        /// </summary>
        /// <returns></returns>
        /// -------------------------------------------------------

        public int StatusUpdateToDeleted(int TestID)
        {
            oDashboardDAL = new DashboardDAL();

            try
            {
                iDBOperationCode = oDashboardDAL.StatusUpdateToDeleted(TestID);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                oDashboardDAL = null;
            }
            return iDBOperationCode;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Update status to AboutToComplete based on TestID
        /// </summary>
        /// <returns></returns>
        /// -------------------------------------------------------

        public int UpdateStatusToAboutToComplete()
        {
            oDashboardDAL = new DashboardDAL();

            try
            {
                iDBOperationCode = oDashboardDAL.UpdateStatusToAboutToComplete();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                oDashboardDAL = null;
            }
            return iDBOperationCode;
        }

        #endregion

    }
}
