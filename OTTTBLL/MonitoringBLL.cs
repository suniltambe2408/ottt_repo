﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using OTTTBO;
using OTTTDAL;
using Common;

#endregion " Additional Namespaces "

namespace OTTTBLL
{
    public class MonitoringBLL
    {
        #region Class Level Variable

        List<MonitoringDataBO> lstMonitoringDataBO = null;
        MonitoringDAL oMonitoringDAL = null;
        DataSet dsDataSet = null;
        //List<MachineMasterBO> lstMachineMasterBO = null;
        #endregion

        #region Public Functions

        public List<MonitoringDataBO> GetAllGraphData(string sFieldValues)
        {
            lstMonitoringDataBO = new List<MonitoringDataBO>();
            oMonitoringDAL = new MonitoringDAL();
            dsDataSet = new DataSet();

            try
            {
                lstMonitoringDataBO = oMonitoringDAL.GetAllGraphData(sFieldValues);
               // dsDataSet = oMonitoringDAL.GetAllGraphData();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            { 

            }
            return lstMonitoringDataBO;
          //  return dsDataSet;
        }

       

        #endregion
    }
}
