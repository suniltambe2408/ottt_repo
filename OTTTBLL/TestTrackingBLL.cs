﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using OTTTBO;
using OTTTDAL;
using Common;

#endregion " Additional Namespaces ";

namespace OTTTBLL
{
    public class TestTrackingBLL
    {
        #region " Class Level Variable "

        int iDBOperationCode = 0;
        TestTrackingDAL oTestTrackingDAL = null;

        List<TestBO> lstAddNewTestBO = null;

        #endregion " Class Level Variable "

        #region " Public Functions "

        /// -------------------------------------------------------
        /// <summary>
        /// Search Test Data based on LIMS NO
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        /// -------------------------------------------------------

        public List<TestBO> SearchLimsData(string LIMSNO)
        {
            oTestTrackingDAL = new TestTrackingDAL();
            lstAddNewTestBO = new List<TestBO>();

            try
            {
                lstAddNewTestBO = oTestTrackingDAL.SearchLimsData(LIMSNO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oTestTrackingDAL = null;
            }
            return lstAddNewTestBO;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Update selected grid view data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------    

        public int UpdateTestInformation(TestBO oTestBO)
        {
            oTestTrackingDAL = new TestTrackingDAL();

            try
            {
                iDBOperationCode = oTestTrackingDAL.UpdateTestInformation(oTestBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oTestTrackingDAL = null;
            }
            return iDBOperationCode;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Update status Deleted based on TestID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------    

        public int StatusUpdateToDeleted(int TestID)
        {
            oTestTrackingDAL = new TestTrackingDAL();

            try
            {
                iDBOperationCode = oTestTrackingDAL.StatusUpdateToDeleted(TestID);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                oTestTrackingDAL = null;
            }
            return iDBOperationCode;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This method check duplicate LIMS Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///--------------------------------------------------------------------- 

        public string CheckDuplicateLIMSNO(string LIMSNO)
        {
            oTestTrackingDAL = new TestTrackingDAL();
            string LIMSNumber;

            try
            {
                LIMSNumber = oTestTrackingDAL.CheckDuplicateLIMSNO(LIMSNO);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {

            }
            return LIMSNumber;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Get count of Tests against given LIMS Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///--------------------------------------------------------------------- 

        public int GetTestCountForLIMSNo(string LIMSNO)
        {
            oTestTrackingDAL = new TestTrackingDAL();
            int count = 0;

            try
            {
                count = oTestTrackingDAL.GetTestCountForLIMSNo(LIMSNO);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {

            }
            return count;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Search Test Data based on LIMS NO
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        /// -------------------------------------------------------

        public List<TestBO> GetTestIdListForLimsNumber(string LimsNumber)
        {
            oTestTrackingDAL = new TestTrackingDAL();
            lstAddNewTestBO = new List<TestBO>();

            try
            {
                lstAddNewTestBO = oTestTrackingDAL.GetTestIdListForLimsNumber(LimsNumber);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oTestTrackingDAL = null;
            }
            return lstAddNewTestBO;
        }

        #endregion
    }
}
