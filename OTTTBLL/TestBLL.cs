﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using OTTTBO;
using OTTTDAL;
using Common;

#endregion " Additional Namespaces "

namespace OTTTBLL
{
    public class TestBLL
    {
        #region " Class Level Variable "

        int iDBOperationCode = 0;
        TestDAL oTestDAL = null;
        CommonFunctions oCommonFunctions = null;
        List<MachineMasterBO> lstMachineMasterBO = null;
        List<TestBO> lstTestBO = null;
        List<CostCenterBO> lstCostCenterBO = null;

        #endregion " Class Level Variable "

        #region " Public Functions "

        /// -------------------------------------------------------
        /// <summary>
        /// Get All Machine NO & Machine Name For DropDoqnList
        /// </summary>
        /// <returns>List of Machines</returns>
        /// -------------------------------------------------------
        
        public List<MachineMasterBO> GetAllMachineName()
        {
            lstMachineMasterBO = new List<MachineMasterBO>();
            oCommonFunctions = new CommonFunctions();

            try
            {
                lstMachineMasterBO = oCommonFunctions.GetAllMachineName();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oCommonFunctions = null;
            }
            return lstMachineMasterBO;

        }

        /// -------------------------------------------------------
        /// <summary>
        /// Inserts Machine No, Machine Name, Machine Type, Room Name
        /// </summary>
        /// <param name=""></param>
        /// <returns>DB Operation status Code</returns>
        /// -------------------------------------------------------

        public int SaveNewTest(TestBO oTestBO)
        {
            oTestDAL = new TestDAL();

            try
            {
                iDBOperationCode = oTestDAL.SaveNewTest(oTestBO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oTestDAL = null;
            }
            return iDBOperationCode;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Get Machine Number list from its test status
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        /// -------------------------------------------------------

        public List<TestBO> GetAllMachineNOByStatus(int TestID)
        {
            lstTestBO = new List<TestBO>();
            oTestDAL=new TestDAL();

            try
            {
                lstTestBO = oTestDAL.GetAllMachineNOByStatus(TestID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oTestDAL = null;
            }
            return lstTestBO;

        }

        /// -------------------------------------------------------
        /// <summary>
        /// Get Count of Tests running in Machine
        /// </summary>
        /// <returns>Count</returns>
        /// -------------------------------------------------------

        public int GetTestCountForMachineNumber(int MachineNumber)
        {
            int TestCount = 0;
            oTestDAL = new TestDAL();
            try
            {
                TestCount = oTestDAL.GetTestCountForMachineNumber(MachineNumber);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oTestDAL = null;
            }
            return TestCount;

        }

        /// -------------------------------------------------------
        /// <summary>
        /// Set status of All Tests running in Machine as FAULT
        /// </summary>
        /// <returns>int</returns>
        /// -------------------------------------------------------

        public int MachineFaultOccurred(int MachineNumber)
        {
            oTestDAL = new TestDAL();
            try
            {
                iDBOperationCode = oTestDAL.MachineFaultOccurred(MachineNumber);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oTestDAL = null;
            }
            return iDBOperationCode;
        }

        public List<CostCenterBO> GetAllCostCenters()
        {
            lstCostCenterBO = new List<CostCenterBO>();
            oTestDAL = new TestDAL();

            try
            {
                lstCostCenterBO = oTestDAL.GetAllCostCenters();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oTestDAL = null;
            }
            return lstCostCenterBO;

        }

        #endregion

    }
}
