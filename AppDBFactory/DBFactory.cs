﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class:         DBFactory
Description:   A wrapper class for accessing and modifying the objects in the database.
               The class will be accessed by the DAL class of all the Buisness Components for database operations.
Author:        
Creation Date: 13-Feb-2008
Notes:         Connection string will be referenced from Web.config File. DBFactory Project should have dll in bin folders: Microsoft.Practices.ObjectBuilder2.dll
 * Microsoft.Practices.EnterpriseLibrary.Data.dll and Microsoft.Practices.EnterpriseLibrary.Common.dll. DBFactory project should have reference
 * to Microsoft.Practices.EnterpriseLibrary.Data.dll and Microsoft.Practices.EnterpriseLibrary.Common.dll. 

Modified Details:
Modified By                Modified Date               Modified Reason

---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

#region " Additional Namespaces "

using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common;

#endregion

namespace AppDBFactory
{
    /// -------------------------------------------------------------------------
    /// <summary>
    /// Class functionality
    /// </summary>
    /// <param name="objTransaction">Transaction object</param>
    /// -------------------------------------------------------------------------
    public class DBFactory
    {
        #region " Class Level Variable "
        
        #endregion

        #region " Constructor "

        #endregion

        #region " Private Variables "

        //To hold Database object
        private static Database dbDatabase = null;

        //To hold Dataset object      
        private static DataSet dsDataset = null;

        //To hold Command object      
        private static DbCommand dcDbCommand = null;

        //To hold result of GetScaler method.        
        private static object objObject = null;

        #endregion

        #region " Shared Variables "

        #endregion

        #region " Public Variables "

        #endregion

        #region " Public Properties "

        #endregion

        #region " Private Functions "

        #endregion

        #region " Protected Functions "

        #endregion

        #region " Public Functions "

        /// -------------------------------------------------------------------------
        /// <summary>
        /// This function retrieves data from database into the Dataset.
        /// </suummary>
        /// <param name="SpName">Name of the Stored Procedure</param>
        /// <param name="Parameters">SP Parameters</param>
        /// <returns></returns
        /// -------------------------------------------------------------------------
        public static DataSet GetDataSet(string SpName, object[] Parameters)
        {
            try
            {
                dsDataset = new DataSet();
                //Create the Database object, using the default database service. The
                //default database service is determined through configuration(Web.Config File).
                //If below method call has error, check if Microsoft.Practices.ObjectBuilder2.dll is present in BIN folder of the project
                dbDatabase = DatabaseFactory.CreateDatabase("DBConnStr");

                //Checking for input parameters for stored procedure if available or not.
                if (Parameters == null)
                {
                    //If stored procedure does not accept any parameter.
                    dcDbCommand = dbDatabase.GetStoredProcCommand(SpName);
                }
                else
                {
                    //If stored procedure accept parameter/parameters.
                    dcDbCommand = dbDatabase.GetStoredProcCommand(SpName, Parameters);
                }

                //dcDbCommand.CommandTimeout = 1000;

                //Get the result as DataSet objet.
                dsDataset = dbDatabase.ExecuteDataSet(dcDbCommand);

                //Returns DataSet object containing resultset.
                return dsDataset;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// -------------------------------------------------------------------------
        /// <summary>
        /// This function retrieves single value from the database into the object.
        /// </summary>
        ///<param name="SpName">StoredProcedure Name</param>
        ///<param name="Parameters">Parameter List</param>
        /// <returns></returns>
        /// -------------------------------------------------------------------------
        public static Object GetScalar(string SpName, object[] Parameters)
        {
            try
            {
                //Create the Database object, using the default database service. The
                //default database service is determined through configuration(Web.Config File).
                dbDatabase = DatabaseFactory.CreateDatabase("DBConnStr");

                //Checking for input parameters for stored procedu  re if available or not.
                if (Parameters == null)
                {
                    //If stored procedure does not accept any parameter.
                    dcDbCommand = dbDatabase.GetStoredProcCommand(SpName);
                }
                else
                {
                    //If stored procedure accept parameter/parameters.
                    dcDbCommand = dbDatabase.GetStoredProcCommand(SpName, Parameters);
                }

                //Get the result as DataSet objet.
                objObject = dbDatabase.ExecuteScalar(dcDbCommand);

                //Returns DataSet object containing resultset.
                return objObject;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// -------------------------------------------------------------------------
        /// <summary>
        /// This funtion is use Inserts,Updates or Deletes the record from the database.
        /// </summary>
        /// <param name="strSPName">StoredProcedure Name</param>
        /// <param name="arrParam">Parameter List</param>
        /// <param name="objTransaction">Transaction object</param>
        /// <returns>Dataset</returns>
        /// -------------------------------------------------------------------------
        public static DataSet RunStoredProcedure(string SpName, object[] Parameters, DbTransaction objTransaction)
        {
            try
            {
                //Create the Database object, using the default database service. The
                //default database service is determined through configuration(Web.Config File).                
                dbDatabase = DatabaseFactory.CreateDatabase("DBConnStr");

                //Getting Command object
                dcDbCommand = dbDatabase.GetStoredProcCommand(SpName, Parameters);

                if (objTransaction == null)
                {
                    //Get the result as DataSet objet.
                    dsDataset = dbDatabase.ExecuteDataSet(dcDbCommand);
                }
                else
                {
                    //Get the result as DataSet objet.
                    dsDataset = dbDatabase.ExecuteDataSet(dcDbCommand, objTransaction);
                }

                //Returns DataSet object containing resultset.
                return dsDataset;
            }
            catch (Exception ex)
            {
                //objTransaction.Rollback();
                //DbFactory.RollBackTransaction(objTransaction);
                throw new Exception(ex.Message);
            }
        }

        /// -------------------------------------------------------------------------
        /// <summary>
        /// This function creates connection with the database and returns trasaction object.
        /// </summary>
        /// <returns>Transaction object</returns>
        /// -------------------------------------------------------------------------
        public static DbTransaction CreateConnection()
        {
            DbConnection DBFConnection = null;
            DbTransaction DBFTransaction = null;
            try
            {
                // Create the Database object, using the default database service. The
                // default database service is determined through configuration (Web.Config File).
                dbDatabase = DatabaseFactory.CreateDatabase();

                DBFConnection = dbDatabase.CreateConnection();
                DBFConnection.Open();
                DBFTransaction = DBFConnection.BeginTransaction();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return DBFTransaction;
        }

        /// -------------------------------------------------------------------------
        /// <summary>
        /// This function commits the transaction.
        /// </summary>
        /// <param name="objTransaction">Transaction object</param>
        /// -------------------------------------------------------------------------
        public static void CommitTransaction(DbTransaction objTransaction)
        {
            try
            {
                objTransaction.Commit();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// -------------------------------------------------------------------------
        /// <summary>
        /// This function rollbacks the transaction.
        /// </summary>
        /// <param name="objTransaction">Transaction object</param>
        /// -------------------------------------------------------------------------
        public static void RollBackTransaction(DbTransaction objTransaction)
        {
            try
            {
                objTransaction.Rollback();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region " Events "

        #endregion
    }
}


