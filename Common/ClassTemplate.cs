﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

#endregion

#region " Additional Namespaces "

using System.IO;
using System.Configuration;
using System.Security.Principal;
using System.Diagnostics;

using Common;

#endregion

namespace Common
{
    public class ClassTemplate
    {
        #region " Class Level Variable "

        TraceUtility oTraceUtility = new TraceUtility();

        #endregion " Class Level Variable "

        #region " Constructor "

        #endregion

        #region " Private Variables "

        #endregion

        #region " Shared Variables "

        #endregion

        #region " Public Variables "

        public static EventLog eLog = null;
        public static bool isEnabled = false;

        #endregion

        #region " Private Functions "              

        private void SampleFunction()
        {
            try
            {

            }
            catch (Exception ex)
            {               
                //lblMessage.Text = Resources.Resource.lblCatchMsg + " " + ex.Message;
                //lblMessage.ForeColor = Color.Red;
                oTraceUtility.TraceService("<Function Name> - Exception: " + ex.Message);
            }
            finally
            {

            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        ///---------------------------------------------------------------------
        private static void SampleWriteToApplicationEventLog(string message)
        {
            try
            {

                if (SampleIsLoggingEnabled())
                {
                    String source = ConfigurationManager.AppSettings["EventLogSource"];
                    String log = "Application";
                    if (!EventLog.SourceExists(source))
                    {
                        EventLog.CreateEventSource(source, log);
                    }

                    eLog = new EventLog();
                    eLog.Source = source;

                    eLog.WriteEntry(message, EventLogEntryType.Information);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                eLog = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ///---------------------------------------------------------------------
        private static bool SampleIsLoggingEnabled()
        {
            try
            {
                isEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["ApplicationLogEnabled"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isEnabled;
        }

        #endregion " Private Functions "

        #region " Protected Functions "

        #endregion

        #region " Public Functions "



        ///---------------------------------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        ///---------------------------------------------------------------------
        public static void SampleTraceService(string content)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(content) == false)
                {
                    string filePath = ConfigurationManager.AppSettings["LogLocation"];
                    using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        StreamWriter sw = new StreamWriter(fs);
                        sw.BaseStream.Seek(0, SeekOrigin.End);
                        sw.WriteLine("[" + DateTime.Now.ToString() + "]: " + content);
                        sw.Flush();
                        sw.Close();
                    }
                }
            }
            catch (IOException ex)
            {
                SampleWriteToApplicationEventLog(ex.Message);
            }
            catch (Exception ex)
            {
                SampleWriteToApplicationEventLog(ex.Message);
            }
        }

        #endregion

        #region " Events "

        #endregion
    }
    
}
