﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

#endregion

#region " Additional Namespaces "

using System.IO;
using System.Configuration;
using System.Security.Principal;
using System.Diagnostics;

#endregion

namespace Common
{
    public class TraceUtility
    {
        #region " Public Variables "

        public static EventLog eLog = null;
        public static bool isEnabled = false;

        #endregion

        #region " Public Functions "

        ///-------------------------------------------------------------
        /// <summary>
        /// Writes logs to the log file. 
        /// Log file location must be configured in app.config/web.config under key "LogLocation"
        /// </summary>
        /// <param name="content"></param>
        ///-------------------------------------------------------------
        public void TraceService(string content)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(content) == false)
                {
                    string filePath = ConfigurationManager.AppSettings["LogLocation"];
                    using (FileStream fs = new FileStream(filePath + DateTime.Now.ToShortDateString().Replace('/', '-') + ".txt", FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        StreamWriter sw = new StreamWriter(fs);
                        sw.BaseStream.Seek(0, SeekOrigin.End);
                        sw.WriteLine("[" + DateTime.Now.ToString() + "]: " + content);
                        sw.Flush();
                        sw.Close();
                    }
                }
            }
            catch (IOException ex)
            {
                WriteToApplicationEventLog(ex.Message);
            }
            catch (Exception ex)
            {
                WriteToApplicationEventLog(ex.Message);
            }
        }

        ///-------------------------------------------------------------
        /// <summary>
        /// Writes logs to windows application event logger
        /// This is another option to log events/errors
        /// </summary>
        /// <param name="message"></param>
        ///-------------------------------------------------------------
        private void WriteToApplicationEventLog(string message)
        {

            if (IsLoggingEnabled())
            {
                String source = ConfigurationManager.AppSettings["EventLogSource"];
                String log = "Application";
                if (!EventLog.SourceExists(source))
                {
                    EventLog.CreateEventSource(source, log);
                }

                EventLog eLog = new EventLog();
                eLog.Source = source;

                eLog.WriteEntry(message, EventLogEntryType.Information);
            }
        }

        #endregion " Public Functions "

        #region " Private Functions "

        ///-------------------------------------------------------------
        /// <summary>
        /// Checks if windows events application logging is enabled
        /// App.config/Web.config must have settings under key "ApplicationLogEnabled". Value "True"
        /// </summary>
        /// <returns></returns>
        ///-------------------------------------------------------------
        private bool IsLoggingEnabled()
        {
            bool isEnabled = false;
            isEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["ApplicationLogEnabled"]);
            return isEnabled;
        }

        ///-------------------------------------------------------------
        /// <summary>
        /// Checks if error logging enabled. AppSettings["EnableErrorLog"] must be True to enable.
        /// </summary>
        /// <returns></returns>
        ///-------------------------------------------------------------
        private static bool IsErrorLoggingEnabled()
        {
            try
            {
                isEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableErrorLog"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isEnabled;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Checks if events logging enabled. AppSettings["EnableEventLog"] must be True to enable.
        /// </summary>
        /// <returns></returns>
        ///---------------------------------------------------------------------
        private static bool IsEventLoggingEnabled()
        {
            try
            {
                isEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableEventLog"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isEnabled;
        }

        #endregion " Private Functions "


    }
}
