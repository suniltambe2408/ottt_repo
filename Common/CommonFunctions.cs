﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   CommonFunctions
Description     :   This class has functions which are commonly used across the project
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

#region " Additional Namespaces "

using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.DirectoryServices;
using System.Security.Principal;
using System.DirectoryServices.AccountManagement;

using System.Data;
using AppDBFactory;
using OTTTBO;

#endregion

namespace Common
{
    public class CommonFunctions
    {
        #region " Class Level Variable "


        DataSet dsDataSet = null;
        List<MachineMasterBO> lstMachineMasterBO = null;
        MachineMasterBO oMachineMasterBO = null;
        int iCount = 0;

        #endregion

        #region " Private Functions "

        #endregion

        #region " Public Functions "

        //--------------------------------------------------------------------------------------------------
        /// <summary>
        /// Sends an email
        /// </summary>     
        /// <param name="to"></param>
        /// <param name="cc"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        //--------------------------------------------------------------------------------------------------
        public static void SendMail(string to, string cc, string subject, string body)
        {
            SmtpClient smtpClient = null;
            MailMessage mailMessage = null;

            try
            {
                //Instantiate a new instance of MailMessage
                mailMessage = new MailMessage();

                //Set the recepient address of the mail message
                if (to != null && to != string.Empty)
                {
                    foreach (string addr in to.Split(';'))
                    {
                        mailMessage.To.Add(new MailAddress(addr));
                    }
                }

                //Check if the cc value is null or an empty value
                if ((cc != null) && (cc != string.Empty))
                {
                    // Set the CC address of the mail message
                    foreach (string addr in cc.Split(';'))
                    {
                        mailMessage.CC.Add(new MailAddress(addr));
                    }
                }

                //Set the subject of the mail message
                mailMessage.Subject = subject;

                // Set the body of the mail message
                mailMessage.Body = body;

                //Set the format of the mail message body as HTML
                mailMessage.IsBodyHtml = true;

                //Set the priority of the mail message to normal
                mailMessage.Priority = MailPriority.Normal;

                //Instantiate a new instance of SmtpClient
                smtpClient = new SmtpClient();

                smtpClient.EnableSsl = false;

                //Send the mail message
                smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                mailMessage = null;
                smtpClient = null;
            }
        }

        public static bool IsValidVWManagerID(string managerEmailID)
        {
            bool bIsVWEmployee = false;
            string adEntry = String.Empty;

            try
            {

                DirectorySearcher adSearcher = new DirectorySearcher(adEntry);
                adSearcher.Filter = ("mail=" + managerEmailID);
                SearchResultCollection searchCollection = adSearcher.FindAll();


                //Check if email id entered for special permission approval is VW employee
                string[] arrApproverEmail = Convert.ToString(managerEmailID).Split('.');

                if (Convert.ToString(arrApproverEmail[0]) == "extern")
                {
                    bIsVWEmployee = false;
                }
                else
                {
                    bIsVWEmployee = true;
                }

                //Check all required conditions for validation
                if (searchCollection.Count > 0 && bIsVWEmployee == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// -------------------------------------------------------
        /// <summary>
        /// Get All Type
        /// </summary>
        /// <returns>List of Type</returns>
        /// -------------------------------------------------------
        public List<MachineMasterBO> GetAllMachineName()
        {
            lstMachineMasterBO = new List<MachineMasterBO>();
            dsDataSet = new DataSet();
            try
            {
                dsDataSet = DBFactory.GetDataSet("SpGetAllMachineName", null);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oMachineMasterBO = new MachineMasterBO();

                        oMachineMasterBO.MachineID = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["MachineID"]);                      
                        oMachineMasterBO.Machine = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["Machine"]);

                        lstMachineMasterBO.Add(oMachineMasterBO);
                        oMachineMasterBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstMachineMasterBO;
        }

        #endregion

    }
}
