﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using System.Data;
using OTTTBO;
using AppDBFactory;

#endregion " Additional Namespaces "

namespace OTTTDAL
{
    public class MachineMasterDAL
    {
        #region " Class Level Variable "

        DataSet dsDataSet = null;
        int iDBOperationCode = 0;
        int iCount = 0;

        List<MonitoringBO> lstMonitoringBO = null;
        MonitoringBO oMonitoringBO = null;

        MachineMasterBO oMachineMasterBO = null;
        List<MachineMasterBO> lstMachineMaster = null;

        #endregion " Class Level Variable "

        #region " Public Functions "

        /// -------------------------------------------------------
        /// <summary>
        /// Inserts Machine Master 
        /// </summary>
        /// <param name="oMachineMasterBO"></param>
        /// <returns>DB Operation status Code</returns>
        /// -------------------------------------------------------

        public int SaveMachine(MachineMasterBO oMachineMasterBO)
        {
            dsDataSet = new DataSet();
            object[] parameters = new object[7];

            try
            {
                parameters.SetValue(oMachineMasterBO.MachineID, 0);
                parameters.SetValue(oMachineMasterBO.MachineNO, 1);
                parameters.SetValue(oMachineMasterBO.MachineNoLong, 2);
                parameters.SetValue(oMachineMasterBO.MachineName, 3);
                parameters.SetValue(oMachineMasterBO.MachineType, 4);
                parameters.SetValue(oMachineMasterBO.RoomName, 5);
                parameters.SetValue(oMachineMasterBO.IsActive, 6);

                dsDataSet = DBFactory.RunStoredProcedure("SpSaveMachineMaster", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                    iDBOperationCode = Convert.ToInt32(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
                parameters = null;
            }
            return iDBOperationCode;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Gets all Machines
        /// </summary>
        /// <returns>List of Machines</returns>
        /// --------------------------------------------------------

        public List<MachineMasterBO> GetAllMachineDetails()
        {
            lstMachineMaster = new List<MachineMasterBO>();
            dsDataSet = new DataSet();

            try
            {
                dsDataSet = DBFactory.GetDataSet("SpGetAllMachineDetails", null);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oMachineMasterBO = new MachineMasterBO();

                        oMachineMasterBO.MachineID = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["MachineID"]);
                        oMachineMasterBO.MachineNO = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["MachineNO"]);
                        oMachineMasterBO.MachineNoLong = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["MachineNoLong"]);
                        oMachineMasterBO.MachineName = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["MachineName"]);
                        oMachineMasterBO.MachineType = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["MachineType"]);
                        oMachineMasterBO.RoomName = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["RoomName"]);
                        oMachineMasterBO.IsActive = Convert.ToBoolean(dsDataSet.Tables[0].Rows[iCount]["IsActive"]);

                        lstMachineMaster.Add(oMachineMasterBO);
                        oMachineMasterBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstMachineMaster;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Search machine based on LIMS_no
        /// </summary>
        /// <returns>List of Machines</returns>
        /// --------------------------------------------------------

        public List<MachineMasterBO> SearchMachine(string Keyword)
        {
            lstMachineMaster = new List<MachineMasterBO>();
            object[] parameter = new object[1];
            dsDataSet = new DataSet();

            try
            {
                parameter.SetValue(Keyword, 0);

                dsDataSet = DBFactory.GetDataSet("SpSearchMachine", parameter);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oMachineMasterBO = new MachineMasterBO();

                        oMachineMasterBO.MachineID = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["MachineID"]);
                       //oMachineMasterBO.MachineNO = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["MachineNO"]);
                        oMachineMasterBO.MachineNoLong = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["MachineNoLong"]);
                        oMachineMasterBO.MachineName = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["MachineName"]);
                        oMachineMasterBO.MachineType = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["MachineType"]);
                        oMachineMasterBO.RoomName = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["RoomName"]);
                        oMachineMasterBO.IsActive = Convert.ToBoolean(dsDataSet.Tables[0].Rows[iCount]["IsActive"]);

                        lstMachineMaster.Add(oMachineMasterBO);
                        oMachineMasterBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstMachineMaster;
        }

        public List<MonitoringBO> GetAllMachineNumbers()
        {
            dsDataSet = new DataSet();
            lstMonitoringBO = new List<MonitoringBO>();
            try
            {
                dsDataSet = DBFactory.GetDataSet("SpGetRunningTestMachineNumbers", null);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oMonitoringBO = new MonitoringBO();

                        if (dsDataSet.Tables[0].Rows[iCount]["MachineNO"] != DBNull.Value)
                        {
                            oMonitoringBO.MachineNumber = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["MachineNO"]);
                        }
                        if (dsDataSet.Tables[0].Rows[iCount]["MachineType"] != DBNull.Value)
                        {
                            oMonitoringBO.MachineType = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["MachineType"]);
                        }
                        if (dsDataSet.Tables[0].Rows[iCount]["MachineNoLong"] != DBNull.Value)
                        {
                            oMonitoringBO.MachineNoLong = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["MachineNoLong"]);
                        }
                        if (dsDataSet.Tables[0].Rows[iCount]["MachineName"] != DBNull.Value)
                        {
                            oMonitoringBO.MachineName = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["MachineName"]);
                        }
                        lstMonitoringBO.Add(oMonitoringBO);
                        oMachineMasterBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {

            }
            return lstMonitoringBO;

        }


        public int GetMachineNoByLimsNo(string LIMSNO)
        {
            dsDataSet = new DataSet();
            oMachineMasterBO = new MachineMasterBO();
            object[] parameter = new object[1];

            try
            {
                parameter.SetValue(LIMSNO, 0);

                dsDataSet = DBFactory.GetDataSet("SpGetMachineNoByLimsNo", parameter);

                if (dsDataSet != null)
                {

                    oMachineMasterBO = new MachineMasterBO();

                    if (dsDataSet.Tables[0].Rows[iCount]["MachineNO"] != DBNull.Value)
                    {
                        oMachineMasterBO.MachineNO = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["MachineNO"]);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {

            }
            return oMachineMasterBO.MachineNO;

        }


        public int CheckDuplicateMachineNO(string MachineNoLong)
        {
            dsDataSet = new DataSet();
            object[] parameters = new object[1];

            try
            {
                parameters.SetValue(MachineNoLong, 0);

                dsDataSet = DBFactory.RunStoredProcedure("SpCheckDuplicateMachineNO", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                    iDBOperationCode = Convert.ToInt32(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                parameters = null;
                dsDataSet = null;
            }
            return iDBOperationCode;
        }

        #endregion
    }
}

