﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using System.Data;
using OTTTBO;
using AppDBFactory;

#endregion " Additional Namespaces "

namespace OTTTDAL
{
    public class TestDAL
    {

        #region " Class Level Variable "

        DataSet dsDataSet = null;
        int iDBOperationCode = 0;
        List<TestBO> lstTestBO = null;
        TestBO oTestBO = null;
        int iCount = 0;
        List<CostCenterBO> lstCostCenterBO = null;
        CostCenterBO oCostCenterBO = null;

        #endregion " Class Level Variable "

        #region " Public Functions "

        ///---------------------------------------------------------------------
        /// <summary>
        /// This method call Stoared procedure to save new test details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///--------------------------------------------------------------------- 

        public int SaveNewTest(TestBO oTestBO)
        {
            dsDataSet = new DataSet();
            object[] parameters = new object[13];

            try
            {
                parameters.SetValue(oTestBO.TestID, 0);
                parameters.SetValue(oTestBO.LIMSNO, 1);
                parameters.SetValue(oTestBO.TestName, 2);
                parameters.SetValue(oTestBO.TestOwner, 3);
                parameters.SetValue(oTestBO.ComponentType, 4);
                parameters.SetValue(oTestBO.MachineID, 5);
                parameters.SetValue(oTestBO.TestCondition, 6);
                parameters.SetValue(oTestBO.StartDateTime, 7);
                parameters.SetValue(oTestBO.EndDateTime, 8);
                parameters.SetValue(oTestBO.Duration, 9);
                parameters.SetValue(oTestBO.Department, 10);
                parameters.SetValue(oTestBO.CostCenterID, 11);
                parameters.SetValue(oTestBO.UniqueTestID, 12);

                dsDataSet = DBFactory.RunStoredProcedure("SpSaveNewTest", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                    iDBOperationCode = Convert.ToInt32(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                parameters = null;
                dsDataSet = null;
            }
            return iDBOperationCode;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This method call stored procedure to get list of machine from its status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///--------------------------------------------------------------------- 

        public List<TestBO> GetAllMachineNOByStatus(int TestID)
        {
            lstTestBO = new List<TestBO>();
            dsDataSet = new DataSet();
            object[] parameters = new object[1];

            try
            {
                parameters.SetValue(TestID, 0);

                dsDataSet = DBFactory.RunStoredProcedure("SpGetAllMachineNOByStatus", parameters, null);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oTestBO = new TestBO();

                        if (dsDataSet.Tables[0].Rows[iCount]["MachineID"] != DBNull.Value)
                        {
                            oTestBO.MachineID = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["MachineID"]);
                        }

                        lstTestBO.Add(oTestBO);
                        oTestBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstTestBO;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This method call stored procedure to get count of tests running on machine
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///--------------------------------------------------------------------- 

        public int GetTestCountForMachineNumber(int MachineNumber)
        {
            int TestCount = 0;
            dsDataSet = new DataSet();
            object[] parameters = new object[1];
            try
            {
                parameters.SetValue(MachineNumber, 0);
                dsDataSet = DBFactory.RunStoredProcedure("SpGetTestCountForMachineNumber", parameters, null);
                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        if (dsDataSet.Tables[0].Rows[iCount]["TestCount"] != DBNull.Value)
                        {
                            TestCount = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["TestCount"]);
                        }
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return TestCount;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This method call stored procedure to get count of tests running on machine
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///--------------------------------------------------------------------- 

        public int MachineFaultOccurred(int MachineNumber)
        {
            dsDataSet = new DataSet();
            object[] parameters = new object[1];
            try
            {
                parameters.SetValue(MachineNumber, 0);
                dsDataSet = DBFactory.RunStoredProcedure("SpSetAllTestsStatusToFault", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                    iDBOperationCode = Convert.ToInt32(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return iDBOperationCode;
        }

        public List<CostCenterBO> GetAllCostCenters()
        {
            dsDataSet = new DataSet();
            object[] parameters = new object[0];
            lstCostCenterBO = new List<CostCenterBO>();
            try
            {
                dsDataSet = DBFactory.RunStoredProcedure("SpGetAllCostCenters", parameters, null);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oCostCenterBO = new CostCenterBO();

                        if (dsDataSet.Tables[0].Rows[iCount]["CostCenterID"] != DBNull.Value)
                        {
                            oCostCenterBO.CostCenterID = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["CostCenterID"]);
                        }
                        if (dsDataSet.Tables[0].Rows[iCount]["CostCenterName"] != DBNull.Value)
                        {
                            oCostCenterBO.CostCenterName = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["CostCenterName"]);
                        }

                        lstCostCenterBO.Add(oCostCenterBO);
                        oCostCenterBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstCostCenterBO;

        }
        #endregion
      
    }
}
