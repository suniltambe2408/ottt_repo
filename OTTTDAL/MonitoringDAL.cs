﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using System.Data;
using OTTTBO;
using AppDBFactory;

#endregion " Additional Namespaces "

namespace OTTTDAL
{
    public class MonitoringDAL
    {
        #region Class Level Variable

        DataSet dsDataSet = null;
       
        MonitoringDataBO oMonitoringDataBO = null;
        List<MonitoringDataBO> lstMonitoringDataBO = null;
        //List<MachineMasterBO> lstMachineMasterBO = null;
        //MachineMasterBO oMachineMasterBO = null;
        
        int iCount = 0;

        #endregion

        #region Public Functions

        public List<MonitoringDataBO> GetAllGraphData(string sFieldValues)
        {
            dsDataSet = new DataSet();
            lstMonitoringDataBO = new List<MonitoringDataBO>();
            oMonitoringDataBO = new MonitoringDataBO();
            object[] parameters = new object[1];
            try
            {
                parameters.SetValue(sFieldValues, 0);
                dsDataSet = DBFactory.GetDataSet("SpGetAllGraphData", parameters);
                if (dsDataSet != null)
                {
                    string[] values = sFieldValues.Split(',');
                    
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oMonitoringDataBO = new MonitoringDataBO();
                        if (dsDataSet.Tables[0].Rows[iCount]["Time"] != DBNull.Value)
                        {
                            oMonitoringDataBO.DateTimes = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["Time"]);
                        }
                      
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = values[i].Trim();
                            if(values[i].Equals("C_Temp_602"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["C_Temp_602"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.C_Temp_602 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["C_Temp_602"]);
                                }
                            }
                            if (values[i].Equals("C_Humidity_602"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["C_Humidity_602"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.C_Humidity_602 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["C_Humidity_602"]);
                                }
                            }
                            if (values[i].Equals("H_Temp_602"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["H_Temp_602"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.H_Temp_602 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["H_Temp_602"]);
                                }
                            }
                            if (values[i].Equals("W_Temp_602"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["W_Temp_602"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.W_Temp_602 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["W_Temp_602"]);
                                }
                            }
                            if (values[i].Equals("T_SP_603"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_SP_603"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_SP_603 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_SP_603"]);
                                }
                            }
                            if (values[i].Equals("T_PV_603"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_PV_603"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_PV_603 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_PV_603"]);
                                }
                            }
                            if (values[i].Equals("T_SP_604"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_SP_604"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_SP_604 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_SP_604"]);
                                }
                            }
                            if (values[i].Equals("T_PV_604"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_PV_604"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_PV_604 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_PV_604"]);
                                }
                            }
                            if (values[i].Equals("T_SP_605"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_SP_605"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_SP_605 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_SP_605"]);
                                }
                            }
                            if (values[i].Equals("T_PV_605"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_PV_605"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_PV_605 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_PV_605"]);
                                }
                            }
                            if (values[i].Equals("T_SP_606"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_SP_606"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_SP_606 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_SP_606"]);
                                }
                            }
                            if (values[i].Equals("T_PV_606"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_PV_606"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_PV_606 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_PV_606"]);
                                }
                            }
                            if (values[i].Equals("T_SP_704"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_SP_704"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_SP_704 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_SP_704"]);
                                }
                            }
                            if (values[i].Equals("T_PV_704"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_PV_704"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_PV_704 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_PV_704"]);
                                }
                            }
                            if (values[i].Equals("T_PV_706"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_PV_706"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_PV_706 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_PV_706"]);
                                }
                            }
                            if (values[i].Equals("TM_SP_706"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["TM_SP_706"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.TM_SP_706 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["TM_SP_706"]);
                                }
                            }
                            if (values[i].Equals("TA_SP_706"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["TA_SP_706"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.TA_SP_706 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["TA_SP_706"]);
                                }
                            }
                            if (values[i].Equals("H_PV_706"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["H_PV_706"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.H_PV_706 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["H_PV_706"]);
                                }
                            }
                            if (values[i].Equals("HM_SP_706"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["HM_SP_706"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.HM_SP_706 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["HM_SP_706"]);
                                }
                            }
                            if (values[i].Equals("HA_SP_706"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["HA_SP_706"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.HA_SP_706 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["HA_SP_706"]);
                                }
                            }
                            if (values[i].Equals("T_SP_707"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_SP_707"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_SP_707 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_SP_707"]);
                                }
                            }
                            if (values[i].Equals("T_PV_707"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["T_PV_707"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.T_PV_707 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["T_PV_707"]);
                                }
                            }
                            if (values[i].Equals("H_PV_707"))
                            {
                                if (dsDataSet.Tables[0].Rows[iCount]["H_PV_707"] != DBNull.Value)
                                {
                                    oMonitoringDataBO.H_PV_707 = Convert.ToDouble(dsDataSet.Tables[0].Rows[iCount]["H_PV_707"]);
                                }
                            }
                        }

                        lstMonitoringDataBO.Add(oMonitoringDataBO);
                        oMonitoringDataBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
               
            }
           
            return lstMonitoringDataBO; 
       
        }

        #endregion
    }
}
