﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using System.Data;
using OTTTBO;
using AppDBFactory;

#endregion " Additional Namespaces "

namespace OTTTDAL
{
    public class TestTrackingDAL
    {

        #region " Class Level Variable "

        DataSet dsDataSet = null;
        int iDBOperationCode = 0;
        int iCount = 0;

        TestBO oTestBO = null;
        List<TestBO> lstAddNewTestBO = null;

        #endregion " Class Level Variable "

        #region " Public Functions "

        ///---------------------------------------------------------------------
        /// <summary>
        /// Search Test Based on LIMS NO
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        
        public List<TestBO> SearchLimsData(string LIMSNO)
        {
            lstAddNewTestBO = new List<TestBO>();
            object[] parameter = new object[1];
            dsDataSet = new DataSet();

            try
            {
                parameter.SetValue(LIMSNO, 0);

                dsDataSet = DBFactory.GetDataSet("SpSearchLimsData", parameter);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oTestBO = new TestBO();
                        oTestBO.LIMSNO = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["LIMSNO"]);
                        oTestBO.TestID = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["TestID"]);
                        oTestBO.TestName = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["TestName"]);
                        oTestBO.TestOwner = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["TestOwner"]);
                        oTestBO.ComponentType = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["ComponentType"]);
                        if (dsDataSet.Tables[0].Rows[iCount]["TestCondition"] != DBNull.Value)
                        {
                            oTestBO.TestCondition = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["TestCondition"]);
                        }
                        if (dsDataSet.Tables[0].Rows[iCount]["Status"] != DBNull.Value)
                        {
                            oTestBO.Status = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["Status"]);
                        }
                        oTestBO.StartDateTime = Convert.ToDateTime(dsDataSet.Tables[0].Rows[iCount]["StartDateTime"]);
                        oTestBO.StartDate=Convert.ToDateTime(dsDataSet.Tables[0].Rows[iCount]["StartDate"]);
                        oTestBO.StartTime = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["StartTime"]);
                        string[] tokens = oTestBO.StartTime.Split(':');
                        oTestBO.StartTimeHh = tokens[0];
                        oTestBO.StartTimeMm = tokens[1];
                        if (Convert.ToInt32(oTestBO.StartTimeHh) < 12)
                        {
                            oTestBO.StartTimeAmPm = "AM";
                        }else{
                            oTestBO.StartTimeAmPm = "PM";
                        }
                        if (dsDataSet.Tables[0].Rows[iCount]["EndDateTime"]!=DBNull.Value)
                        {
                            oTestBO.EndDateTime = Convert.ToDateTime(dsDataSet.Tables[0].Rows[iCount]["EndDateTime"]);
                        }
                       
                        oTestBO.Duration = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["Duration"]);

                        if ((oTestBO.Duration / 60) < 10)
                        {
                            oTestBO.DurationHh = "0" + Convert.ToString(oTestBO.Duration / 60);
                        }
                        else
                        {
                            oTestBO.DurationHh = Convert.ToString(oTestBO.Duration / 60);
                        }
                        if ((oTestBO.Duration % 60) < 10)
                        {
                            oTestBO.DurationMm = "0" + Convert.ToString(oTestBO.Duration % 60);
                        }
                        else
                        {
                            oTestBO.DurationMm = Convert.ToString(oTestBO.Duration % 60);
                        }
                        oTestBO.MachineID = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["MachineID"]);
                        oTestBO.Machine = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["Machine"]);
                        if (dsDataSet.Tables[0].Rows[iCount]["UniqueTestID"] != DBNull.Value)
                        {
                            oTestBO.UniqueTestID = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["UniqueTestID"]);
                        }

                        lstAddNewTestBO.Add(oTestBO);
                        oTestBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstAddNewTestBO;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Update selected grid view data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------    

        public int UpdateTestInformation(TestBO oTestBO)
        {
            dsDataSet = new DataSet();
            object[] parameters = new object[10];

            try
            {
                parameters.SetValue(oTestBO.TestID, 0);
                parameters.SetValue(oTestBO.LIMSNO, 1);
                parameters.SetValue(oTestBO.ComponentType, 2);
                parameters.SetValue(oTestBO.TestName, 3);
                parameters.SetValue(oTestBO.TestCondition, 4);
                parameters.SetValue(oTestBO.TestOwner, 5);
                parameters.SetValue(oTestBO.StartDateTime, 6);
                parameters.SetValue(oTestBO.EndDateTime, 7);
                parameters.SetValue(oTestBO.Duration, 8);
                parameters.SetValue(oTestBO.MachineID, 9);
                

                dsDataSet = DBFactory.RunStoredProcedure("SpUpdateTestTrackingDetails", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                    iDBOperationCode = Convert.ToInt32(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                parameters = null;
                dsDataSet = null;
            }
            return iDBOperationCode;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Update status Deleted based on TestID
        /// </summary>
        /// <returns>List of Test</returns>
        /// --------------------------------------------------------

        public int StatusUpdateToDeleted(int TestID)
        {
            dsDataSet = new DataSet();
            object[] parameters = new object[1];

            try
            {
                parameters.SetValue(TestID, 0);

                dsDataSet = DBFactory.RunStoredProcedure("SpStatusUpdateToDeleted", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                    iDBOperationCode = Convert.ToInt32(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
            return iDBOperationCode;
        }
                
        ///---------------------------------------------------------------------
        /// <summary>
        /// This method check duplicate LIMS Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///--------------------------------------------------------------------- 
        public string CheckDuplicateLIMSNO(string LIMSNO)
        {
            dsDataSet = new DataSet();
            object[] parameters = new object[1];
            string LIMSNumber=null;
            try
            {
                parameters.SetValue(LIMSNO, 0);

                dsDataSet = DBFactory.RunStoredProcedure("SpCheckDuplicateLIMSNO", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                   LIMSNumber = Convert.ToString(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                parameters = null;
                dsDataSet = null;
            }
            return LIMSNumber;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Get count of Tests against given LIMS Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        public int GetTestCountForLIMSNo(string LIMSNO)
        {
            dsDataSet = new DataSet();
            object[] parameters = new object[1];
            int count = 0;
            try
            {
                parameters.SetValue(LIMSNO, 0);

                dsDataSet = DBFactory.RunStoredProcedure("GetTestCountForLIMSNo", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                    if (dsDataSet.Tables[0].Rows[iCount]["TestCount"] != DBNull.Value)
                    {
                        count = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["TestCount"]);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                parameters = null;
                dsDataSet = null;
            }
            return count;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Get Test Id List based on LIMS Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        public List<TestBO> GetTestIdListForLimsNumber(string LimsNumber)
        {
            lstAddNewTestBO = new List<TestBO>();
            object[] parameter = new object[1];
            dsDataSet = new DataSet();

            try
            {
                parameter.SetValue(LimsNumber, 0);

                dsDataSet = DBFactory.GetDataSet("SpGetTestIdListForLimsNumber", parameter);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oTestBO = new TestBO();
                        if (dsDataSet.Tables[0].Rows[iCount]["UniqueTestID"] != DBNull.Value)
                        {
                            oTestBO.UniqueTestID = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["UniqueTestID"]);
                        }
                        lstAddNewTestBO.Add(oTestBO);
                        oTestBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstAddNewTestBO;
        }

        #endregion

    }
}
