﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using System.Data;
using OTTTBO;
using AppDBFactory;

#endregion " Additional Namespaces "

namespace OTTTDAL
{
    public class DashboardDAL
    {

        #region " Class Level Variable "

        DataSet dsDataSet = null;
        int iDBOperationCode = 0;
        int iCount = 0;

        TestBO oTestBO = null;
        MachineStatusBO oMachineStatusBO = null;
        List<TestBO> lstTestBO = null;
        List<MachineStatusBO> lstMachineStatusBO = null;

        #endregion " Class Level Variable "

        #region " Public Functions "

        /// -------------------------------------------------------
        /// <summary>
        /// Call procedure to get all Scheduled test
        /// </summary>
        /// <returns>List of Test</returns>
        /// --------------------------------------------------------

        public List<TestBO> GetAllScheduledTest()
        {
            lstTestBO = new List<TestBO>();
            dsDataSet = new DataSet();

            try
            {
                dsDataSet = DBFactory.GetDataSet("SpGetAllScheduledTest", null);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oTestBO = new TestBO();

                        oTestBO.TestID = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["TestID"]);
                        oTestBO.LIMSNO = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["LIMSNO"]);
                        oTestBO.ComponentType = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["ComponentType"]);
                        oTestBO.TestName = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["TestName"]);
                        oTestBO.TestOwner = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["TestOwner"]);
                        if (dsDataSet.Tables[0].Rows[iCount]["StartDateTime"] != DBNull.Value)
                        {
                            oTestBO.StartDateTime = Convert.ToDateTime(dsDataSet.Tables[0].Rows[iCount]["StartDateTime"]);
                        }
                        oTestBO.StartDate = Convert.ToDateTime(dsDataSet.Tables[0].Rows[iCount]["StartDate"]);
                        oTestBO.StartTime = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["StartTime"]);
                        string[] tokens = oTestBO.StartTime.Split(':');
                        oTestBO.StartTimeHh = tokens[0];
                        oTestBO.StartTimeMm = tokens[1];
                        if (dsDataSet.Tables[0].Rows[iCount]["EndDateTime"] != DBNull.Value)
                        {
                            oTestBO.EndDateTime = Convert.ToDateTime(dsDataSet.Tables[0].Rows[iCount]["EndDateTime"]);
                        }
                        oTestBO.Duration = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["Duration"]);
                        oTestBO.Machine = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["Machine"]);
                        if ((oTestBO.Duration / 60) < 10)
                        {
                            oTestBO.DurationHh = "0" + Convert.ToString(oTestBO.Duration / 60);
                        }
                        else
                        {
                            oTestBO.DurationHh = Convert.ToString(oTestBO.Duration / 60);
                        }
                        if ((oTestBO.Duration % 60) < 10)
                        {
                            oTestBO.DurationMm = "0" + Convert.ToString(oTestBO.Duration % 60);
                        }
                        else
                        {
                            oTestBO.DurationMm = Convert.ToString(oTestBO.Duration % 60);
                        }
                        
                        lstTestBO.Add(oTestBO);
                        oTestBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstTestBO;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Update Scheduled status to Started test based on TestID
        /// </summary>
        /// <returns></returns>
        /// --------------------------------------------------------       

        public int UpdateStatusToStarted(int TestID)
        {
            object[] parameters = new object[1];

            try
            {
                parameters.SetValue(TestID, 0);

                dsDataSet = DBFactory.RunStoredProcedure("SpUpdateStatusToStarted", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                    iDBOperationCode = Convert.ToInt32(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                dsDataSet = null;
                parameters = null;
            }
            return iDBOperationCode;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Update Scheduled test based on TestID
        /// </summary>
        /// <returns></returns>
        /// --------------------------------------------------------

        public int UpdateScheduledTest(TestBO oTestBO)
        {
            dsDataSet = new DataSet();
            object[] parameters = new object[9];

            try
            {
                parameters.SetValue(oTestBO.TestID, 0);
                parameters.SetValue(oTestBO.LIMSNO, 1);
                parameters.SetValue(oTestBO.ComponentType, 2);
                parameters.SetValue(oTestBO.TestName, 3);
                parameters.SetValue(oTestBO.TestOwner, 4);
                parameters.SetValue(oTestBO.StartDateTime, 5);
                parameters.SetValue(oTestBO.EndDateTime, 6);
                parameters.SetValue(oTestBO.Duration, 7);
                parameters.SetValue(oTestBO.MachineID, 8);

                dsDataSet = DBFactory.RunStoredProcedure("SpUpdateScheduledTest", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                    iDBOperationCode = Convert.ToInt32(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
            return iDBOperationCode;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Delete Scheduled Test based on TestID
        /// </summary>
        /// <returns>List of Test</returns>
        /// --------------------------------------------------------

        public int StatusUpdateToDeleted(int TestID)
        {
            dsDataSet = new DataSet();
            object[] parameters = new object[1];

            try
            {
                parameters.SetValue(TestID, 0);

                dsDataSet = DBFactory.RunStoredProcedure("SpStatusUpdateToDeleted", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                    iDBOperationCode = Convert.ToInt32(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
            return iDBOperationCode;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Call procedure to get all Recent test
        /// </summary>
        /// <returns>List of Scheduled Test</returns>
        /// -------------------------------------------------------

        public List<TestBO> GetAllRecentTest()
        {
            lstTestBO = new List<TestBO>();
            dsDataSet = new DataSet();

            try
            {
                dsDataSet = DBFactory.GetDataSet("SpGetAllRecentTest", null);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oTestBO = new TestBO();

                        oTestBO.TestID = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["TestID"]);
                        oTestBO.LIMSNO = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["LIMSNO"]);
                        oTestBO.ComponentType = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["ComponentType"]);
                        oTestBO.TestName = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["TestName"]);
                        oTestBO.TestOwner = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["TestOwner"]);
                        oTestBO.Status = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["Status"]);
                        if (dsDataSet.Tables[0].Rows[iCount]["StartDateTime"] != DBNull.Value)
                        {
                            oTestBO.StartDateTime = Convert.ToDateTime(dsDataSet.Tables[0].Rows[iCount]["StartDateTime"]);
                        }
                        if (dsDataSet.Tables[0].Rows[iCount]["EndDateTime"] != DBNull.Value)
                        {
                            oTestBO.EndDateTime = Convert.ToDateTime(dsDataSet.Tables[0].Rows[iCount]["EndDateTime"]);
                        }
                        oTestBO.Duration = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["Duration"]);
                        if ((oTestBO.Duration / 60) < 10)
                        {
                            oTestBO.DurationHh = "0" + Convert.ToString(oTestBO.Duration / 60);
                        }
                        else
                        {
                            oTestBO.DurationHh = Convert.ToString(oTestBO.Duration / 60);
                        }
                        if ((oTestBO.Duration % 60) < 10)
                        {
                            oTestBO.DurationMm = "0" + Convert.ToString(oTestBO.Duration % 60);
                        }
                        else
                        {
                            oTestBO.DurationMm = Convert.ToString(oTestBO.Duration % 60);
                        }
                        oTestBO.MachineID = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["MachineID"]);
                        oTestBO.Machine = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["Machine"]);

                        lstTestBO.Add(oTestBO);
                        oTestBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dsDataSet = new DataSet();
            }
            return lstTestBO;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Call procedure to update status to Complete
        /// </summary>
        /// <returns></returns>
        /// -------------------------------------------------------

        public int UpdateStatusToComplete(int TestID)
        {
            object[] parameters = new object[1];

            try
            {
                parameters.SetValue(TestID, 0);

                dsDataSet = DBFactory.RunStoredProcedure("SpUpdateStatusToComplete", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                    iDBOperationCode = Convert.ToInt32(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                dsDataSet = null;
                parameters = null;
            }
            return iDBOperationCode;
        }


        /// -------------------------------------------------------
        /// <summary>
        /// Call procedure to update status to Pause
        /// </summary>
        /// <returns></returns>
        /// -------------------------------------------------------

        public int UpdateStatusToPause(int TestID)
        {
            object[] parameters = new object[1];

            try
            {
                parameters.SetValue(TestID, 0);

                dsDataSet = DBFactory.RunStoredProcedure("SpUpdateStatusToPause", parameters, null);

                if (dsDataSet.Tables.Count > 0)
                {
                    iDBOperationCode = Convert.ToInt32(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                dsDataSet = null;
                parameters = null;
            }
            return iDBOperationCode;
        }



        /// -------------------------------------------------------
        /// <summary>
        /// Call procedure to update status to AboutToComplete
        /// </summary>
        /// <returns>List of Scheduled Test</returns>
        /// -------------------------------------------------------

        public int UpdateStatusToAboutToComplete()
        {
            try
            {
                dsDataSet = DBFactory.GetDataSet("SpUpdateStatusAboutToComplete", null);

                if (dsDataSet.Tables.Count < 0)
                {
                    iDBOperationCode = Convert.ToInt32(dsDataSet.Tables[0].Rows[0]["ErrorNumber"]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dsDataSet = null;
            }
            return iDBOperationCode;
        }

        /// -------------------------------------------------------
        /// <summary>
        /// Call procedure to get all Machine Status data
        /// </summary>
        /// <returns>List of Test</returns>
        /// --------------------------------------------------------

        public List<MachineStatusBO> GetAllMachineStatus()
        {
            lstMachineStatusBO = new List<MachineStatusBO>();
            dsDataSet = new DataSet();

            try
            {
                dsDataSet = DBFactory.GetDataSet("SpGetAllMachineStatus", null);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oMachineStatusBO = new MachineStatusBO();

                        oMachineStatusBO.MachineID = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["MachineID"]);
                        oMachineStatusBO.MachineNoLong = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["MachineNoLong"]);
                        oMachineStatusBO.MachineName = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["MachineName"]);
                        oMachineStatusBO.Status = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["Status"]);
                        if (dsDataSet.Tables[0].Rows[iCount]["AvailableDateTime"] != DBNull.Value)
                        {
                            oMachineStatusBO.AvailableDateTime = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["AvailableDateTime"]);
                        }

                        lstMachineStatusBO.Add(oMachineStatusBO);
                        oMachineStatusBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstMachineStatusBO;
        }

        #endregion

    }
}
