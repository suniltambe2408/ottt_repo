﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using System.Data;
using OTTTBO;
using AppDBFactory;

#endregion " Additional Namespaces "
namespace OTTTDAL
{
    public class AllMachineTestReportDAL
    {

        #region " Class Level Variable "

        DataSet dsDataSet = null;
        int iDBOperationCode = 0;
        List<AllMachineReportBO> lstAllMachineReportBO = null;
        List<MachineMasterBO> lstMachineMasterBO = null;
        AllMachineReportBO oAllMachineReportBO = null;
        MachineMasterBO oMachineMasterBO = null;
        int iCount = 0;
    

        #endregion " Class Level Variable "

        #region "Public Function "

        ///---------------------------------------------------------------------
        /// <summary>
        /// This method call stored procedure to get list of machine numbers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///--------------------------------------------------------------------- 
        public List<MachineMasterBO> GetDistinctMachineNumbers()
        {
            lstMachineMasterBO = new List<MachineMasterBO>();
            dsDataSet = new DataSet();
            object[] parameters = new object[0];

            try
            {
                dsDataSet = DBFactory.RunStoredProcedure("SpGetDistinctMachineNumbers", parameters, null);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oMachineMasterBO = new MachineMasterBO();

                        if (dsDataSet.Tables[0].Rows[iCount]["MachineID"] != DBNull.Value)
                        {
                            oMachineMasterBO.MachineID = Convert.ToInt32(dsDataSet.Tables[0].Rows[iCount]["MachineID"]);
                        }

                        if (dsDataSet.Tables[0].Rows[iCount]["MachineNoLong"] != DBNull.Value)
                        {
                            oMachineMasterBO.MachineNoLong = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["MachineNoLong"]);
                        }

                        lstMachineMasterBO.Add(oMachineMasterBO);
                        oMachineMasterBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstMachineMasterBO;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This method call stored procedure to get list of testowner name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///--------------------------------------------------------------------- 
        public List<AllMachineReportBO> GetDistinctTestOwnerName()
        {
            lstAllMachineReportBO = new List<AllMachineReportBO>();
            dsDataSet = new DataSet();
            object[] parameters = new object[0];

            try
            {
                dsDataSet = DBFactory.RunStoredProcedure("SpGetDistinctTestOwnerName", parameters, null);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oAllMachineReportBO = new AllMachineReportBO();

                        if (dsDataSet.Tables[0].Rows[iCount]["TestOwner"] != DBNull.Value)
                        {
                            oAllMachineReportBO.TestOwner = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["TestOwner"]);
                        }

                        lstAllMachineReportBO.Add(oAllMachineReportBO);
                        oAllMachineReportBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstAllMachineReportBO;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This method call stored procedure to get list of Status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///--------------------------------------------------------------------- 
        public List<AllMachineReportBO> GetDistinctMachineStatus()
        {
            lstAllMachineReportBO = new List<AllMachineReportBO>();
            dsDataSet = new DataSet();
            object[] parameters = new object[0];

            try
            {
                dsDataSet = DBFactory.RunStoredProcedure("SpGetDistinctMachineStatus", parameters, null);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oAllMachineReportBO = new AllMachineReportBO();

                        if (dsDataSet.Tables[0].Rows[iCount]["Status"] != DBNull.Value)
                        {
                            oAllMachineReportBO.Status = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["Status"]);
                        }

                        lstAllMachineReportBO.Add(oAllMachineReportBO);
                        oAllMachineReportBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstAllMachineReportBO;
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This method call stored procedure to get list of Status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///--------------------------------------------------------------------- 
        public List<AllMachineReportBO> GetDistinctDepartment()
        {
            lstAllMachineReportBO = new List<AllMachineReportBO>();
            dsDataSet = new DataSet();
            object[] parameters = new object[0];

            try
            {
                dsDataSet = DBFactory.RunStoredProcedure("SpGetDistinctDepartment", parameters, null);

                if (dsDataSet != null)
                {
                    while (dsDataSet.Tables[0].Rows.Count > iCount)
                    {
                        oAllMachineReportBO = new AllMachineReportBO();

                        if (dsDataSet.Tables[0].Rows[iCount]["Department"] != DBNull.Value)
                        {
                            oAllMachineReportBO.Department = Convert.ToString(dsDataSet.Tables[0].Rows[iCount]["Department"]);
                        }

                        lstAllMachineReportBO.Add(oAllMachineReportBO);
                        oAllMachineReportBO = null;
                        iCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                dsDataSet = null;
            }
            return lstAllMachineReportBO;
        }

        #endregion "Public Function "
    }
}
