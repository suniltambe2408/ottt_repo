﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;

namespace OTTTWeb
{
    public partial class Home : System.Web.UI.Page
    {
        static TraceUtility oTraceUtility = new TraceUtility();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                oTraceUtility.TraceService("Home.aspx : Page_Load");

                if (!Page.IsPostBack)
                {
                    if (Convert.ToInt32(Session["IsValidUser"]) == 1)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                oTraceUtility.TraceService("Home.aspx : Exception: " + ex.Message);

                //lblMessage.Text = "Error : Please contact application administrator" + ex.Message;
                //lblMessage.ForeColor = System.Drawing.Color.Red;
            }
            finally
            {

            }
        }
    }
}