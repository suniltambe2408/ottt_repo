﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.Master" AutoEventWireup="true"
    CodeBehind="MachineMaster.aspx.cs" Inherits="TTTWeb.MachineMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/jquery-1.11.3.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            var evt = null;
            isNumberQuantity(evt);
            isName(evt);
            isAlphabetWithSpace(evt);
            isNumberAlphabet(evt);
            isAlphabet(evt);
            TrackingNumber(evt);
            isParkingNumber(evt);


            $('#<%=btnMachineSave.ClientID %>').click(function () {
                $('#<%=lblMachineNoError.ClientID %>').html("");
                $('#<%=txtMachineNoRFV.ClientID %>').html("Please enter machine number");
                $('#<%=lblMachineNameError.ClientID %>').html("");
                $('#<%=txtMachineNameRFV.ClientID %>').html("Please enter machine name");
                $('#<%=lblMachineTypeError.ClientID %>').html("");
                $('#<%=txtMachineTypeRFV.ClientID %>').html("Please enter machine type");

                $('#<%=lblRoomNameError.ClientID %>').html("");
            });

            $('#<%=btnSearch.ClientID %>').click(function () {
                $('#<%=lblSearchError.ClientID %>').html("");
                $('#<%=txtSearchRFV.ClientID %>').html("Enter keyword");
            });

        });

        function isNumberQuantity(evt) {
            if (null != evt) {
                evt = (evt) ? evt : window.event;

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    $('#<%=lblMachineNoError.ClientID %>').html("Please enter number only");
                    $('#<%=txtMachineNoRFV.ClientID %>').html("");


                    return false;
                }
                else {
                    $('#<%=lblMachineNoError.ClientID %>').html("");
                }
            }
            return true;
        }


        function isNumberQuantitySearch(evt) {
            if (null != evt) {
                evt = (evt) ? evt : window.event;

                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if ((charCode >= 65 && charCode <= 90)
                    || (charCode >= 97 && charCode <= 122)
                    || (charCode >= 48 && charCode <= 57)
                    || (charCode == 95) || (charCode == 32)) {
                    return true;
                    $('#<%=lblSearchError.ClientID %>').html("Enter alpha numeric value!");
                    $('#<%=txtSearchRFV.ClientID %>').html("");

                }
                else {

                    $('#<%=lblSearchError.ClientID %>').html("");
                    return false;
                }
            }
            return false;
        }

        function isAlphabetWithSpace(evt) {
            if (null != evt) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {

                    if (charCode == 32) {
                        return true;
                    }
                    $('#<%=lblMachineNameError.ClientID %>').html("Please enter alphabets only");
                    $('#<%=txtMachineNameRFV.ClientID %>').html("");
                    return false;
                }
                else {
                    $('#<%=lblMachineNameError.ClientID %>').html("");
                }

            }
            return true;
        }

        function isAlphabetWithSpaceMachineType(evt) {
            if (null != evt) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {

                    if (charCode == 32) {
                        return true;
                    }
                    $('#<%=lblMachineTypeError.ClientID %>').html("Please enter alphabets only");
                    $('#<%=txtMachineTypeRFV.ClientID %>').html("");
                    return false;
                }
                else {
                    $('#<%=lblMachineTypeError.ClientID %>').html("");
                }

            }
            return true;
        }


        function isAlphabetWithSpaceRoomName(evt) {
            if (null != evt) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {

                    if (charCode == 32) {
                        return true;
                    }
                    $('#<%=lblRoomNameError.ClientID %>').html("Please enter alphabets only");
                    return false;
                }
                else {
                    $('#<%=lblRoomNameError.ClientID %>').html("");
                }

            }
            return true;
        }

        function isName(evt) {
            if (evt != null) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if ((charCode >= 65 && charCode <= 90)
                    || (charCode >= 97 && charCode <= 122)
                    || (charCode >= 48 && charCode <= 57)
                    || (charCode == 95) || (charCode == 32)) {
                    return true;
                }

                else {

                    return false;
                }

            } else {

                return false;
            }
        }

        function isNumberAlphabet(evt) {
            if (null != evt) {
                evt = (evt) ? evt : window.event;

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {

                    return false;
                }

            }

            return true;
        }

        function isAlphabet(evt) {
            if (null != evt) {
                evt = (evt) ? evt : window.event;

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {

                    return false;
                }

            }
            return true;
        }

        function TrackingNumber(evt) {
            if (null != evt) {
                evt = (evt) ? evt : window.event;

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode == 84) {
                        return true;
                    }
                    if (charCode == 116) {
                        return true;
                    }
                    return false;
                }

            }
            return true;
        }

        function isParkingNumber(evt) {

            if (null != evt) {
                evt = (evt) ? evt : window.event;

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode == 45) {
                        return true;
                    }
                    return false;
                }

            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="height: 50px; width: 100%">
                <tr align="right">
                    <td>
                        <asp:Image ID="imgPinIcon" runat="server" Width="20" Height="20" ImageUrl="~/Images/ico_mand.png" />
                        <asp:Label ID="Label15" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        <asp:Label ID="lblManField" runat="server" Text="Mandatory Fields"></asp:Label>
                    </td>
                <tr>
                    <td width="20%"  align="center">
                        <asp:Label ID="lblMessage" runat="server" height="12px" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>
            <div align="center" style="width: 100%;">
                <table class="table" style="width: 45%; border: thin solid #ADD8E6;">
                    <tr style="border-color: #ADD8E6">
                        <td colspan="4" align="center" style="width: 20%; background-color: #07538B; font-size: 20px; color: White; font-family: Calibri;">Machine Master
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%" align="right">
                            <asp:Label ID="lblMachineNo" runat="server" Text="Machine Number"></asp:Label>
                            <asp:Label ID="lblMachineNoStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        </td>
                        <td style="width: 30%" align="left">
                            <asp:TextBox ID="txtMachineNo" runat="server" Width="97%" MaxLength="11" CssClass="uppercase" onkeypress="return isName(event)" OnTextChanged="txtMachineNo_TextChanged" AutoPostBack="true"></asp:TextBox>
                        </td>
                        <td style="width: 3%" align="center">
                            <asp:Image ID="imgTTType" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                ToolTip="Enter machine number. Example: COR_QCT_603" />
                        </td>
                        <td>

                            <asp:RequiredFieldValidator ID="txtMachineNoRFV" runat="server" ErrorMessage="Please enter machine number"
                                ControlToValidate="txtMachineNo" ValidationGroup="vgrpSaveMachine" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblMachineNoError" runat="server" Text="" ForeColor="Red"></asp:Label>
                            <asp:Label ID="lblDuplidate" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td style="width: 30%" align="right">
                            <asp:Label ID="lblMachineName" runat="server" Text="Machine Name"></asp:Label>
                            <asp:Label ID="lblMachineNameStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        </td>
                        <td style="width: 30%" align="left">
                            <asp:TextBox ID="txtMachineName" runat="server" Width="97%" MaxLength="100" onkeypress="return isName(event)"></asp:TextBox>
                        </td>
                        <td style="width: 3%" align="center">
                            <asp:Image ID="Image1" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                ToolTip="Enter machine name. Example: Vacuum Oven" />
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="txtMachineNameRFV" runat="server" ErrorMessage="Please enter machine name"
                                ControlToValidate="txtMachineName" ValidationGroup="vgrpSaveMachine" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblMachineNameError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%" align="right">
                            <asp:Label ID="lblMachineType" runat="server" Text="Machine Type"></asp:Label>
                            <asp:Label ID="lblMachineTypeStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        </td>
                        <td style="width: 30%" align="left">
                            <asp:TextBox ID="txtMachineType" runat="server" Width="97%" MaxLength="50" onkeypress="return isAlphabetWithSpaceMachineType(event)"></asp:TextBox>
                        </td>
                        <td style="width: 3%" align="center">
                            <asp:Image ID="Image2" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                ToolTip="Enter machine type. Example: Chamber/Oven" />
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="txtMachineTypeRFV" runat="server" ErrorMessage="Please enter machine type"
                                ControlToValidate="txtMachineType" ValidationGroup="vgrpSaveMachine" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblMachineTypeError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%" align="right">
                            <asp:Label ID="lblRoomName" runat="server" Text="Room Name"></asp:Label>
                        </td>
                        <td style="width: 30%" align="left">
                            <asp:TextBox ID="txtRoomName" runat="server" Width="97%" MaxLength="100" onkeypress="return isName(event)"></asp:TextBox>
                        </td>
                        <td style="width: 3%" align="center">
                            <asp:Image ID="Image3" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                ToolTip="Enter room name where machine is installed. Example: Corrosion Test" />
                        </td>
                        <td>
                            <asp:Label ID="lblRoomNameError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%" align="right">
                            <asp:Label ID="lblIsActive" runat="server" Text="Is Active"></asp:Label>
                        </td>
                        <td style="width: 30%" align="left">
                            <asp:CheckBox ID="chkIsActive" runat="server" />
                        </td>
                        <td style="width: 3%" align="left">
                            <asp:Image ID="Image4" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                ToolTip="Check to keep this machine active" />
                        </td>

                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="right">
                            <asp:Button ID="btnMachineSave" runat="server" Text="Save" BackColor="#03518B" ForeColor="White"
                                ValidationGroup="vgrpSaveMachine" OnClick="btnMachineSave_Click" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" BackColor="#03518B" ForeColor="White"
                                OnClick="btnCancel_Click" />
                        </td>
                        <td></td>
                        <td></td>
                    </tr>


                </table>
                <%--</div>--%>
                <table class="table" style="width: 75%; margin-top: 20px; border: thin solid #ADD8E6;">
                    <tr style="border-color: #ADD8E6">
                        <td colspan="4" align="center" style="width: 20%; background-color: #07538B; font-size: 20px; color: White; font-family: Calibri;">Machine List
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left" >
                            <asp:Label ID="Label1" runat="server" style="margin-left:20px" Text="Machine Number/Name"></asp:Label>
                            <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <asp:TextBox ID="txtSearch" runat="server" Width="230px" onkeypress="return isNumberQuantitySearch(event)" MaxLength="11"></asp:TextBox>
                            <asp:Button ID="btnSearch" runat="server" Text="Search" BackColor="#03518B" Style="margin-left: 5px"
                                ForeColor="White" OnClick="btnSearch_Click" ValidationGroup="vgrpSearchMachine" />

                            <asp:RequiredFieldValidator ID="txtSearchRFV" runat="server" ErrorMessage="Enter Machine Number"
                                ControlToValidate="txtSearch" ValidationGroup="vgrpSearchMachine" ForeColor="Red" Display="Dynamic" Style="margin-left: 20px"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblSearchError" runat="server" Text="" ForeColor="Red" Style="margin-left: 20px"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gvMachineMaster" runat="server" AutoGenerateColumns="false" DataKeyNames="MachineID"
                                Width="100%" AllowPaging="true" PageSize="5" OnSelectedIndexChanged="gvMachineMaster_SelectedIndexChanged"
                                OnPageIndexChanging="gvMachineMaster_PageIndexChanging">
                                <%-- OnRowDeleting="gvMachineMaster_RowDeleting"--%>
                                <Columns>

                                    <asp:TemplateField HeaderText="Machine No" ItemStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMachineID" runat="server" Visible="false" Text='<%# Bind("MachineID") %>'></asp:Label>
                                            <asp:Label ID="lblMachineNO" runat="server" Text='<%# Bind("MachineNoLong") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Machine Name" ItemStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMachineName" runat="server" Text='<%# Bind("MachineName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Machine Type" ItemStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMachineType" runat="server" Text='<%# Bind("MachineType") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Room Name" ItemStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRoomName" runat="server" Text='<%# Bind("RoomName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Is Active" ItemStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gvIsActive" runat="server" Enabled="false" Checked='<%# Eval("IsActive") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Select"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>



                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <EditRowStyle BackColor="White" />
                                <FooterStyle BackColor="#5D7B9D" ForeColor="White" />
                                <HeaderStyle BackColor="#5D7B9D" ForeColor="White" />
                                <%--<PagerSettings Mode="NextPrevious" NextPageText="Next" PreviousPageText="Previous" />--%>
                                <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#E2DED6" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />

                            </asp:GridView>
                        </td>
                    </tr>
                </table>

                <asp:Label ID="lblMachineID" runat="server" Visible="false"></asp:Label>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnMachineSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>
