﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.Master" AutoEventWireup="true"
    CodeBehind="Dashboard.aspx.cs" Inherits="TTTWeb.Dashboard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .circle
        {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            background-color: DEEPSKYBLUE;
        }
    </style>


    <script type="text/javascript">

        function ClearStartDateTimeError() {
            $('#<%=lblMessage.ClientID %>').html("");
        }



        function Clear() {
            $('#<%=lblMessage.ClientID %>').html("");
        }

        function isNumberQuantity(evt) {

            if (null != evt) {
                evt = (evt) ? evt : window.event;

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode == 47) {
                        return true;
                    }
                    $('#<%=lblMessage.ClientID %>').html("Please enter numbers only");
                    return false;
                }
                else {
                    $('#<%=lblMessage.ClientID %>').html("");
                }
            }
            return true;
        }

        function ConfirmStart() {
            if (confirm('Are you sure you want to start this test?') == true)
                return true;
            else
                return false;
        }

        function ConfirmDelete() {

            if (confirm('Are you sure you want to delete this test?') == true)
                return true;
            else
                return false;
        }


        function ConfirmEnd() {

            if (confirm('Are you sure you want to end this test?') == true)
                return true;
            else
                return false;
        }

        function isAlphabetWithSpace(evt) {
            if (null != evt) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {

                    //if (charCode == 95) {
                    //    return true;
                    //}
                    //else if (charCode == 96) {
                    //    return true;
                    //}
                    if (charCode == 32) {
                        return true;
                    }
                    $('#<%=lblMessage.ClientID %>').html("Please enter alphabets only");

                    return false;
                }
                else {
                    $('#<%=lblMessage.ClientID %>').html("");
                }

            }
            return true;
        }

        function onKeyDownDate(event) {

            if (null != event) {

                $('#<%=lblMessage.ClientID %>').html("Please select date from calendar.");
                return false;
            }


        }

        function checkDate(sender, args) {
            if (sender._selectedDate < new Date()) {
                alert("Start date cannot be less than today's date!!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div align="right" style="margin-top: 30px;">

        <asp:Label ID="lblDate" runat="server" Text="Date: " align="right"></asp:Label>
        <asp:Label ID="lblDateTime" runat="server" align="right"></asp:Label>
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <p style="width: 100%; text-align: center; background-color: #07538B; font-size: 20px; color: White; font-family: Calibri; margin: 6px">
        <b>Dashboard</b>

    </p>


    <fieldset style="padding: 10px; width: 96%; margin-left: 15px; margin-top: 5px; border: thin solid #ADD8E6">
        <legend style="font-size: medium"><strong>Recent Tests Results</strong>
        </legend>
        <div>
            

            <div style="height: 10px">

                <div align="right" style="float: left; width: 660px;">
                    <asp:Label ID="lblMessageError" runat="server" Text="" ForeColor="Red"></asp:Label>
                </div>

                <div align="right" style="margin-bottom: 5px; float: right; width: 660px;">
                    <asp:Button ID="btnStarted" runat="server" BorderColor="Black" BorderWidth="1" Style="height: 15px; width: 15px; border-radius: 100%; -moz-border-radius: 100%; -webkit-border-radius: 100%;"
                        BackColor="#07a3e2" Enabled="false" />
                    <asp:Label ID="lblStarted" runat="server">Started</asp:Label>

                    <asp:Button ID="btnAboutToComplete" runat="server" BorderColor="Black" BorderWidth="1" Style="margin-left: 15px; height: 15px; width: 15px; border-radius: 100%; -moz-border-radius: 100%; -webkit-border-radius: 100%;"
                        BackColor="#ffff00" Enabled="false" />
                    <asp:Label ID="lblAboutToComplete" runat="server">About To Complete</asp:Label>

                    <asp:Button ID="btnComplete" runat="server" BorderColor="Black" BorderWidth="1" Style="margin-left: 15px; height: 15px; width: 15px; border-radius: 100%; -moz-border-radius: 100%; -webkit-border-radius: 100%;"
                        BackColor="#00b74a" Enabled="false" />
                    <asp:Label ID="lblComplete" runat="server">Complete</asp:Label>

                </div>
            </div>

            <%-- <div align="right" style="margin-bottom: 5px">
               
            </div>--%>
            <asp:UpdatePanel ID="UpdateRecentTestResult" runat="server">

                <ContentTemplate>
                    <asp:GridView ID="gvRecentTestResult" runat="server" AutoGenerateColumns="false" DataKeyNames="TestID" Width="100%" AllowPaging="true" PageSize="4"
                        OnPageIndexChanging="gvRecentTestResult_PageIndexChanging" OnRowCommand="gvRecentTestResult_RowCommand" OnRowDataBound="gvRecentTestResult_RowDataBound">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <EditRowStyle BackColor="White" />
                        <FooterStyle BackColor="#5D7B9D" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" ForeColor="White" />
                        <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                        <Columns>

                            <asp:TemplateField HeaderText="LIMS NO">
                                <ItemTemplate>
                                    <asp:Label ID="lblTestID" runat="server" Visible="false" Text='<%#Eval("TestID") %>'></asp:Label>
                                    <asp:Label ID="lblScheduleLIMSNO" runat="server" Text='<%#Eval("LIMSNO") %>'></asp:Label>

                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Height="15px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Component Type">
                                <ItemTemplate>
                                    <asp:Label ID="lblComponentType" runat="server" Text='<%#Eval("ComponentType") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Test Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblRTestName" runat="server" Text='<%#Eval("TestName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Test Owner">
                                <ItemTemplate>
                                    <asp:Label ID="lblRecentTestOwner" runat="server" Text='<%#Eval("TestOwner") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>' Visible="false"></asp:Label>
                                    <asp:Button ID="btnStartedTest" runat="server" BorderColor="Black" BorderWidth="1" Style="height: 15px; width: 15px; border-radius: 100%; -moz-border-radius: 100%; -webkit-border-radius: 100%;"
                                        BackColor="#07a3e2" Enabled="false" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start Date & Time">
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDateTime" runat="server" Text='<%#Eval("StartDateTime") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End Date & Time">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDateTime" runat="server" Text='<%#Eval("EndDateTime") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Duration(HH:MM)">
                                <ItemTemplate>
                                    <asp:Label ID="lblDuration" runat="server" Text='<%# string.Concat( Eval("DurationHh"), ":", Eval("DurationMm")) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDuration" runat="server" Text='<%#Eval("Duration") %>' Width="80px"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Machine NO">
                                <ItemTemplate>
                                    <asp:Label ID="lblMachineNO" runat="server" Text='<%#Eval("Machine") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                                <%--  <EditItemTemplate>
                                    <asp:TextBox ID="txtMachineNO" runat="server" Text='<%#Eval("MachineID") %>' Width="80px"></asp:TextBox>
                                </EditItemTemplate>--%>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEndTest" runat="server" Text="End" CommandName="lnkEndTest" CommandArgument='<%#Eval("TestID") %>' OnClientClick="return ConfirmEnd()"></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>

                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Interval="60000"></asp:Timer>



        </div>
    </fieldset>

    <fieldset style="padding: 10px; width: 96%; margin-left: 15px; margin-top: 10px; border: thin solid #ADD8E6">
        <legend style="font-size: medium"><strong>Scheduled Tests</strong>
        </legend>
        <div style="height: 10px">

            <div align="right" style="float: left; width: 660px;">
                <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            </div>

            <div align="right" style="margin-bottom: 5px; float: right; width: 660px;">
                <asp:Button ID="btnScheduled" runat="server" BorderColor="Black" BorderWidth="1" Style="height: 15px; width: 15px; border-radius: 100%; -moz-border-radius: 100%; -webkit-border-radius: 100%;"
                    BackColor="White" Enabled="false" />
                <asp:Label ID="lblScheduled" runat="server">Scheduled</asp:Label>
            </div>
        </div>

        <asp:UpdatePanel ID="UpdateScheduledTest" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvScheduledTests" runat="server" AutoGenerateColumns="false" DataKeyNames="TestID" AllowPaging="true" PageSize="4" Width="100%" OnRowCommand="gvScheduledTests_RowCommand"
                    OnRowEditing="gvScheduledTests_RowEditing" OnRowCancelingEdit="gvScheduledTests_RowCancelingEdit" OnRowUpdating="gvScheduledTests_RowUpdating"
                    OnPageIndexChanging="gvScheduledTests_PageIndexChanging" OnRowDataBound="gvScheduledTests_RowDataBound">
                    <%--OnRowDeleting="gvScheduledTests_RowDeleting"--%>
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <EditRowStyle BackColor="White" />
                        <FooterStyle BackColor="#5D7B9D" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" ForeColor="White" />
                        <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    <Columns>

                        <asp:TemplateField HeaderText="LIMS NO">
                            <ItemTemplate>
                                <asp:Label ID="lblTestID" runat="server" Visible="false" Text='<%#Eval("TestID") %>'></asp:Label>
                                <asp:Label ID="lblScheduleLIMSNO" runat="server" Text='<%#Eval("LIMSNO") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="80px" Height="20px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Component Type">
                            <ItemTemplate>
                                <asp:Label ID="lblScheduleComponentType" runat="server" Text='<%#Eval("ComponentType") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="120px" HorizontalAlign="Center" />
                            <EditItemTemplate>
                                <asp:TextBox ID="txtScheduleComponentType" runat="server" Text='<%#Eval("ComponentType") %>' Width="100px" 
                                    onkeypress="return isAlphabetWithSpace(event)"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvgvComponentType" runat="server" ErrorMessage="*" ForeColor="Red" ValidationGroup="gvrfvValidate" 
                                                    ControlToValidate="txtScheduleComponentType"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Test Name">
                            <ItemTemplate>
                                <asp:Label ID="lblTestName" runat="server" Text='<%#Eval("TestName") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="120px" HorizontalAlign="Center" />
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTestName" runat="server" Text='<%#Eval("TestName") %>' Width="100px" 
                                    onkeypress="return isAlphabetWithSpace(event)"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvgvTestName" runat="server" ErrorMessage="*" ForeColor="Red" ValidationGroup="gvrfvValidate" 
                                                    ControlToValidate="txtTestName"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Test Owner">
                            <ItemTemplate>
                                <asp:Label ID="lblTestOwner" runat="server" Text='<%#Eval("TestOwner") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="100px" HorizontalAlign="Center" />
                            <EditItemTemplate>
                                <asp:TextBox ID="txtTestOwner" runat="server" Text='<%#Eval("TestOwner") %>' Width="80px" 
                                    onkeypress="return isAlphabetWithSpace(event)"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvgvTestOwner" runat="server" ErrorMessage="*" ForeColor="Red" ValidationGroup="gvrfvValidate" 
                                                    ControlToValidate="txtTestOwner"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemStyle Width="60px" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Button ID="btnScheduled" runat="server" BorderColor="Black" BorderWidth="1" 
                                    Style="height: 15px; width: 15px; border-radius: 100%; -moz-border-radius: 100%; -webkit-border-radius: 100%;"
                                    BackColor="White" Enabled="false" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Start Date & Time">
                            <ItemTemplate>
                                <asp:Label ID="lblStartDateTime" runat="server" Text='<%#Eval("StartDateTime") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="200px" HorizontalAlign="Center" />
                            <EditItemTemplate>
                                <asp:TextBox ID="txtStartDateTime" runat="server" Text='<%#Eval("StartDateTime") %>' Width="69px" onkeypress="return onKeyDownDate(event)" onblur="ClearStartDateTimeError();"></asp:TextBox>
                                <asp:ImageButton ID="imgCalender" runat="server" ImageUrl="~/Images/calendar.jpg" />
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server"
                                    TargetControlID="txtStartDateTime" PopupButtonID="imgCalender" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate">
                                </cc1:CalendarExtender>
                                <asp:RequiredFieldValidator ID="rfvgvTestStartDate" runat="server" ErrorMessage="*" ForeColor="Red" ValidationGroup="gvrfvValidate" 
                                                    ControlToValidate="txtStartDateTime"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtStartTimeHrs" runat="server" MaxLength="2" AutoPostBack="true" Text='<%# Eval("StartTimeHh") %>' Width="20px" TabIndex="9" OnTextChanged="txtStartTimeHrs_TextChanged"></asp:TextBox>
                                                <asp:Label ID="lblHrMinSeperator" runat="server" Text=":"></asp:Label>
                                                <asp:TextBox ID="txtStartTimeMins" runat="server" MaxLength="2" AutoPostBack="true" Text='<%# Eval("StartTimeMm") %>' Width="20px" TabIndex="9" OnTextChanged="txtStartTimeMins_TextChanged"></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End Date & Time">
                            <ItemTemplate>
                                <asp:Label ID="lblEndtDateTime" runat="server" Text='<%#Eval("EndDateTime") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="160px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Duration(HH:MM)">
                            <ItemTemplate>
                                <asp:Label ID="lblDuration" runat="server" Text='<%# string.Concat( Eval("DurationHh"), ":", Eval("DurationMm")) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="80px" HorizontalAlign="Center" />
                            <EditItemTemplate>
                                <asp:TextBox ID="gvtxtDurationHrs" runat="server" MaxLength="4" Width="20px" AutoPostBack="true" Height="15px" Text='<%# Eval("DurationHh") %>' OnTextChanged="gvtxtDurationHrs_TextChanged"></asp:TextBox>
                                    <asp:Label ID="gvlblDurationHhMmSeperator" runat="server" Text=":"></asp:Label>
                                    <asp:TextBox ID="gvtxtDurationMins" runat="server" MaxLength="2" AutoPostBack="true" Height="15px" Width="20px" Text='<%# Eval("DurationMm") %>' OnTextChanged="gvtxtDurationMins_TextChanged"></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Machine NO">
                            <ItemTemplate>
                                <asp:Label ID="lblMachineNO" runat="server" Text='<%#Eval("Machine") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="100px" HorizontalAlign="Center" />
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlMachineNumber" runat="server" Width="130px"></asp:DropDownList>
                                <asp:Label ID="lblMachineNumber" runat="server" Text='<%#Eval("Machine") %>' Style="display: none"></asp:Label>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemStyle Width="150px" />
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkStartTest" runat="server" Text="Start" CommandName="lnkStartTest" CommandArgument='<%#Eval("TestID") %>' OnClientClick="return ConfirmStart()"></asp:LinkButton>&nbsp;
                        <asp:LinkButton ID="lnkEditTest" runat="server" Text="Edit" CommandName="Edit"></asp:LinkButton>&nbsp;
                        <asp:LinkButton ID="lnkDeleteTest" runat="server" Text="Delete" CommandName="lnkDeleteTest" CommandArgument='<%#Eval("TestID") %>' OnClientClick="return ConfirmDelete()"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkUpdateTest" runat="server" Text="Update" CommandName="Update" ValidationGroup="gvrfvValidate" OnClientClick="Clear()"></asp:LinkButton>
                                <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="Cancel" OnClientClick="Clear()"></asp:LinkButton>
                            </EditItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <PagerStyle HorizontalAlign="Center" />
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
            </Triggers>
        </asp:UpdatePanel>



    </fieldset>

    <fieldset style="padding: 10px; width: 25%; margin-left: 15px; margin-top: 10px; border: thin solid #ADD8E6;">
        <legend style="font-size: medium"><strong>Machine Status</strong>
        </legend>
        <div align="right" style="margin: 5px">

            <asp:Button ID="btnEmpty" runat="server" BorderColor="Black" BorderWidth="1" Style="height: 15px; width: 15px; border-radius: 100%; -moz-border-radius: 100%; -webkit-border-radius: 100%;"
                BackColor="White" Enabled="false" />
            <asp:Label ID="lblEmpty" runat="server">Empty</asp:Label>
            &nbsp;&nbsp;
            <asp:Button ID="btnInUse" runat="server" BorderColor="Black" BorderWidth="1" Style="height: 15px; width: 15px; border-radius: 100%; -moz-border-radius: 100%; -webkit-border-radius: 100%;"
                BackColor="#07a3e2" Enabled="false" />
            <asp:Label ID="lblInUse" runat="server">In Use</asp:Label>
        </div>
        <asp:UpdatePanel ID="UpdateMachineStatus" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvMachineStatus" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="4" OnPageIndexChanging="gvMachineStatus_PageIndexChanging" OnRowDataBound="gvMachineStatus_RowDataBound">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <EditRowStyle BackColor="White" />
                    <FooterStyle BackColor="#5D7B9D" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" ForeColor="White" />
                    <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    <Columns>
                        <asp:TemplateField HeaderText="Machine No" ItemStyle-Width="100px">
                            <ItemTemplate>
                                <asp:Label ID="lblMachineID" runat="server" Text='<%#Eval("MachineNoLong") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Height="15px" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Machine Name" ItemStyle-Width="200px">
                            <ItemTemplate>
                                <asp:Label ID="lblMachineName" runat="server" Text='<%#Eval("MachineName")%>'></asp:Label><%--<%#Eval("MachineDesc") %>--%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>' Visible="false"></asp:Label>
                                <asp:Button ID="btnInUSe" runat="server" BorderColor="Black" BorderWidth="1" Style="height: 15px; width: 15px; border-radius: 100%; -moz-border-radius: 100%; -webkit-border-radius: 100%;"
                                    BackColor="White" Enabled="false" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Availabe Date & Time" ItemStyle-Width="200px">
                            <ItemTemplate>
                                <asp:Label ID="lblAvailable" runat="server" Text='<%#Eval("AvailableDateTime") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>--%>
                    </Columns>
                    <PagerStyle HorizontalAlign="Center" />
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
            </Triggers>
        </asp:UpdatePanel>


    </fieldset>
  
</asp:Content>
