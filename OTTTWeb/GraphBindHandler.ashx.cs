﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using OTTTBLL;
using OTTTBO;
using System.Data;
using System.Web.UI.DataVisualization.Charting;
using System.Web.Script.Serialization;
using System.Text;
using System.Configuration;

namespace OTTTWeb
{
    /// <summary>
    /// Summary description for GraphBindHandler
    /// </summary>
    public class GraphBindHandler : IHttpHandler
    {
        #region Class Level Variable

        MonitoringBLL oMonitoringBLL = null;
        MonitoringBO oMonitoringBO = null;

        TestBLL oTestBLL = null;
        TestBO oTestBO = null;

        MachineMasterBLL oMachineMasterBLL = null;
        DataTable dtTable = null;
        DataSet dsDataset = null;
        List<MonitoringDataBO> lstMonitoringDataBO = null;
        List<MachineMasterBO> lstMachineMasterBO = null;
        List<MonitoringBO> lstMonitoringBO = null;

        #endregion

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");          
            string json = this.BindGraphData();
            context.Response.ContentType = "text/json";
            context.Response.Write(json);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        protected string BindGraphData()
        {
            oMonitoringBLL = new MonitoringBLL();
            lstMonitoringBO = new List<MonitoringBO>();
            dsDataset = new DataSet();
            dtTable = new DataTable();
            lstMonitoringDataBO = new List<MonitoringDataBO>();
            List<MonitoringBO>  lstMonitoringBO2 = new List<MonitoringBO>();
            oMachineMasterBLL = new MachineMasterBLL();
            oTestBLL = new TestBLL();
            try
            {
                lstMonitoringBO = oMachineMasterBLL.GetAllMachineNumbers();
                if (lstMonitoringBO.Count != 0)
                {   
                    foreach (var row in lstMonitoringBO)
                    {
                        oMonitoringBO = row;
                        lstMonitoringDataBO = new List<MonitoringDataBO>();
                        int MachineNumber = row.MachineNumber;
                        if (MachineNumber > 0)
                        {
                            oMonitoringBO.MachineNumber = MachineNumber;
                            // TODO - CHECK IF TEST IS RUNNING IN MACHINE
                            int TestCount = oTestBLL.GetTestCountForMachineNumber(MachineNumber);
                            if (TestCount <= 0)
                            {
                                // TODO - IF NO TEST IS RUNNING THEN SET TEST DATA AS NULL TO AVOID GRAPH PLOTTING   
                                lstMonitoringDataBO = null;
                                oMonitoringBO.lstMonitoringDataBO = lstMonitoringDataBO;
                                oMonitoringBO.IsAlertRequired = false;
                                oMonitoringBO.AlertMessage = "Machine " + MachineNumber + " not in use!";
                                lstMonitoringBO2.Add(oMonitoringBO);
                            }
                            else
                            {
                                // TODO - IF TEST IS RUNNING THEN GET TEST DATA                                
                                String sFieldValues = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings[Convert.ToString(MachineNumber)]);

                                lstMonitoringDataBO = oMonitoringBLL.GetAllGraphData(sFieldValues);
                                oMonitoringBO.lstMonitoringDataBO = lstMonitoringDataBO;

                                MonitoringDataBO latestData = lstMonitoringDataBO[0];

                                if (MachineNumber == 602)
                                {
                                    //if (latestData.C_Humidity_602 > latestData.C_Temp_602 && latestData.H_Temp_602 > latestData.W_Temp_602)
                                    //{
                                    //    oMonitoringBO.IsAlertRequired = true;
                                    //    oMonitoringBO.AlertMessage = "Temperature going above Set Limit! Please check.";
                                    //}

                                    if ((latestData.C_Humidity_602 == 0 || latestData.C_Humidity_602 == null) && (latestData.C_Temp_602 == 0 || latestData.C_Temp_602 == null) && (latestData.H_Temp_602 == 0 || latestData.H_Temp_602 == null) && (latestData.W_Temp_602 == 0 || latestData.W_Temp_602 == null))
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Machine " + MachineNumber + " not sending data!  Test stopped.";
                                        // Set Status of All Tests Running on the Machine as FAULT
                                        oTestBLL.MachineFaultOccurred(MachineNumber);
                                    }
                                }
                                else if (MachineNumber == 603)
                                {
                                    if (latestData.T_PV_603 > latestData.T_SP_603)
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Temperature going above Set Limit! Please check.";
                                    }
                                    
                                    if ((latestData.T_PV_603 == 0 || latestData.T_PV_603 == null))
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Machine " + MachineNumber + " not sending data! Test stopped.";
                                        // Set Status of All Tests Running on the Machine as FAULT
                                        oTestBLL.MachineFaultOccurred(MachineNumber);
                                    }
                                }
                                else if (MachineNumber == 604)
                                {
                                    if (latestData.T_PV_604 > latestData.T_SP_604)
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Temperature going above Set Limit! Please check.";
                                    }

                                    if ((latestData.T_PV_604 == 0 || latestData.T_PV_604 == null))
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Machine " + MachineNumber + " not sending data! Test stopped..";
                                        // Set Status of All Tests Running on the Machine as FAULT
                                        oTestBLL.MachineFaultOccurred(MachineNumber);
                                    }
                                }
                                else if (MachineNumber == 605)
                                {
                                    if (latestData.T_PV_605 > latestData.T_SP_605)
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Temperature going above Set Limit! Please check.";
                                    }
                                    
                                    if ((latestData.T_PV_605 == 0 || latestData.T_PV_605 == null))
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Machine " + MachineNumber + " not sending data! Test stopped.";
                                        // Set Status of All Tests Running on the Machine as FAULT
                                        oTestBLL.MachineFaultOccurred(MachineNumber);
                                    }
                                }
                                else if (MachineNumber == 606)
                                {
                                    if (latestData.T_PV_606 > latestData.T_SP_606)
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Temperature going above Set Limit! Please check.";
                                    }

                                    if ((latestData.T_PV_606 == 0 || latestData.T_PV_606 == null))
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Machine " + MachineNumber + " not sending data! Test stopped.";
                                        // Set Status of All Tests Running on the Machine as FAULT
                                        oTestBLL.MachineFaultOccurred(MachineNumber);
                                    }
                                }
                                else if (MachineNumber == 704)
                                {
                                    if (latestData.T_PV_704 > latestData.T_SP_704)
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Temperature going above Set Limit! Please check.";
                                    }

                                    if ((latestData.T_PV_704 == 0 || latestData.T_PV_704 == null))
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Machine " + MachineNumber + " not sending data! Test stopped.";
                                        // Set Status of All Tests Running on the Machine as FAULT
                                        oTestBLL.MachineFaultOccurred(MachineNumber);
                                    }
                                }
                                else if (MachineNumber == 706)
                                {
                                    if ((latestData.H_PV_706 > latestData.HM_SP_706) || (latestData.T_PV_706 > latestData.TA_SP_706) || (latestData.H_PV_706 > latestData.HA_SP_706)
                                        || (latestData.T_PV_706 > latestData.TM_SP_706))
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        if ((latestData.H_PV_706 > latestData.HM_SP_706) && (latestData.T_PV_706 > latestData.TA_SP_706) && (latestData.H_PV_706 > latestData.HA_SP_706) &&
                                         (latestData.T_PV_706 > latestData.TM_SP_706))
                                        {
                                            oMonitoringBO.AlertMessage = "All Values going above Set Limit! Please check.";
                                        }
                                        else if ((latestData.H_PV_706 > latestData.HM_SP_706) || (latestData.H_PV_706 > latestData.HA_SP_706))
                                        {
                                            oMonitoringBO.AlertMessage = "Humidity value going above Set Limit! Please check.";
                                        }
                                        else if ((latestData.T_PV_706 > latestData.TA_SP_706) || (latestData.T_PV_706 > latestData.TM_SP_706))
                                        {
                                            oMonitoringBO.AlertMessage = "Temperature Value going above Set Limit! Please check.";
                                        }
                                    }
                                                                       
                                    if ((latestData.H_PV_706 == 0) && (latestData.T_PV_706 == 0) && (latestData.TA_SP_706 == 0) && (latestData.TM_SP_706 == 0) && (latestData.HM_SP_706 == 0) && (latestData.HA_SP_706 == 0))
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Machine " + MachineNumber + " not sending data! Test stopped.";
                                        // Set Status of All Tests Running on the Machine as FAULT
                                        oTestBLL.MachineFaultOccurred(MachineNumber);
                                    }
                                }
                                else if (MachineNumber == 707)
                                {
                                    if (latestData.H_PV_707 > latestData.T_SP_707 || latestData.T_PV_707 > latestData.T_SP_707)
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        if (latestData.H_PV_707 > latestData.T_SP_707 && latestData.T_PV_707 > latestData.T_SP_707)
                                        {
                                            oMonitoringBO.AlertMessage = "All Values going above Set Limit! Please check.";
                                        }
                                        else if (latestData.H_PV_707 > latestData.T_SP_707)
                                        {
                                            oMonitoringBO.AlertMessage = "Humidity value going above Set Limit! Please check.";
                                        }
                                        else if (latestData.T_PV_707 > latestData.T_SP_707)
                                        {
                                            oMonitoringBO.AlertMessage = "Temperature Value going above Set Limit! Please check.";
                                        }
                                    }
                                    
                                    if ((latestData.T_PV_707 == null || latestData.T_PV_707 == 0) && (latestData.H_PV_707 == null || latestData.H_PV_707 == 0))
                                    {
                                        oMonitoringBO.IsAlertRequired = true;
                                        oMonitoringBO.AlertMessage = "Machine " + MachineNumber + " not sending data! Test stopped.";
                                        // Set Status of All Tests Running on the Machine as FAULT
                                        oTestBLL.MachineFaultOccurred(MachineNumber);
                                    }
                                }
                                lstMonitoringBO2.Add(oMonitoringBO);
                            }
                        }                        
                   }
                }        
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            StringBuilder sb = new StringBuilder();
            return js.Serialize(lstMonitoringBO2);
        }
    }
}