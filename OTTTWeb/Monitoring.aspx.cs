﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   CommonFunctions
Description     :   This class has functions which are commonly used across the project
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using OTTTBLL;
using OTTTBO;
using System.Web.UI.DataVisualization.Charting;
using System.Web.Services;

#endregion " Additional Namespaces "

namespace TTTWeb
{
    public partial class Monitoring : System.Web.UI.Page
    {

        #region Class Level Variable

        static int iCount = 0;

        #endregion

        #region Events

        ///---------------------------------------------------------------------
        /// <summary>
        /// Loads page content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    iCount = 0;
                    Tab1.CssClass = "Clicked";
                    MainView.ActiveViewIndex = 0;
                    UpdateTimer.Interval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings[Convert.ToString("TabFrequency")]);
                }
                lblTabChangeFrequency.Text = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings[Convert.ToString("TabFrequency")]);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }

        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Loads Tab1 content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void Tab1_Click(object sender, EventArgs e)
        {
            try
            {
                Tab1.CssClass = "Clicked";
                Tab2.CssClass = "Initial";
                Tab3.CssClass = "Initial";
                Tab4.CssClass = "Initial";
                Tab5.CssClass = "Initial";
                Tab6.CssClass = "Initial";
                MainView.ActiveViewIndex = 0;
                iCount = 1;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Loads Tab2 content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void Tab2_Click(object sender, EventArgs e)
        {
            try
            {
                Tab1.CssClass = "Initial";
                Tab2.CssClass = "Clicked";
                Tab3.CssClass = "Initial";
                Tab4.CssClass = "Initial";
                Tab5.CssClass = "Initial";
                Tab6.CssClass = "Initial";
                MainView.ActiveViewIndex = 1;
                iCount = 2;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Loads Tab3 content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void Tab3_Click(object sender, EventArgs e)
        {
            try
            {
                Tab1.CssClass = "Initial";
                Tab2.CssClass = "Initial";
                Tab3.CssClass = "Clicked";
                Tab4.CssClass = "Initial";
                Tab5.CssClass = "Initial";
                Tab6.CssClass = "Initial";
                MainView.ActiveViewIndex = 2;
                iCount = 3;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Loads Tab4 content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void Tab4_Click(object sender, EventArgs e)
        {
            try
            {
                Tab1.CssClass = "Initial";
                Tab2.CssClass = "Initial";
                Tab3.CssClass = "Initial";
                Tab4.CssClass = "Clicked";
                Tab5.CssClass = "Initial";
                Tab6.CssClass = "Initial";
                MainView.ActiveViewIndex = 3;
                iCount = 4;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Loads Tab5 content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void Tab5_Click(object sender, EventArgs e)
        {
            try
            {
                Tab1.CssClass = "Initial";
                Tab2.CssClass = "Initial";
                Tab3.CssClass = "Initial";
                Tab4.CssClass = "Initial";
                Tab5.CssClass = "Clicked";
                Tab6.CssClass = "Initial";
                MainView.ActiveViewIndex = 4;
                iCount = 5;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Loads Tab6 content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void Tab6_Click(object sender, EventArgs e)
        {
            try
            {
                Tab1.CssClass = "Initial";
                Tab2.CssClass = "Initial";
                Tab3.CssClass = "Initial";
                Tab4.CssClass = "Initial";
                Tab5.CssClass = "Initial";
                Tab6.CssClass = "Clicked";
                MainView.ActiveViewIndex = 5;
                iCount = 0;
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// UpdateTimer_Tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void UpdateTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (chkStayHere.Checked == false)
                {
                    //lblTimer.Text = string.Empty;
                    if (iCount == 0)
                    {
                        Tab1_Click(sender, e);
                        iCount = 1;
                    }
                    else if (iCount == 1)
                    {
                        Tab2_Click(sender, e);
                        iCount = 2;
                    }
                    else if (iCount == 2)
                    {
                        Tab3_Click(sender, e);
                        iCount = 3;
                    }
                    else if (iCount == 3)
                    {
                        Tab4_Click(sender, e);
                        iCount = 4;
                    }
                    else if (iCount == 4)
                    {
                        Tab5_Click(sender, e);
                        iCount = 5;
                    }
                    else if (iCount == 5)
                    {
                        Tab6_Click(sender, e);
                        iCount = 0;
                    }
                    lblTimer.Text = "Date:" + " " + DateTime.Now.ToString();
                  
                }
                else
                {
                   // lblTimer.Text = "Date:" + " " + DateTime.Now.ToString();
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }

        }

        #endregion Events

        protected void chkStayHere_CheckedChanged(object sender, EventArgs e)
        {
            try 
            {
               if(chkStayHere.Checked==false)
               {
                   
               }
               else
               {
                   lblTimer.Text = string.Empty;
               }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
        }

        protected void lnkButton_Click(object sender, EventArgs e)
        {
            
        }

    }
}