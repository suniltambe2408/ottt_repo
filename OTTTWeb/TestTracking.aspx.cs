﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   CommonFunctions
Description     :   This class has functions which are commonly used across the project
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using OTTTBLL;
using OTTTBO;
using System.Data;

#endregion " Additional Namespaces "

namespace OTTTWeb
{
    public partial class TestTracking : System.Web.UI.Page
    {
        #region " Class Level Variable "

        int iDBOperationCode = 0;
        TestBO oTestBO = null;
        TestTrackingBLL oTestTrackingBLL = null;
        List<TestBO> lstTestBO = null;
        TestBLL oTestBLL = null;

        #endregion " Class Level Variable "

        #region " Events "

        ///---------------------------------------------------------------------
        /// <summary>
        /// Loads page content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e)
        {
            oTestBLL = new TestBLL();

            try
            {
                if (!Page.IsPostBack)
                {
                    ddlMachineNo.DataSource = oTestBLL.GetAllMachineName();
                    ddlMachineNo.DataValueField = "MachineID";
                    ddlMachineNo.DataTextField = "Machine";
                    ddlMachineNo.DataBind();
                    ddlMachineNo.Items.Insert(0, new ListItem("Select", "0"));
                    ddlMachineNo.SelectedIndex = 0;

                    oTestBLL = new TestBLL();
                    ddlCostCenter.DataSource = oTestBLL.GetAllCostCenters();
                    ddlCostCenter.DataValueField = "CostCenterID";
                    ddlCostCenter.DataTextField = "CostCenterName";
                    ddlCostCenter.DataBind();
                    ddlCostCenter.Items.Insert(0, new ListItem("Select", "0"));
                    ddlCostCenter.SelectedIndex = 0;

                    if (!string.IsNullOrEmpty(Convert.ToString(Session["UserFristName"])))
                    {
                        txtTestOwner.Text = Convert.ToString(Session["UserFristName"]) + " " + Convert.ToString(Session["UserLastName"]);
                    }
                    else
                    {
                        txtTestOwner.Text = string.Empty;
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(Session["UserDepartment"])))
                    {
                        txtDepartment.Text = Convert.ToString(Session["UserDepartment"]);
                    }
                    else
                    {
                        txtDepartment.Text = string.Empty;
                    }
                    txtStartDate.Text = DateTime.Now.Date.ToShortDateString();
                    
                    txtStartTimeMins.Text = Convert.ToString(DateTime.Now.Minute);
                    if (DateTime.Now.Hour < 12)
                    {
                        ddlStartTimeAmPm.SelectedValue = "AM";
                        txtStartTimeHrs.Text = Convert.ToString(DateTime.Now.Hour);
                    }
                    else
                    {
                        ddlStartTimeAmPm.SelectedValue = "PM";
                        txtStartTimeHrs.Text = Convert.ToString((DateTime.Now.Hour - 12));
                    }
                    txtDurationHrs.Text = "01";
                    txtDurationMins.Text = "00";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oTestBLL = null;
            }
        }    

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event create gridview row as editable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------    
        protected void gvTestInformation_RowEditing(object sender, GridViewEditEventArgs e)
        {
            oTestTrackingBLL = new TestTrackingBLL();
            lstTestBO = new List<TestBO>();
            lstTestBO = oTestTrackingBLL.SearchLimsData(txtLimsNo.Text);
            try
            {
                txtLimsNo.Enabled = false;
                btnSearchLIMS.Enabled = false;
                txtComponentType.Enabled = false ;
                txtTestOwner.Enabled = false;
                gvTestInformation.EditIndex = e.NewEditIndex;
                gvTestInformation.DataSource = lstTestBO;
                gvTestInformation.DataBind();
                for (int i = 0; i < gvTestInformation.Rows.Count; i++)
                {
                    Label lbl = (Label)gvTestInformation.Rows[i].FindControl("lblStatus");
                    LinkButton btnEdit = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkEdit");
                    LinkButton btnDelete = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkDelete");
                    if ((btnEdit != null) && (btnDelete != null) && (lbl.Text != "Scheduled" || lbl.Text != "Fault"))
                    {
                        btnEdit.Visible = false;
                        btnDelete.Visible = false;
                    }
                }
                lblMessage.Text = string.Empty;
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {

            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Update selected grid view data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------    
        protected void gvTestInformation_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            oTestBO = new TestBO();
            oTestTrackingBLL = new TestTrackingBLL();
            try
            {
                oTestBO.TestID = Convert.ToInt32(((Label)gvTestInformation.Rows[e.RowIndex].Cells[0].FindControl("lblTestID")).Text);
                oTestBO.LIMSNO = Convert.ToString(((Label)gvTestInformation.Rows[e.RowIndex].Cells[0].FindControl("lblgvLIMSNo")).Text);
                oTestBO.ComponentType = Convert.ToString(txtComponentType.Text);
                oTestBO.TestOwner = Convert.ToString(txtTestOwner.Text);
                oTestBO.TestName = Convert.ToString(((TextBox)gvTestInformation.Rows[e.RowIndex].Cells[0].FindControl("txtTestName")).Text);
                oTestBO.TestCondition = Convert.ToString(((TextBox)gvTestInformation.Rows[e.RowIndex].Cells[0].FindControl("txtTestCondition")).Text);
               
                string[] date = Convert.ToString(((TextBox)gvTestInformation.Rows[e.RowIndex].Cells[0].FindControl("txtStartDateTime")).Text).Split('/');
                //string time = Convert.ToString(((DropDownList)gvTestInformation.Rows[e.RowIndex].FindControl("ddlStartTimes")).SelectedItem.Text);
                string StartTimeHH = Convert.ToString(((TextBox)gvTestInformation.Rows[e.RowIndex].Cells[0].FindControl("gvtxtStartTimeHrs")).Text);
                string StartTimeMM = Convert.ToString(((TextBox)gvTestInformation.Rows[e.RowIndex].Cells[0].FindControl("gvtxtStartTimeMins")).Text);
                string time = StartTimeHH + ":" + StartTimeMM + ":00";
                oTestBO.StartDateTime = Convert.ToDateTime(date[0] + "/" + date[1] + "/" + date[2].Substring(0, 4) + " " + time);

                int durationHrs= Convert.ToInt32(((TextBox)gvTestInformation.Rows[e.RowIndex].Cells[0].FindControl("gvtxtDurationHrs")).Text);
                int durationMins = Convert.ToInt32(((TextBox)gvTestInformation.Rows[e.RowIndex].Cells[0].FindControl("gvtxtDurationMins")).Text);
                oTestBO.Duration = (durationHrs * 60) + durationMins;
                oTestBO.EndDateTime = oTestBO.StartDateTime.AddHours(durationHrs);
                oTestBO.EndDateTime = oTestBO.EndDateTime.AddMinutes(durationMins);
                oTestBO.MachineID = Convert.ToInt32(((DropDownList)gvTestInformation.Rows[e.RowIndex].FindControl("ddlMachineNumber")).SelectedValue);

                iDBOperationCode = oTestTrackingBLL.UpdateTestInformation(oTestBO);

                if (iDBOperationCode == 0)
                {
                    lblMessage.Text = Resources.Resource.msgSaveSuccess;
                    lblMessage.ForeColor = Color.Green;
                    lstTestBO = oTestTrackingBLL.SearchLimsData(txtLimsNo.Text);
                    gvTestInformation.EditIndex = -1;
                    gvTestInformation.DataSource = lstTestBO;
                    gvTestInformation.DataBind();
                    for (int i = 0; i < gvTestInformation.Rows.Count; i++)
                    {
                        Label lbl = (Label)gvTestInformation.Rows[i].FindControl("lblStatus");
                        LinkButton btnEdit = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkEdit");
                        LinkButton btnDelete = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkDelete");
                        if (lbl.Text == "Scheduled" || lbl.Text == "Fault")
                        {
                            btnEdit.Visible = true;
                            btnDelete.Visible = true;
                        }
                        else
                        {
                            btnEdit.Visible = false;
                            btnDelete.Visible = false;
                        }
                    }
                    txtLimsNo.Enabled = true;
                    txtComponentType.Enabled = false;
                    txtTestOwner.Enabled = false;
                }
                else
                {
                    lblMessage.Text = Resources.Resource.msgSaveError + Resources.Resource.msgDBErrorCode + iDBOperationCode;
                    lblMessage.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {

            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event cancel gridview row editing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------    
        protected void gvTestInformation_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            oTestTrackingBLL = new TestTrackingBLL();
            lstTestBO = new List<TestBO>();
            lstTestBO = oTestTrackingBLL.SearchLimsData(txtLimsNo.Text);
            try
            {
                gvTestInformation.EditIndex = -1;
                gvTestInformation.DataSource = lstTestBO;
                gvTestInformation.DataBind();
                for (int i = 0; i < gvTestInformation.Rows.Count; i++)
                {
                    Label lbl = (Label)gvTestInformation.Rows[i].FindControl("lblStatus");
                    LinkButton btnEdit = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkEdit");
                    LinkButton btnDelete = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkDelete");
                    if (lbl.Text == "Scheduled" || lbl.Text == "Fault")
                    {
                        btnEdit.Visible = true;
                        btnDelete.Visible = true;
                    }
                    else
                    {
                        btnEdit.Visible = false;
                        btnDelete.Visible = false;
                    }
                }
                txtLimsNo.Enabled = true;
                txtComponentType.Enabled = false;
                txtTestOwner.Enabled = false;
               // txtLimsNo.Text = txtTestOwner.Text = txtComponentType.Text = lblMessage.Text = string.Empty;
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {

            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event will update status to deleted & clear all fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------    
        protected void gvTestInformation_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            oTestTrackingBLL = new TestTrackingBLL();
            lstTestBO = new List<TestBO>();

            try
            {
                if (e.CommandName == "lnkDelete")
                {
                    txtLimsNo.Text = txtComponentType.Text = txtTestOwner.Text = lblMessage.Text = string.Empty;

                    lstTestBO = oTestTrackingBLL.SearchLimsData(txtLimsNo.Text);

                    int TestID = Convert.ToInt32(e.CommandArgument);

                    iDBOperationCode = oTestTrackingBLL.StatusUpdateToDeleted(TestID);

                    if (iDBOperationCode == 0)
                    {
                        gvTestInformation.DataSource = lstTestBO;
                        gvTestInformation.DataBind();
                        for (int i = 0; i < gvTestInformation.Rows.Count; i++)
                        {
                            Label lbl = (Label)gvTestInformation.Rows[i].FindControl("lblStatus");
                            LinkButton btnEdit = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkEdit");
                            LinkButton btnDelete = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkDelete");
                            if (lbl.Text == "Scheduled" || lbl.Text == "Fault")
                            {
                                btnEdit.Visible = true;
                                btnDelete.Visible = true;
                            }
                            else
                            {
                                btnEdit.Visible = false;
                                btnDelete.Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oTestTrackingBLL = null;
                oTestBO = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        ///  This event used to bind dropdown in Gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void gvTestInformation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            oTestBLL = new TestBLL();

            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlMachineNumber = (DropDownList)e.Row.FindControl("ddlMachineNumber");
                    if (ddlMachineNumber != null)
                    {
                        ddlMachineNumber.DataSource = oTestBLL.GetAllMachineName();
                        ddlMachineNumber.DataValueField = "MachineID";
                        ddlMachineNumber.DataTextField = "Machine";
                        ddlMachineNumber.DataBind();

                        Label lblMachineNumber = (Label)e.Row.FindControl("lblMachineNumber");
                        if (lblMachineNumber.Text != null)
                        {
                            ddlMachineNumber.SelectedIndex = ddlMachineNumber.Items.IndexOf(ddlMachineNumber.Items.FindByValue(lblMachineNumber.Text));
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red; 
            }
            finally
            {

            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        ///  Page Index Changing Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void gvTestInformation_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            oTestTrackingBLL = new TestTrackingBLL();
            try
            {
                string LIMSNo = txtLimsNo.Text;
                gvTestInformation.PageIndex = e.NewPageIndex;
                gvTestInformation.DataSource = oTestTrackingBLL.SearchLimsData(LIMSNo);
                gvTestInformation.DataBind();
                for (int i = 0; i < gvTestInformation.Rows.Count; i++)
                {
                    Label lbl = (Label)gvTestInformation.Rows[i].FindControl("lblStatus");
                    LinkButton btnEdit = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkEdit");
                    LinkButton btnDelete = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkDelete");
                    if (lbl.Text == "Scheduled" || lbl.Text == "Fault")
                    {
                        btnEdit.Visible = true;
                        btnDelete.Visible = true;
                    }
                    else
                    {
                        btnEdit.Visible = false;
                        btnDelete.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oTestTrackingBLL = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        ///  GvtxtDurationMins_TextChanged Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void gvtxtDurationMins_TextChanged(object sender, EventArgs e)
        {
            TextBox gvtxtDurationMins = sender as TextBox;
            if (gvtxtDurationMins.Text != "")
            {
                int durationMins = Convert.ToInt32(gvtxtDurationMins.Text);
                if (durationMins > 59 || durationMins < 0)
                {
                    lblMessage.Text = "Duration minutes cannot be less than 0 or greater than 59";
                    lblMessage.ForeColor = Color.Red;
                    gvtxtDurationMins.Text = "00";
                }
            }
            else
            {
                lblMessage.Text = "Duration minutes cannot be blank";
                lblMessage.ForeColor = Color.Red;
                gvtxtDurationMins.Text = "00";
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// gvtxtDurationHrs_TextChanged Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void gvtxtDurationHrs_TextChanged(object sender, EventArgs e)
        {
            TextBox gvtxtDurationHrs = sender as TextBox;
            if (gvtxtDurationHrs.Text != "")
            {
                if (Convert.ToInt32(gvtxtDurationHrs.Text) < 0)
                {
                    lblMessage.Text = "Duration hours cannot be less than 0";
                    lblMessage.ForeColor = Color.Red;
                    gvtxtDurationHrs.Text = "01";
                }
            }
            else
            {
                lblMessage.Text = "Duration hours cannot be blank";
                lblMessage.ForeColor = Color.Red;
                gvtxtDurationHrs.Text = "01";
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// gvtxtStartTimeHrs_TextChanged Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void gvtxtStartTimeHrs_TextChanged(object sender, EventArgs e)
        {
            TextBox gvtxtStartTimeHrs = sender as TextBox;
            if (gvtxtStartTimeHrs.Text != "")
            {
                if (Convert.ToInt32(gvtxtStartTimeHrs.Text) > 23 || Convert.ToInt32(gvtxtStartTimeHrs.Text) < 0)
                {
                    lblMessage.Text = "Start time hours cannot be greater than 23 or less than 0";
                    lblMessage.ForeColor = Color.Red;
                    gvtxtStartTimeHrs.Text = Convert.ToString(DateTime.Now.Hour);
                }
            }
            else
            {
                lblMessage.Text = "Start time hours cannot be blank";
                lblMessage.ForeColor = Color.Red;
                gvtxtStartTimeHrs.Text = Convert.ToString(DateTime.Now.Hour);
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        ///gvtxtStartTimeMins_TextChanged Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void gvtxtStartTimeMins_TextChanged(object sender, EventArgs e)
        {
            TextBox gvtxtStartTimeMins = sender as TextBox;
            if (gvtxtStartTimeMins.Text != "")
            {
                if (Convert.ToInt32(gvtxtStartTimeMins.Text) > 59 || Convert.ToInt32(gvtxtStartTimeMins.Text) < 0)
                {
                    lblMessage.Text = "Start time minutes cannot be greater than 59 or less than 0";
                    lblMessage.ForeColor = Color.Red;
                    gvtxtStartTimeMins.Text = Convert.ToString(DateTime.Now.Minute);
                }
            }
            else
            {
                lblMessage.Text = "Start time minutes cannot be blank";
                lblMessage.ForeColor = Color.Red;
                gvtxtStartTimeMins.Text = Convert.ToString(DateTime.Now.Minute);
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event search data based on LIMS Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------    
        protected void btnSearchLIMS_Click(object sender, EventArgs e)
        {
            oTestTrackingBLL = new TestTrackingBLL();
            lstTestBO = new List<TestBO>();
            btnSearchLIMS.Enabled = true;

            try
            {
                lstTestBO = oTestTrackingBLL.SearchLimsData(txtLimsNo.Text);
                if (lstTestBO.Count > 0)
                {
                    foreach (var row in lstTestBO)
                    {
                        txtComponentType.Text = Convert.ToString(row.ComponentType);
                        txtTestOwner.Text = Convert.ToString(row.TestOwner);
                    }
                  
                    gvTestInformation.DataSource = lstTestBO;
                    gvTestInformation.DataBind();
                    int i = 0;
                    for (i = 0; i < gvTestInformation.Rows.Count; i++)
                    {
                        Label lbl = (Label)gvTestInformation.Rows[i].FindControl("lblStatus");
                        LinkButton btnEdit = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkEdit");
                        LinkButton btnDelete = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkDelete");
                        if (lbl.Text == "Scheduled" || lbl.Text == "Fault")
                        {
                            btnEdit.Visible = true;
                            btnDelete.Visible = true;
                        }
                        else
                        {
                            btnEdit.Visible = false;
                            btnDelete.Visible = false;
                        }
                    }
                    lblMessage.Text = string.Empty;
                }
                else
                {
                    txtComponentType.Text = txtTestOwner.Text = string.Empty;

                    lstTestBO = oTestTrackingBLL.SearchLimsData(null);

                    gvTestInformation.DataSource = lstTestBO;
                    gvTestInformation.DataBind();
                    lblMessage.Text = Resources.Resource.msgNoRecord;
                    lblMessage.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oTestTrackingBLL = null;
                lstTestBO = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event will clear all the fields and reset gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------    
        protected void btnClear_Click(object sender, EventArgs e)
        {
            lstTestBO = new List<TestBO>();
            btnSearchLIMS.Enabled = true;

            try
            {
                txtLimsNo.Text = txtComponentType.Text = txtTestOwner.Text = lblMessage.Text = txtTestOwners.Text = txtDepartment.Text = string.Empty;

                gvTestInformation.EditIndex = -1;
                gvTestInformation.DataSource = lstTestBO;
                gvTestInformation.DataBind();
                for (int i = 0; i < gvTestInformation.Rows.Count; i++)
                {
                    Label lbl = (Label)gvTestInformation.Rows[i].FindControl("lblStatus");
                    LinkButton btnEdit = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkEdit");
                    LinkButton btnDelete = (LinkButton)gvTestInformation.Rows[i].FindControl("lnkDelete");
                    if (lbl.Text == "Scheduled" || lbl.Text == "Fault")
                    {
                        btnEdit.Visible = true;
                        btnDelete.Visible = true;
                    }
                    else
                    {
                        btnEdit.Visible = false;
                        btnDelete.Visible = false;
                    }
                }
                txtLimsNo.Focus();
                txtLimsNo.Enabled = true;
                txtComponentType.Enabled = false;
                txtTestOwner.Enabled = false;
                lblMessage.Text = "";
                txtStartDate.Text = DateTime.Now.Date.ToShortDateString();
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                lstTestBO = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event save the New Test details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void btnSave_Click(object sender, EventArgs e)
        {
            oTestBLL = new TestBLL();
            oTestBO = new TestBO();

            try
            {
                if (string.IsNullOrEmpty(lblTestID.Text))
                {
                    oTestBO.TestID = 0;
                }

                oTestBO.LIMSNO = Convert.ToString(txtLimsNumber.Text.ToUpper());
                oTestBO.TestName = Convert.ToString(txtTestName.Text);
                oTestBO.TestOwner = Convert.ToString(txtTestOwners.Text);
                oTestBO.MachineID = Convert.ToInt32(ddlMachineNo.SelectedValue);
                oTestBO.CostCenterID = Convert.ToInt32(ddlCostCenter.SelectedValue);
                oTestBO.ComponentType = Convert.ToString(txtComponentTypes.Text);
                oTestBO.UniqueTestID = Convert.ToString(txtUniqueTestId.Text);
                if (!string.IsNullOrEmpty(txtTestCondition.Text))
                {
                    oTestBO.TestCondition = Convert.ToString(txtTestCondition.Text);
                }

                string[] date = txtStartDate.Text.Split('/');
                string time = txtStartTimeHrs.Text + ':' + txtStartTimeMins.Text + ddlStartTimeAmPm.SelectedItem.Value; //Convert.ToString(ddlStartTime.SelectedValue);
                oTestBO.StartDateTime = Convert.ToDateTime(date[0] + "/" + date[1] + "/" + date[2] + " " + time);
                int duration = ((Convert.ToInt32(txtDurationHrs.Text) * 60) + Convert.ToInt32(txtDurationMins.Text));
                oTestBO.Duration = duration;

                oTestBO.EndDateTime = oTestBO.StartDateTime.AddHours(Convert.ToInt32(txtDurationHrs.Text));
                oTestBO.EndDateTime = oTestBO.EndDateTime.AddMinutes(Convert.ToInt32(txtDurationMins.Text));
                oTestBO.Department = Convert.ToString(txtDepartment.Text);

                iDBOperationCode = oTestBLL.SaveNewTest(oTestBO);

                if (iDBOperationCode == 0)
                {
                    lblAddTestError.Text = Resources.Resource.msgSaveSuccess;
                    lblAddTestError.ForeColor = Color.Green;
                    Clear();
                }
                else
                {
                    lblLIMSNOError.Text = Resources.Resource.msgDuplicateLIMS;
                    lblLIMSNOError.ForeColor = Color.Red;
                }

            }
            catch (Exception ex)
            {
                lblAddTestError.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblAddTestError.ForeColor = Color.Red;
            }
            finally
            {
                oTestBLL = null;
                oTestBO = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event Clear all fields in New Test Details form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        ///
        protected void btnClearFields_Click(object sender, EventArgs e)
        {
            try
            {
                lblAddTestError.Text = string.Empty;
                btnSearchLIMS.Enabled = true;
                Clear();
            }
            catch (Exception ex)
            {
                lblAddTestError.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblAddTestError.ForeColor = Color.Red;
            }
            finally
            {

            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// btnTestAnalysisReport_Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        ///
        protected void btnTestAnalysisReport_Click(object sender, EventArgs e)
        {
            string LIMSNO = string.Empty;
            string LIMSID = string.Empty;
            foreach (GridViewRow gvrow in gvTestInformation.Rows)
            {
                CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
                if (chk != null & chk.Checked)
                {
                    LIMSNO += (gvrow.FindControl("lblgvLIMSNo") as Label).Text;
                    LIMSID += (gvrow.FindControl("lblgvTestID") as Label).Text;
                      Response.Redirect("~/Reports/TestAnalysisReport.aspx?LIMSNO=" + LIMSNO + "," + LIMSID + "");
                
                }
                else
                {
                    lblMessage.Text = "Please select one test !!";
                    lblMessage.ForeColor = Color.Red;
                }
            }


        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event check the duplicate record based on LIMS Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void txtLimsNumber_TextChanged(object sender, EventArgs e)
        {
            oTestTrackingBLL = new TestTrackingBLL();
            lblAddTestError.Text = string.Empty;
            string UniqueTestId = null;
            try
            {
                // Populate TestID field here using LIMS Number & no of test count against given LIMS Number
                string LIMSNO = Convert.ToString(txtLimsNumber.Text);
                UniqueTestId = LIMSNO + '_' + (oTestTrackingBLL.GetTestCountForLIMSNo(LIMSNO) + 1);
                txtUniqueTestId.Text = UniqueTestId;
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oTestTrackingBLL = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event check the Current time minute
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void txtStartTimeMins_TextChanged(object sender, EventArgs e)
        {
            if (txtStartTimeMins.Text != "")
            {
                int startTimeMins = Convert.ToInt32(txtStartTimeMins.Text);
                if (startTimeMins > 23 || startTimeMins < 0)
                {
                    lblMessage.Text = "Start time minutes cannot be less than 0 or greater than 59";
                    lblMessage.ForeColor = Color.Red;
                    txtStartTimeMins.Text = Convert.ToString(DateTime.Now.Minute);
                }
            }
            else
            {
                lblMessage.Text = "Start time minutes cannot be blank";
                lblMessage.ForeColor = Color.Red;
                txtStartTimeMins.Text = Convert.ToString(DateTime.Now.Minute);
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event check the Current time hour
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void txtStartTimeHrs_TextChanged(object sender, EventArgs e)
        {
            if (txtStartTimeHrs.Text != "")
            {
                int startTimeHour = Convert.ToInt32(txtStartTimeHrs.Text);
                if (startTimeHour > 23 || startTimeHour < 0)
                {
                    lblMessage.Text = "Start time hours cannot be less than 0 or greater than 23";
                    lblMessage.ForeColor = Color.Red;

                    if (DateTime.Now.Hour < 12)
                    {
                        ddlStartTimeAmPm.SelectedValue = "AM";
                        txtStartTimeHrs.Text = Convert.ToString(DateTime.Now.Hour);
                    }
                    else
                    {
                        ddlStartTimeAmPm.SelectedValue = "PM";
                        txtStartTimeHrs.Text = Convert.ToString((DateTime.Now.Hour - 12));
                    }
                }
                else
                {
                    if (startTimeHour > 12)
                    {
                        ddlStartTimeAmPm.SelectedValue = "PM";
                        txtStartTimeHrs.Text = Convert.ToString((startTimeHour - 12));
                    }
                    else
                    {
                        ddlStartTimeAmPm.SelectedValue = "AM";
                        txtStartTimeHrs.Text = Convert.ToString((startTimeHour));
                    }

                    if (startTimeHour == 0)
                    {
                        ddlStartTimeAmPm.SelectedValue = "AM";
                        txtStartTimeHrs.Text = Convert.ToString((startTimeHour + 12));
                    }
                }
            }
            else
            {
                lblMessage.Text = "Start time hours cannot be blank";
                lblMessage.ForeColor = Color.Red;
                if (DateTime.Now.Hour < 12)
                {
                    ddlStartTimeAmPm.SelectedValue = "AM";
                    txtStartTimeHrs.Text = Convert.ToString((DateTime.Now.Hour));
                }
                else
                {
                    ddlStartTimeAmPm.SelectedValue = "PM";
                    txtStartTimeHrs.Text = Convert.ToString((DateTime.Now.Hour - 12));
                }
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event check the Current time minute
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void txtDurationMins_TextChanged(object sender, EventArgs e)
        {
            if (txtDurationMins.Text != "")
            {
                int durationMins = Convert.ToInt32(txtDurationMins.Text);
                if (durationMins > 59 || durationMins < 0)
                {
                    lblMessage.Text = "Duration minutes cannot be less than 0 or greater than 59";
                    lblMessage.ForeColor = Color.Red;
                    txtDurationMins.Text = "00";
                }
            }
            else
            {
                lblMessage.Text = "Duration minutes cannot be blank";
                lblMessage.ForeColor = Color.Red;
                txtDurationMins.Text = "00";
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event check the Current time hour
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void txtDurationHrs_TextChanged(object sender, EventArgs e)
        {
            if (txtDurationHrs.Text != "")
            {
                if (Convert.ToInt32(txtDurationHrs.Text) < 0)
                {
                    lblMessage.Text = "Duration hours cannot be less than 0";
                    lblMessage.ForeColor = Color.Red;
                    txtDurationHrs.Text = "01";
                }
            }
            else
            {
                lblMessage.Text = "Duration hours cannot be blank";
                lblMessage.ForeColor = Color.Red;
                txtDurationHrs.Text = "01";
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// chkSelect_CheckedChanged Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        protected void chkSelect_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox activeCheckBox = sender as CheckBox;

                foreach (GridViewRow rw in gvTestInformation.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("chkSelect");
                    if (chkBx != activeCheckBox)
                    {
                        chkBx.Checked = false;
                        rw.BackColor = Color.White;
                        rw.ForeColor = Color.Black;
                        lblMessage.Text = string.Empty;
                       
                    }
                    else
                    {
                        lblMessage.Text = string.Empty;
                        chkBx.Checked = true;
                        rw.BackColor = Color.Green;
                        rw.ForeColor = Color.White;
                       
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            { }
        }

        #endregion " Events "

        #region " Public Functions "

        ///---------------------------------------------------------------------
        /// <summary>
        /// This method clear all the textbox fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------
        public void Clear()
        {
            txtLimsNumber.Text = txtTestName.Text = txtTestOwners.Text = txtComponentTypes.Text = txtTestCondition.Text = lblLIMSNOError.Text = txtUniqueTestId.Text = string.Empty;
            ddlMachineNo.SelectedIndex = 0;
            
            ddlCostCenter.SelectedIndex = 0;
            txtDepartment.Text = string.Empty;
            txtLimsNumber.Focus();
            txtStartDate.Text = DateTime.Now.Date.ToShortDateString();            
            txtStartTimeMins.Text = Convert.ToString(DateTime.Now.Minute);
            if (DateTime.Now.Hour < 12)
            {
                ddlStartTimeAmPm.SelectedValue = "AM";
                txtStartTimeHrs.Text = Convert.ToString((DateTime.Now.Hour));
            }
            else
            {
                ddlStartTimeAmPm.SelectedValue = "PM";
                txtStartTimeHrs.Text = Convert.ToString((DateTime.Now.Hour - 12));
            }
            txtDurationHrs.Text = "01";
            txtDurationMins.Text = "00";

        }

        #endregion
      

    }
}