﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

#region " Additional Namespaces "

using System.Net;
using System.DirectoryServices;
using System.Security.Principal;
using System.DirectoryServices.AccountManagement;
using System.Configuration;
using Common;
using OTTTWeb.UserAuthenticationServiceRef;

#endregion

namespace OTTTWeb
{
    public class Global : System.Web.HttpApplication
    {

        private string sPath = String.Empty;
        private string sFilterAttribute = String.Empty;
        public static SearchResult adSearchresult;
        static TraceUtility oTraceUtility = new TraceUtility();

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            try
            {
                //Write log to database
                oTraceUtility.TraceService("Session_Started");

                // Code that runs when a new session is started
                //  Session["UserDeviceHostName"] = Dns.GetHostEntry(Request.ServerVariables["REMOTE_ADDR"]).HostName;

                // oTraceUtility.TraceService("Session_Start:UserDeviceHostName: " + Convert.ToString(Session["UserDeviceHostName"]));

                // Code that runs when a new session is started
                string sUserIDWithDomain = string.Empty;
                sUserIDWithDomain = HttpContext.Current.User.Identity.Name;

                if (!String.IsNullOrEmpty(sUserIDWithDomain))
                {
                    Session["UserID"] = sUserIDWithDomain.Split('\\').ElementAt(1).ToUpper();
                    Session["Domain"] = sUserIDWithDomain.Split('\\').ElementAt(0);
                }

                oTraceUtility.TraceService("Session_Start. User ID " + Convert.ToString(Session["UserID"]) + "Domain: " + Convert.ToString(Session["Domain"]));

                //Check if authorization is on. Must be set to "True" in web.config file on Production deployment
                if (Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableWindowsAuthentication")))
                {
                    oTraceUtility.TraceService("Session_Start. Before DisplayADUserInfo call");

                    // DisplayADUserInfo();
                    //Authenticate logged in user
                    AuthenticateUser();

                    oTraceUtility.TraceService("Session_Start. After DisplayADUserInfo call");
                }
                else //Temporary ELES condition for code to work without AD authentication for quick testing of all roles.
                {
                    Session["IsValidUser"] = 1;
                    Session["UserRoleID"] = 8;     //Change this role id for quick testing.  
                }

                // Condition to set name on the screen according to role. (ReviewComment: Check possibility to move onrespective page load instead of Global.asax)
             
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

        /// -------------------------------------------------------------------------
        /// <summary>
        /// Returns true if AD information is fetch successfully. Displays user information from AD
        /// This functionality only works when network connection is available
        /// </summary>
        /// <param name="objTransaction">Transaction object</param>
        /// -------------------------------------------------------------------------        
        private void DisplayADUserInfo()
        {
            DirectoryEntry entry = new DirectoryEntry(sPath);
            Object obj = entry.NativeObject;
            DirectorySearcher search = new DirectorySearcher(entry);

            try
            {
                oTraceUtility.TraceService("DisplayADUserInfo. Before 'SAMAccountName' line");

                //Bind to the native AdsObject to force authentication. 
                search.Filter = "(SAMAccountName=" + Convert.ToString(Session["UserID"]) + ")";

                adSearchresult = search.FindOne();

                oTraceUtility.TraceService("DisplayADUserInfo. Ater 'search.FindOne' line");

                if (adSearchresult != null)
                {
                    oTraceUtility.TraceService("DisplayADUserInfo.  adSearchresult != null ");

                    if (adSearchresult.Properties["givenname"].Count > 0)
                    {
                        Session["UserFristName"] = Convert.ToString(adSearchresult.Properties["givenname"][0]);

                        oTraceUtility.TraceService("DisplayADUserInfo. UserFristName: " + Convert.ToString(Session["UserFristName"]));
                    }

                    if (adSearchresult.Properties["sn"].Count > 0)
                    {
                        Session["UserLastName"] = Convert.ToString(adSearchresult.Properties["sn"][0]);

                        oTraceUtility.TraceService("DisplayADUserInfo. UserLastName: " + Convert.ToString(Session["UserLastName"]));
                    }

                    if (adSearchresult.Properties["mail"].Count > 0)
                    {
                        Session["UserEmailID"] = Convert.ToString(adSearchresult.Properties["mail"][0]);

                        oTraceUtility.TraceService("DisplayADUserInfo. UserEmailID: " + Convert.ToString(Session["UserEmailID"]));
                    }

                    if (adSearchresult.Properties["mobile"].Count > 0)
                    {
                        Session["UserMobile"] = Convert.ToString(adSearchresult.Properties["mobile"][0]);

                        oTraceUtility.TraceService("DisplayADUserInfo. UserMobile: " + Convert.ToString(Session["UserMobile"]));
                    }

                    if (adSearchresult.Properties["telephonenumber"].Count > 0)
                    {
                        Session["UserDeskPhone"] = Convert.ToString(adSearchresult.Properties["telephonenumber"][0]);

                        oTraceUtility.TraceService("DisplayADUserInfo. UserDeskPhone: " + Convert.ToString(Session["UserDeskPhone"]));
                    }

                    if (adSearchresult.Properties["department"].Count > 0)
                    {
                        Session["UserDepartment"] = Convert.ToString(adSearchresult.Properties["department"][0]);

                        oTraceUtility.TraceService("DisplayADUserInfo. UserDepartment: " + Convert.ToString(Session["UserDepartment"]));
                    }


                    if (adSearchresult.Properties["displayname"].Count > 0)
                    {
                        Session["DisplayNameAD"] = Convert.ToString(adSearchresult.Properties["displayname"][0]);

                        string sInviteeDisplayName = string.Empty;
                        sInviteeDisplayName = Convert.ToString(Session["DisplayNameAD"]);

                        Session["UserOrganization"] = sInviteeDisplayName.Split('(', ')')[1].ToUpper();

                        oTraceUtility.TraceService("DisplayADUserInfo. After 'Session['UserOrganization']");
                    }
                }

                //Update the new path to the user in the directory.      
                sPath = adSearchresult.Path;
                sFilterAttribute = (String)adSearchresult.Properties["cn"][0];

                oTraceUtility.TraceService("End of DisplayADUserInfo function");
            }
            catch (Exception ex)
            {
                oTraceUtility.TraceService("DisplayADUserInfo: Exception : " + ex.Message);
                throw ex;
            }
            finally
            {
                obj = null;
                search = null;
            }

        }

        /// -------------------------------------------------------
        /// <summary>
        /// Authenticates user based on windows identity (User must be on AD server)
        /// </summary>
        /// -------------------------------------------------------
        public void AuthenticateUser()
        {
            try
            {
                oTraceUtility.TraceService("Global.asax -Entered into AuthenticateUser function ");

                //Create object of WCF service & get users details from Application access request database
                UserAuthenticationServiceRef.UserAuthenticationServiceClient UserAuthenticationServiceRef = new UserAuthenticationServiceRef.UserAuthenticationServiceClient();

                oTraceUtility.TraceService("Global.asax -Getting UserRequestDetails ");
                RequestDetailsBO oRequestDetailsBO = new RequestDetailsBO();
                oRequestDetailsBO = UserAuthenticationServiceRef.GetUserRequestDetails(Convert.ToString(Session["UserID"]), Convert.ToString(ConfigurationManager.AppSettings.Get("AppShortName")));


                oTraceUtility.TraceService("Global.asax -Check if user is registered using online form & not deactivated for any reason ");
                //Check if user is registered using online form & not deactivated for any reason.
                if (oRequestDetailsBO.AccessReqID > 0)
                {
                    if (oRequestDetailsBO.IsUserActive)
                    {
                        Session["IsValidUser"] = 1;
                        Session["UserRoleID"] = oRequestDetailsBO.RoleID;
                        oTraceUtility.TraceService("Global.asax -User is registered. Role ID" + Convert.ToString(Session["UserRoleID"]));
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(oRequestDetailsBO.DeactivationReason)) //If deactivation reason is empty, means user submitted new request
                        {
                            oTraceUtility.TraceService("Global.asax -Redirecting to approval status page");
                            //When user is not active, it means request is not approved yet. 
                            //Redirect user to Application access request app page to see approval status
                            Response.Redirect(ConfigurationManager.AppSettings.Get("ApprovalStatusPagePath") + "?UserID=" + Session["UserID"] + "&AppShortName=" + ConfigurationManager.AppSettings.Get("AppShortName"));

                        }
                        else      //else user access is disabled. So, user should be redirected to Deactivation message page. However, deactivated user could submit new request
                        {
                            oTraceUtility.TraceService("Global.asax - Redirecting to DeactivatedUser page");
                            Response.Redirect(ConfigurationManager.AppSettings.Get("DeactivatedUserInfoPagePath"));
                        }
                    }
                }
                else       //If user has not submitted the request, it means, user is not registered to use the application
                {
                    oTraceUtility.TraceService("Global.asax -Redirecting to Home page as user is not registered");
                    Response.Redirect("~/Home.aspx");
                }
            }
            catch (Exception ex)
            {
                oTraceUtility.TraceService("Global.asax - AuthenticateUser function : Exception: " + ex.Message);
                //lblMessageHomeMaster.Text = Resources.Resource.lblCatchMsg + ex.Message;
                //lblMessageHomeMaster.ForeColor = Color.Red;
                throw ex;
            }
        }

    }
}
