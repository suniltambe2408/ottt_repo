﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.Master" MaintainScrollPositionOnPostback ="true" AutoEventWireup="true"   EnableEventValidation="false" CodeBehind="Monitoring.aspx.cs" Inherits="TTTWeb.Monitoring" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script src="Scripts/d3/d3.js" type="text/javascript"></script>
    <script src="Scripts/d3/d3.min.js" type="text/javascript"></script>

    <style>
        visualisation {
            font: 12px Arial;
        }

        #MainContent_lblScrollFrequency {
            margin-left: 50px;
        }

        path {
            stroke-width: 1;
            fill: none;
        }

        .axis path, .axis line {
            fill: none;
            stroke: grey;
            stroke-width: 1;
            shape-rendering: crispEdges;
        }

        .hdr_mddlebg {
            padding-top: 0px;
        }

        .container1 {
            width: 1220px;
            height: 585px;
            overflow-x: scroll;
            overflow-x: hidden;
        }
    </style>
    <style type="text/css">
        .Initial {
            display: block;
            padding: 4px 18px 4px 18px;
            float: left;
            background: url("../Images/LighrGray.png") no-repeat right top;
            color: Black;
            font-weight: bold;
        }

            .Initial:hover {
                color: White;
                background: url("../Images/DarkGray.png") no-repeat right top;
            }

        .Clicked {
            float: left;
            display: block;
            background: url("../Images/DarkGray.png") no-repeat right top;
            padding: 4px 18px 4px 18px;
            color: Black;
            font-weight: bold;
            color: White;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            var currentDt = new Date();
            var mm = currentDt.getMonth() + 1;
            var dd = currentDt.getDate();
            var yyyy = currentDt.getFullYear();
            var date = mm + '/' + dd + '/' + yyyy;
            var time = currentDt.toLocaleTimeString();
            //console.log(date + ' ' + time);
            document.getElementById('<%= lblTimer.ClientID %>').innerHTML = "Date: " + date + " " + time;


            GetGraphData();
            //var page = 0;
            var tabChangeFrequency = document.getElementById('<%= lblTabChangeFrequency.ClientID %>').innerHTML;
            var refreshFrequency = $('#MainContent_ddlScrollFrequency option:selected').val();
            
            window.setInterval(function () {
                currentDt = new Date();
                mm = currentDt.getMonth() + 1;
                dd = currentDt.getDate();
                yyyy = currentDt.getFullYear();
                date = mm + '/' + dd + '/' + yyyy;
                time = currentDt.toLocaleTimeString();
                //console.log(date + ' ' + time);
                document.getElementById('<%= lblTimer.ClientID %>').innerHTML = "Date: " + date + " " + time;
            }, 300000);


            window.setInterval(function () {
                refreshFrequency = $('#MainContent_ddlScrollFrequency option:selected').val();
                GetGraphData();
            }, refreshFrequency);

            <%--if (!$('#chkStayHere').is(":checked")) {
                window.setInterval(function () {
                    tabChangeFrequency = document.getElementById('<%= lblTabChangeFrequency.ClientID %>').innerHTML
                    if (page == 0) {
                        document.getElementById('<%= Tab1.ClientID %>').click();
                        page = 1;
                    }
                    else if (page == 1) {
                            document.getElementById('<%= Tab2.ClientID %>').click();
                            page = 2;
                    }
                    else if (page == 2) {
                            document.getElementById('<%= Tab3.ClientID %>').click();
                            page = 3;
                    }
                    else if (page == 3) {
                            document.getElementById('<%= Tab4.ClientID %>').click();
                            page = 4;
                    }
                    else if (page == 4) {
                            document.getElementById('<%= Tab5.ClientID %>').click();
                            page = 5;
                    }
                    else if (page == 5) {
                            document.getElementById('<%= Tab6.ClientID %>').click();
                            page = 0;
                    }
               

                    $('#Tab1').click(function clicked() {
                        $('#Tab1').class('Clicked');
                        $('#Tab2').class('Initial');
                        $('#Tab3').class('Initial');
                        $('#Tab4').class('Initial');
                        $('#Tab5').class('Initial');
                        $('#Tab6').class('Initial');
                        page = 1;
                });

                    $('#Tab2').click(function clicked() {
                        $('#Tab1').class('Initial');
                        $('#Tab2').class('Clicked');
                        $('#Tab3').class('Initial');
                        $('#Tab4').class('Initial');
                        $('#Tab5').class('Initial');
                        $('#Tab6').class('Initial');
                        page = 2;
                });

                    $('#Tab3').click(function clicked() {
                        $('#Tab1').class('Initial');
                        $('#Tab2').class('Initial');
                        $('#Tab3').class('Clicked');
                        $('#Tab4').class('Initial');
                        $('#Tab5').class('Initial');
                        $('#Tab6').class('Initial');
                        page = 3;
                });

                    $('#Tab4').click(function clicked() {
                        $('#Tab1').class('Initial');
                        $('#Tab2').class('Initial');
                        $('#Tab3').class('Initial');
                        $('#Tab4').class('Clicked');
                        $('#Tab5').class('Initial');
                        $('#Tab6').class('Initial');
                        page = 4;
                });

                    $('#Tab5').click(function clicked() {
                        $('#Tab1').class('Initial');
                        $('#Tab2').class('Initial');
                        $('#Tab3').class('Initial');
                        $('#Tab4').class('Initial');
                        $('#Tab5').class('Clicked');
                        $('#Tab6').class('Initial');
                        page = 5;
                });

                    $('#Tab6').click(function clicked() {
                        $('#Tab1').class('Initial');
                        $('#Tab2').class('Initial');
                        $('#Tab3').class('Initial');
                        $('#Tab4').class('Initial');
                        $('#Tab5').class('Initial');
                        $('#Tab6').class('Clicked');
                        page = 0;
                });

                }, tabChangeFrequency);
            }--%>

            //function Tab1_Click() {
            //    Tab1.CssClass = "Clicked";
            //    Tab2.CssClass = "Initial";
            //    Tab3.CssClass = "Initial";
            //    Tab4.CssClass = "Initial";
            //    Tab5.CssClass = "Initial";
            //    Tab6.CssClass = "Initial";
            //    MainView.ActiveViewIndex = 0;
            //    page = 1;
            //}

            //function Tab2_Click() {
            //    Tab1.CssClass = "Initial";
            //    Tab2.CssClass = "Clicked";
            //    Tab3.CssClass = "Initial";
            //    Tab4.CssClass = "Initial";
            //    Tab5.CssClass = "Initial";
            //    Tab6.CssClass = "Initial";
            //    MainView.ActiveViewIndex = 1;
            //    page = 2;
            //}

            //function Tab3_Click() {
            //    Tab1.CssClass = "Clicked";
            //    Tab2.CssClass = "Initial";
            //    Tab3.CssClass = "Initial";
            //    Tab4.CssClass = "Initial";
            //    Tab5.CssClass = "Initial";
            //    Tab6.CssClass = "Initial";
            //    MainView.ActiveViewIndex = 2;
            //    page = 3;
            //}

            //function Tab4_Click() {
            //    Tab1.CssClass = "Initial";
            //    Tab2.CssClass = "Clicked";
            //    Tab3.CssClass = "Initial";
            //    Tab4.CssClass = "Initial";
            //    Tab5.CssClass = "Initial";
            //    Tab6.CssClass = "Initial";
            //    MainView.ActiveViewIndex = 3;
            //    page = 4;
            //}

            //function Tab5_Click() {
            //    Tab1.CssClass = "Clicked";
            //    Tab2.CssClass = "Initial";
            //    Tab3.CssClass = "Initial";
            //    Tab4.CssClass = "Initial";
            //    Tab5.CssClass = "Initial";
            //    Tab6.CssClass = "Initial";
            //    MainView.ActiveViewIndex = 4;
            //    page = 5;
            //}

            //function Tab6_Click() {
            //    Tab1.CssClass = "Initial";
            //    Tab2.CssClass = "Clicked";
            //    Tab3.CssClass = "Initial";
            //    Tab4.CssClass = "Initial";
            //    Tab5.CssClass = "Initial";
            //    Tab6.CssClass = "Initial";
            //    MainView.ActiveViewIndex = 5;
            //    page = 6;
            //}

        });

        //function lnkButtonClicked() {
        //    $("#outerContainer").width('100%').height('100%');
        //}

        function GetGraphData() {
            $.ajax({
                url: "GraphBindHandler.ashx/BindGraphData",
                type: "GET",
                async: "true",
                cache: "false",
                success: function (Data) {
                    var j = 0;
                    $.each(Data, function (i, graphData) {
                        drawD3Chart(graphData, i);
                        j = i + 1;
                    });
                    while (j < 13) {
                        j = j + 1;
                        var divToRemove = "#graph" + j;
                        $(divToRemove).hide();
                    }
                },
                Error: function (x, e) {
                    alert('Something went wrong');
                }
            });
        }
        function drawD3Chart(graphdata, i) {
            var data = graphdata.lstMonitoringDataBO;

            var margin = {
                top: 6,
                right: 20,
                bottom: 20,
                left: 30
            };
            var width = 1250 - margin.left - margin.right;
            var height = 220 - margin.top - margin.bottom;

            var parseDate = d3.time.format("%H:%M:%S").parse;

            var x = d3.time.scale().range([0, width]);
            var y = d3.scale.linear().range([height, 0]);

            var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom")
                .tickFormat(d3.time.format("%H:%M:%S")).ticks(15);

            var yAxis = d3.svg.axis().scale(y)
                .orient("left").ticks(5);
            var labelName = '#MainContent_lblOvenName' + (i + 1);
            $(labelName).text(graphdata.MachineNoLong + " : " + graphdata.MachineName);

            if (graphdata.MachineNumber == 603) { // For Machine Number 603
                if (data != null && data.length > 0) {
                    var valueline = d3.svg.line()
                .x(function (d) {
                    return x(d.DateTimes);
                })
                .y(function (d) {
                    return y(d.T_SP_603);
                });

                    var valueline2 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.T_PV_603);
                        });

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .on(graphdata.IsAlertRequired, function () { return tooltip.style("visibility", "visible"); })
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    data.forEach(function (d) {
                        d.DateTimes = parseDate(d.DateTimes);
                        d.T_SP_603 = +d.T_SP_603;
                        d.T_PV_603 = +d.T_PV_603;
                    });

                    // Scale the range of the data
                    x.domain(d3.extent(data, function (d) {
                        return d.DateTimes;
                    }));
                    y.domain([0, d3.max(data, function (d) {
                        return d.T_SP_603;
                    }) + 20]);


                    var setValueBox1 = '#MainContent_txtGraphTempSetValue' + (i + 1);
                    $(setValueBox1).val(data[0].T_SP_603);

                    var processValueBox1 = '#MainContent_txtGraphTempProcessValue' + (i + 1);
                    $(processValueBox1).val(data[0].T_PV_603);

                    if (!graphdata.IsAlertRequired) {
                        svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "black");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "red")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "#70FF00")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text("");
                    } else {
                        svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "yellow");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "red")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "green")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text(graphdata.AlertMessage);
                    }


                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
                else {
                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "black");
                    var alertMessage = "#MainContent_lblAlert" + (i + 1);
                    $(alertMessage).text("No Test Running!");

                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }

            }
            else if (graphdata.MachineNumber == 605) {  // For Machine Number 605
                if (data != null && data.length > 0) {
                    var valueline = d3.svg.line()
                .x(function (d) {
                    return x(d.DateTimes);
                })
                .y(function (d) {
                    return y(d.T_SP_605);
                });

                    var valueline2 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.T_PV_605);
                        });

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .on(graphdata.IsAlertRequired, function () { return tooltip.style("visibility", "visible"); })
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    data.forEach(function (d) {
                        d.DateTimes = parseDate(d.DateTimes);
                        d.T_SP_605 = +d.T_SP_605;
                        d.T_PV_605 = +d.T_PV_605;
                    });

                    // Scale the range of the data
                    x.domain(d3.extent(data, function (d) {
                        return d.DateTimes;
                    }));
                    y.domain([0, d3.max(data, function (d) {
                        return d.T_SP_605;
                    }) + 20]);


                    var setValueBox2 = '#MainContent_txtGraphTempSetValue' + (i + 1);
                    $(setValueBox2).val(data[0].T_SP_605);

                    var processValueBox2 = '#MainContent_txtGraphTempProcessValue' + (i + 1);
                    $(processValueBox2).val(data[0].T_PV_605);

                    if (!graphdata.IsAlertRequired) {
                        svg.append("rect")
                       .attr("width", width)
                       .attr("height", height)
                       .attr("fill", "#072400");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "aqua")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "#70FF00")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text("");
                    } else {
                        svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "yellow");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "blue")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "red")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text(graphdata.AlertMessage);
                    }
                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
                else {
                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "black");
                    var alertMessage = "#MainContent_lblAlert" + (i + 1);
                    $(alertMessage).text("No Test Running!");

                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }

            } else if (graphdata.MachineNumber == 704) {   // For Machine Number 704
                //alert(data.length);
                if (data != null && data.length > 0) {
                    var valueline = d3.svg.line()
                .x(function (d) {
                    return x(d.DateTimes);
                })
                .y(function (d) {
                    return y(d.T_SP_704);
                });

                    var valueline2 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.T_PV_704);
                        });

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .on(graphdata.IsAlertRequired, function () { return tooltip.style("visibility", "visible"); })
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    data.forEach(function (d) {
                        d.DateTimes = parseDate(d.DateTimes);
                        d.T_SP_704 = +d.T_SP_704;
                        d.T_PV_704 = +d.T_PV_704;
                    });

                    // Scale the range of the data
                    x.domain(d3.extent(data, function (d) {
                        return d.DateTimes;
                    }));
                    y.domain([0, d3.max(data, function (d) {
                        return d.T_SP_704;
                    }) + 20]);

                    var setValueBox3 = '#MainContent_txtGraphTempSetValue' + (i + 1);
                    $(setValueBox3).val(data[0].T_SP_704);

                    var processValueBox3 = '#MainContent_txtGraphTempProcessValue' + (i + 1);
                    $(processValueBox3).val(data[0].T_PV_704);

                    if (!graphdata.IsAlertRequired) {
                        svg.append("rect")
                       .attr("width", width)
                       .attr("height", height)
                       .attr("fill", "#072400");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "aqua")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "#70FF00")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text("");
                    } else {
                        svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "yellow");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "blue")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "red")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text(graphdata.AlertMessage);
                    }
                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
                else {

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "black");
                    var alertMessage = "#MainContent_lblAlert" + (i + 1);
                    $(alertMessage).text("No Test Running!");

                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }

            }
            else if (graphdata.MachineNumber == 602) {  // For Machine Number 602
                if (data != null && data.length > 0) {

                    var valueline = d3.svg.line()
                .x(function (d) {
                    return x(d.DateTimes);
                })
                .y(function (d) {
                    return y(d.C_Humidity_602);
                });

                    var valueline2 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.C_Temp_602);
                        });
                    var valueline3 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.H_Temp_602);
                        });
                    var valueline4 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.W_Temp_602);
                        });

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .on(graphdata.IsAlertRequired, function () { return tooltip.style("visibility", "visible"); })
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    data.forEach(function (d) {
                        d.DateTimes = parseDate(d.DateTimes);
                        d.C_Humidity_602 = +d.C_Humidity_602;
                        d.C_Temp_602 = +d.C_Temp_602;
                        d.H_Temp_602 = +d.H_Temp_602;
                        d.W_Temp_602 = +d.W_Temp_602;
                    });

                    // Scale the range of the data
                    x.domain(d3.extent(data, function (d) {
                        return d.DateTimes;
                    }));
                    y.domain([0, d3.max(data, function (d) {
                        return d.C_Temp_602;
                    }) + 20]);

                    var txtGraphCTempValue4 = '#MainContent_txtGraphCTempValue' + (i + 1);
                    $(txtGraphCTempValue4).val(data[0].C_Temp_602);

                    var txtGraphCHumidityValue4 = '#MainContent_txtGraphCHumidityValue' + (i + 1);
                    $(txtGraphCHumidityValue4).val(data[0].C_Humidity_602);

                    var txtGraphHTempValue4 = '#MainContent_txtGraphHTempValue' + (i + 1);
                    $(txtGraphHTempValue4).val(data[0].H_Temp_602);

                    var txtGraphWTempValue4 = '#MainContent_txtGraphWTempValue' + (i + 1);
                    $(txtGraphWTempValue4).val(data[0].W_Temp_602);

                    if (!graphdata.IsAlertRequired) {
                        svg.append("rect")
                       .attr("width", width)
                       .attr("height", height)
                       .attr("fill", "#072400");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "aqua")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "#70FF00")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);

                        svg.append("path") // Add the valueline path for H_Temp process value data.
                            .style("stroke", "yellow")
                            .attr("d", valueline3(data));

                        svg.append("path") // Add the valueline path for W_Temp process value data.
                            .style("stroke", "blue")
                            .attr("d", valueline4(data));
                        $(alertMessage).text("");
                    } else {
                        svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "yellow");

                        svg.append("path") // Add the valueline path for C_Humidity_602 process value data.
                             .style("stroke", "blue")
                             .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for C_Temp process value data.
                                .style("stroke", "red")
                                .attr("d", valueline2(data));

                        svg.append("path") // Add the valueline path for H_Temp process value data.
                            .style("stroke", "black")
                            .attr("d", valueline3(data));

                        svg.append("path") // Add the valueline path for W_Temp process value data.
                            .style("stroke", "green")
                            .attr("d", valueline4(data));

                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text(graphdata.AlertMessage);
                    }
                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
                else {

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "black");
                    var alertMessage = "#MainContent_lblAlert" + (i + 1);
                    $(alertMessage).text("No Test Running!");

                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
            } else if (graphdata.MachineNumber == 604) {  // For Machine Number 605
                if (data != null && data.length > 0) {
                    var valueline = d3.svg.line()
                .x(function (d) {
                    return x(d.DateTimes);
                })
                .y(function (d) {
                    return y(d.T_SP_604);
                });

                    var valueline2 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.T_PV_604);
                        });

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .on(graphdata.IsAlertRequired, function () { return tooltip.style("visibility", "visible"); })
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    data.forEach(function (d) {
                        d.DateTimes = parseDate(d.DateTimes);
                        d.T_SP_604 = +d.T_SP_604;
                        d.T_PV_604 = +d.T_PV_604;
                    });

                    // Scale the range of the data
                    x.domain(d3.extent(data, function (d) {
                        return d.DateTimes;
                    }));
                    y.domain([0, d3.max(data, function (d) {
                        return d.T_SP_604;
                    }) + 20]);

                    var setValueBox5 = '#MainContent_txtGraphTempSetValue' + (i + 1);
                    $(setValueBox5).val(data[0].T_SP_604);

                    var processValueBox5 = '#MainContent_txtGraphTempProcessValue' + (i + 1);
                    $(processValueBox5).val(data[0].T_PV_604);

                    if (!graphdata.IsAlertRequired) {
                        svg.append("rect")
                       .attr("width", width)
                       .attr("height", height)
                       .attr("fill", "#072400");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "aqua")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "#70FF00")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text("");
                    } else {
                        svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "yellow");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "blue")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "red")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text(graphdata.AlertMessage);
                    }
                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
                else {
                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "black");
                    var alertMessage = "#MainContent_lblAlert" + (i + 1);
                    $(alertMessage).text("No Test Running!");

                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
            } else if (graphdata.MachineNumber == 707) {  // For Machine Number 602
                if (data != null && data.length > 0) {
                    

                    var valueline = d3.svg.line()
                .x(function (d) {
                    return x(d.DateTimes);
                })
                .y(function (d) {
                    return y(d.T_SP_707);
                });

                    var valueline2 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.T_PV_707);
                        });
                    var valueline3 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.H_PV_707);
                        });

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .on(graphdata.IsAlertRequired, function () { return tooltip.style("visibility", "visible"); })
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    data.forEach(function (d) {
                        d.DateTimes = parseDate(d.DateTimes);
                        d.T_SP_707 = +d.T_SP_707;
                        d.T_PV_707 = +d.T_PV_707;
                        d.H_PV_707 = +d.H_PV_707;
                    });

                    // Scale the range of the data
                    x.domain(d3.extent(data, function (d) {
                        return d.DateTimes;
                    }));
                    y.domain([0, d3.max(data, function (d) {
                        return d.T_SP_707;
                    }) + 20]);
                    
                    var txtGraphTempSetValue6 = '#MainContent_txtGraphTempSetValue' + (i + 1);
                    $(txtGraphTempSetValue6).val(data[0].T_SP_707);

                    var txtGraphTempProcessValue6 = '#MainContent_txtGraphTempProcessValue' + (i + 1);
                    $(txtGraphTempProcessValue6).val(data[0].T_PV_707);

                    var txtGraphHumidityProcessValue6 = '#MainContent_txtGraphHumidityProcessValue' + (i + 1);
                    $(txtGraphHumidityProcessValue6).val(data[0].H_PV_707);

                    if (!graphdata.IsAlertRequired) {
                        svg.append("rect")
                       .attr("width", width)
                       .attr("height", height)
                       .attr("fill", "#072400");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "aqua")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "#70FF00")
                                .attr("d", valueline2(data));

                        svg.append("path") // Add the valueline path for H_Temp process value data.
                            .style("stroke", "yellow")
                            .attr("d", valueline3(data));

                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text("");
                    } else {
                        svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "yellow");


                        svg.append("path") // Add the valueline path for C_Humidity_602 process value data.
                            .style("stroke", "blue")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for C_Temp process value data.
                                .style("stroke", "red")
                                .attr("d", valueline2(data));


                        svg.append("path") // Add the valueline path for H_Temp process value data.
                            .style("stroke", "black")
                            .attr("d", valueline3(data));


                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text(graphdata.AlertMessage);
                    }
                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
                else {

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "black");
                    var alertMessage = "#MainContent_lblAlert" + (i + 1);
                    $(alertMessage).text("No Test Running!");

                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
            } else if (graphdata.MachineNumber == 606) {  // For Machine Number 605
                if (data != null && data.length > 0) {
                   

                    var valueline = d3.svg.line()
                .x(function (d) {
                    return x(d.DateTimes);
                })
                .y(function (d) {
                    return y(d.T_SP_606);
                });

                    var valueline2 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.T_PV_606);
                        });

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .on(graphdata.IsAlertRequired, function () { return tooltip.style("visibility", "visible"); })
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    data.forEach(function (d) {
                        d.DateTimes = parseDate(d.DateTimes);
                        d.T_SP_606 = +d.T_SP_606;
                        d.T_PV_606 = +d.T_PV_606;
                    });

                    // Scale the range of the data
                    x.domain(d3.extent(data, function (d) {
                        return d.DateTimes;
                    }));
                    y.domain([0, d3.max(data, function (d) {
                        return d.T_SP_606;
                    }) + 20]);

                    var txtGraphTempSetValue = '#MainContent_txtGraphTempSetValue' + (i + 1);
                    $(txtGraphTempSetValue).val(data[0].T_SP_606);

                    var txtGraphTempProcessValue = '#MainContent_txtGraphTempProcessValue' + (i + 1);
                    $(txtGraphTempProcessValue).val(data[0].T_PV_606);

                    if (!graphdata.IsAlertRequired) {
                        svg.append("rect")
                       .attr("width", width)
                       .attr("height", height)
                       .attr("fill", "#072400");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "aqua")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "#70FF00")
                                .attr("d", valueline2(data));

                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text("");
                    } else {
                        svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "yellow");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "blue")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "red")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text(graphdata.AlertMessage);
                    }
                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
                else {
                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "black");
                    var alertMessage = "#MainContent_lblAlert" + (i + 1);
                    $(alertMessage).text("No Test Running!");

                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
            } else if (graphdata.MachineNumber == 706) {  // For Machine Number 706
                if (data != null && data.length > 0) {

                    
                    var valueline = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.TM_SP_706);
                        });

                    var valueline2 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.TA_SP_706);
                        });
                    var valueline3 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.H_PV_706);
                        });
                    var valueline4 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.HM_SP_706);
                        });
                    var valueline5 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.HA_SP_706);
                        });

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .on(graphdata.IsAlertRequired, function () { return tooltip.style("visibility", "visible"); })
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    data.forEach(function (d) {
                        d.DateTimes = parseDate(d.DateTimes);
                        d.TM_SP_706 = +d.TM_SP_706;
                        d.TA_SP_706 = +d.TA_SP_706;
                        d.H_PV_706 = +d.H_PV_706;
                        d.HM_SP_706 = +d.HM_SP_706;
                        d.HA_SP_706 = +d.HA_SP_706;
                    });

                    // Scale the range of the data
                    x.domain(d3.extent(data, function (d) {
                        return d.DateTimes;
                    }));
                    y.domain([0, d3.max(data, function (d) {
                        return d.H_PV_706;
                    }) + 20]);

                    var txtTMSetValue8 = '#MainContent_txtGraphTMSetValue' + (i + 1);
                    $(txtTMSetValue8).val(data[0].TM_SP_706);

                    var txtTASetValue8 = '#MainContent_txtGraphTASetValue' + (i + 1);
                    $(txtTASetValue8).val(data[0].TA_SP_706);

                    var txtTempProcessValue8 = '#MainContent_txtGraphTempProcessValue' + (i + 1);
                    $(txtTempProcessValue8).val(data[0].T_PV_706);

                    var txtGraphHMSetValue8 = '#MainContent_txtGraphHMSetValue' + (i + 1);
                    $(txtGraphHMSetValue8).val(data[0].HM_SP_706);

                    var txtGraphHASetValue8 = '#MainContent_txtGraphHASetValue' + (i + 1);
                    $(txtGraphHASetValue8).val(data[0].HA_SP_706);

                    var txtGraphHumidityProcessValue8 = '#MainContent_txtGraphHumidityProcessValue' + (i + 1);
                    $(txtGraphHumidityProcessValue8).val(data[0].H_PV_706);


                    if (!graphdata.IsAlertRequired) {
                        svg.append("rect")
                       .attr("width", width)
                       .attr("height", height)
                       .attr("fill", "#072400");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "aqua")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "#70FF00")
                                .attr("d", valueline2(data));

                        svg.append("path") // Add the valueline path for H_Temp process value data.
                            .style("stroke", "yellow")
                            .attr("d", valueline3(data));

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "pink")
                            .attr("d", valueline3(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "blue")
                                .attr("d", valueline4(data));

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "white")
                            .attr("d", valueline5(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text("");
                    } else {
                        svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "yellow");

                        svg.append("path") // Add the valueline path for C_Humidity_602 process value data.
                            .style("stroke", "blue")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for C_Temp process value data.
                                .style("stroke", "red")
                                .attr("d", valueline2(data));

                        svg.append("path") // Add the valueline path for H_Temp process value data.
                            .style("stroke", "black")
                            .attr("d", valueline3(data));

                        svg.append("path") // Add the valueline path for W_Temp process value data.
                            .style("stroke", "green")
                            .attr("d", valueline4(data));

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "brown")
                            .attr("d", valueline5(data));

                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text(graphdata.AlertMessage);
                    }
                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
                else {

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


                    svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "black");
                    var alertMessage = "#MainContent_lblAlert" + (i + 1);
                    $(alertMessage).text("No Test Running!");

                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
            }
            else if (graphdata.MachineNumber == 601) {  // For Machine Number 605
                if (data != null && data.length > 0) {
                    var valueline = d3.svg.line()
                .x(function (d) {
                    return x(d.DateTimes);
                })
                .y(function (d) {
                    return y(d.T_SP_605);
                });

                    var valueline2 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.T_PV_605);
                        });

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .on(graphdata.IsAlertRequired, function () { return tooltip.style("visibility", "visible"); })
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    data.forEach(function (d) {
                        d.DateTimes = parseDate(d.DateTimes);
                        d.T_SP_605 = +d.T_SP_605;
                        d.T_PV_605 = +d.T_PV_605;
                    });

                    // Scale the range of the data
                    x.domain(d3.extent(data, function (d) {
                        return d.DateTimes;
                    }));
                    y.domain([0, d3.max(data, function (d) {
                        return d.T_SP_605;
                    }) + 20]);



                    if (!graphdata.IsAlertRequired) {
                        svg.append("rect")
                       .attr("width", width)
                       .attr("height", height)
                       .attr("fill", "#072400");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "aqua")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "#70FF00")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text("");
                    } else {
                        svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "yellow");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "blue")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "red")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text(graphdata.AlertMessage);
                    }
                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
                else {
                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


                    svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "black");
                    var alertMessage = "#MainContent_lblAlert" + (i + 1);
                    $(alertMessage).text("No Test Running!");

                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }

            }
            else if (graphdata.MachineNumber == 703) {  // For Machine Number 605
                if (data != null && data.length > 0) {
                    var valueline = d3.svg.line()
                .x(function (d) {
                    return x(d.DateTimes);
                })
                .y(function (d) {
                    return y(d.T_SP_605);
                });

                    var valueline2 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.T_PV_605);
                        });

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .on(graphdata.IsAlertRequired, function () { return tooltip.style("visibility", "visible"); })
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    data.forEach(function (d) {
                        d.DateTimes = parseDate(d.DateTimes);
                        d.T_SP_605 = +d.T_SP_605;
                        d.T_PV_605 = +d.T_PV_605;
                    });

                    // Scale the range of the data
                    x.domain(d3.extent(data, function (d) {
                        return d.DateTimes;
                    }));
                    y.domain([0, d3.max(data, function (d) {
                        return d.T_SP_605;
                    }) + 20]);



                    if (!graphdata.IsAlertRequired) {
                        svg.append("rect")
                       .attr("width", width)
                       .attr("height", height)
                       .attr("fill", "#072400");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "aqua")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "#70FF00")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text("");
                    } else {
                        svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "yellow");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "blue")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "red")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text(graphdata.AlertMessage);
                    }
                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
                else {
                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


                    svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "black");
                    var alertMessage = "#MainContent_lblAlert" + (i + 1);
                    $(alertMessage).text("No Test Running!");

                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }

            }
            else if (graphdata.MachineNumber == 705) {  // For Machine Number 605
                if (data != null && data.length > 0) {
                    var valueline = d3.svg.line()
                .x(function (d) {
                    return x(d.DateTimes);
                })
                .y(function (d) {
                    return y(d.T_SP_605);
                });

                    var valueline2 = d3.svg.line()
                        .x(function (d) {
                            return x(d.DateTimes);
                        })
                        .y(function (d) {
                            return y(d.T_PV_605);
                        });

                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .on(graphdata.IsAlertRequired, function () { return tooltip.style("visibility", "visible"); })
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    data.forEach(function (d) {
                        d.DateTimes = parseDate(d.DateTimes);
                        d.T_SP_605 = +d.T_SP_605;
                        d.T_PV_605 = +d.T_PV_605;
                    });

                    // Scale the range of the data
                    x.domain(d3.extent(data, function (d) {
                        return d.DateTimes;
                    }));
                    y.domain([0, d3.max(data, function (d) {
                        return d.T_SP_605;
                    }) + 20]);


                    if (!graphdata.IsAlertRequired) {
                        svg.append("rect")
                       .attr("width", width)
                       .attr("height", height)
                       .attr("fill", "#072400");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "aqua")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "#70FF00")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text("");
                    } else {
                        svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "yellow");

                        svg.append("path") // Add the valueline path for set value data.
                            .style("stroke", "blue")
                            .attr("d", valueline(data));

                        svg.append("path")        // Add the valueline2 path for process value data.
                                .style("stroke", "red")
                                .attr("d", valueline2(data));
                        var alertMessage = "#MainContent_lblAlert" + (i + 1);
                        $(alertMessage).text(graphdata.AlertMessage);
                    }
                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }
                else {
                    var divName = "#visualisation" + (i + 1);
                    d3.select(divName).select("svg").remove();

                    var svg = d3.select(divName)
                        .append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                    svg.append("rect")
                        .attr("width", width)
                        .attr("height", height)
                        .attr("fill", "black");
                    var alertMessage = "#MainContent_lblAlert" + (i + 1);
                    $(alertMessage).text("No Test Running!");

                    svg.append("g") // Add the X Axis
                    .attr("class", "x axis")
                        .attr("transform", "translate(0," + height + ")")
                        .call(xAxis);

                    svg.append("g") // Add the Y Axis
                    .attr("class", "y axis")
                        .call(yAxis);
                }

            }

        }

        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Timer runat="server" ID="UpdateTimer" OnTick="UpdateTimer_Tick"></asp:Timer>

    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <Triggers>
            <asp:AsyncPostBackTrigger ControlID="UpdateTimer" EventName="Tick" />
        </Triggers>
        <ContentTemplate>
            <table id="graphTable" style="width: 100%; margin-top: -50px;" align="center">
                <tr>
                    <td>
                       <%-- <asp:LinkButton ID="lnkButton" runat="server" OnClick="lnkButton_Click">FullScreen</asp:LinkButton>
                    </td>
                    <td>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>

                </tr>
                <tr>
                    <td colspan="2">
                         <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                             <ContentTemplate>

                            
                        <asp:Button Text="Oven" BorderStyle="None" ID="Tab1" CssClass="Clicked" runat="server"
                            OnClick="Tab1_Click" />
                        <asp:Button Text="Oven" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server"
                            OnClick="Tab2_Click" />
                        <asp:Button Text="Oven" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
                            OnClick="Tab3_Click" />
                        <asp:Button Text="Chamber" BorderStyle="None" ID="Tab4" CssClass="Initial" runat="server"
                            OnClick="Tab4_Click" />
                        <asp:Button Text="Chamber" BorderStyle="None" ID="Tab5" CssClass="Initial" runat="server"
                            OnClick="Tab5_Click" />
                        <asp:Button Text="Chamber" BorderStyle="None" ID="Tab6" CssClass="Initial" runat="server"
                            OnClick="Tab6_Click" />
                                  </ContentTemplate>
                             </asp:UpdatePanel>
                        <asp:Label ID="lblScrollFrequency" runat="server" Height="20px" Text="Graph Refresh Frequency(seconds): " ForeColor="Black"></asp:Label>

                        <asp:DropDownList ID="ddlScrollFrequency" runat="server" Width="40px" Height="20px" AutoPostBack="true">
                            <asp:ListItem Value="1000">1</asp:ListItem>
                            <asp:ListItem Value="3000">3</asp:ListItem>
                            <asp:ListItem Value="5000" Selected="True">5</asp:ListItem>
                            <asp:ListItem Value="10000">10</asp:ListItem>
                            <asp:ListItem Value="15000">15</asp:ListItem>
                            <asp:ListItem Value="30000">30</asp:ListItem>
                            <asp:ListItem Value="60000">60</asp:ListItem>
                        </asp:DropDownList>

                        <asp:CheckBox ID="chkStayHere" Text="Stay on this page" runat="server" AutoPostBack="true" OnCheckedChanged="chkStayHere_CheckedChanged" />

                        <asp:Label ID="lblTimer" runat="server" Font-Bold="true" Style="margin-left: 180px"></asp:Label>
                        <asp:Label ID="lblTabChangeFrequency" runat="server" style="display:none"></asp:Label>
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td>
                                            <div class="graph1" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven1" runat="server" Text="Oven1:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName1" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue1" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue1" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblGraphTempProcessValue1" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue1" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblAlert1" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="Label3" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="Label1" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="Label2" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation1" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="graph2" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven2" runat="server" Text="Oven2:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName2" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue2" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue2" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblGraphTempProcessValue2" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue2" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                      <asp:Label ID="lblAlert2" runat="server" Text="" Font-Bold="true" ForeColor="Red"></asp:Label>
                                                            &nbsp;
                                                      <asp:Label ID="Label4" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="Label5" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="Label6" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation2" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>

                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td>
                                            <div class="graph3" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven3" runat="server" Text="Oven3:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName3" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue3" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue3" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblGraphTempProcessValue3" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue3" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblAlert3" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                            &nbsp;
                                                      <asp:Label ID="Label7" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="Label8" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="Label9" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation3" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="graph4" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven4" runat="server" Text="Oven4:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName4" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue4" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue4" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblGraphTempProcessValue4" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue4" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                      <asp:Label ID="lblAlert4" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                            &nbsp;
                                                      <asp:Label ID="Label10" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="Label11" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image8" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="Label12" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation4" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="View3" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td>
                                            <div class="graph5" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">

                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven5" runat="server" Text="Oven5:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName5" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue5" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue5" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                      <asp:Label ID="lblGraphTempProcessValue5" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue5" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                       <asp:Label ID="lblAlert5" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                            &nbsp;
                                                      <asp:Label ID="Label13" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="Label14" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image10" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="Label15" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation5" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="graph6" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">

                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven6" runat="server" Text="Oven6:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName6" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue6" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue6" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblGraphTempProcessValue6" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue6" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblAlert6" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>

                                                            &nbsp;
                                                      <asp:Label ID="Label16" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="Label17" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image12" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="Label18" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation6" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>

                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="View4" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td>
                                            <div class="graph7" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven7" runat="server" Text="Chamber1:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName7" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTMSetValue7" runat="server" Text="TM SP :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue7" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTASetValue7" runat="server" Text="TA SP :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTASetValue7" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempProcessValue7" runat="server" Text="T PV :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue7" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphHMSetValue7" runat="server" Text="HM SP :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphHMSetValue7" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphHASetValue7" runat="server" Text="HA SP :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphHASetValue7" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphHumidityProcessValue7" runat="server" Text="H PV :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphHumidityProcessValue7" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblAlert7" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>

                                                            &nbsp;
                                                      <%--<asp:Label ID="Label19" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image13" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="Label20" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image14" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="Label21" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation7" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="graph8" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven8" runat="server" Text="Chamber2:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName8" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue8" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue8" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblGraphTempProcessValue8" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue8" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblAlert8" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>

                                                            &nbsp;
                                                      <asp:Label ID="Label22" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image15" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="Label23" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image16" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="Label24" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation8" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>

                                </table>
                            </asp:View>
                            <asp:View ID="View5" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td>
                                            <div class="graph9" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">

                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven9" runat="server" Text="Chamber3:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName9" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue9" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue9" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempProcessValue9" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue9" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                      <asp:Label ID="lblAlert9" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>

                                                            &nbsp;
                                                    <asp:Label ID="Label25" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image17" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="Label26" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image18" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="Label27" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation9" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="graph10" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven10" runat="server" Text="Chamber4:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName10" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue10" runat="server" Text="T SP :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue10" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempProcessValue10" runat="server" Text="T PV :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue10" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphHumidityProcessValue10" runat="server" Text="H PV :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphHumidityProcessValue10" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblAlert10" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="Label28" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image19" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="Label29" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image20" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="Label30" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation10" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="View6" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td>
                                            <div class="graph11" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">

                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven11" runat="server" Text="Chamber5:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName11" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphCTempValue11" runat="server" Text="C Temp :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphCTempSetValue11" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphCHumidityValue11" runat="server" Text="C Humidity :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphCHumidityValue11" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphHTempValue11" runat="server" Text="H Temp :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphHTempValue11" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphWTempValue11" runat="server" Text="W Temp :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphWTempValue11" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblAlert11" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="Label36" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image21" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="Label37" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="Image22" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="Label38" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation11" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>


                                </table>
                            </asp:View>

                        </asp:MultiView>


                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>--%>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">

        <ContentTemplate>
            <div id="outerContainer">
                <table id="graphTable" style="width: 100%; margin-top: -50px;" align="center">
                <tr>
                    <%--<td>
                        <asp:LinkButton ID="lnkButton" runat="server" OnClick="lnkButton_Click">FullScreen</asp:LinkButton>
                    </td>--%>
                    <td>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>

                </tr>
                <tr>
                    <td colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:Button Text="Oven" BorderStyle="None" ID="Tab1" CssClass="Clicked" runat="server"
                                    OnClick="Tab1_Click" />
                                <asp:Button Text="Oven" BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server"
                                    OnClick="Tab2_Click" />
                                <asp:Button Text="Oven" BorderStyle="None" ID="Tab3" CssClass="Initial" runat="server"
                                    OnClick="Tab3_Click" />
                                <asp:Button Text="Chamber" BorderStyle="None" ID="Tab4" CssClass="Initial" runat="server"
                                    OnClick="Tab4_Click" />
                                <asp:Button Text="Chamber" BorderStyle="None" ID="Tab5" CssClass="Initial" runat="server"
                                    OnClick="Tab5_Click" />
                                <asp:Button Text="Chamber" BorderStyle="None" ID="Tab6" CssClass="Initial" runat="server"
                                    OnClick="Tab6_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:Label ID="lblScrollFrequency" runat="server" Height="20px" Text="Graph Refresh Frequency(seconds): " ForeColor="Black"></asp:Label>

                        <asp:DropDownList ID="ddlScrollFrequency" runat="server" Width="40px" Height="20px" AutoPostBack="true">

                            <asp:ListItem Value="5000" Selected="True">5</asp:ListItem>
                            <asp:ListItem Value="10000">10</asp:ListItem>
                            <asp:ListItem Value="15000">15</asp:ListItem>
                            <asp:ListItem Value="30000">30</asp:ListItem>
                            <asp:ListItem Value="60000">60</asp:ListItem>
                        </asp:DropDownList>

                        <asp:CheckBox ID="chkStayHere" Text="Stay on this page" runat="server" AutoPostBack="true" OnCheckedChanged="chkStayHere_CheckedChanged" />

                        <asp:Label ID="lblTimer" runat="server" Font-Bold="true" Style="margin-left: 180px"></asp:Label>
                        <asp:Label ID="lblTabChangeFrequency" runat="server" style="display:none"></asp:Label>
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td>
                                            <div class="graph1" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven1" runat="server" Text="Oven1:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName1" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue1" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue1" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblGraphTempProcessValue1" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue1" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblAlert1" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblLegend1" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgGreenImage1" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="lblProcessValue1" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgRedImage1" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="lblSetValue1" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation1" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="graph2" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven2" runat="server" Text="Oven2:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName2" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue2" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue2" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblGraphTempProcessValue2" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue2" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                      <asp:Label ID="lblAlert2" runat="server" Text="" Font-Bold="true" ForeColor="Red"></asp:Label>
                                                            &nbsp;
                                                      <asp:Label ID="lblLegend2" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgGreenImage2" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="lblProcessValue2" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgRedImage2" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="lblSetValue2" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation2" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>

                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td>
                                            <div class="graph3" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven3" runat="server" Text="Oven3:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName3" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue3" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue3" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblGraphTempProcessValue3" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue3" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblAlert3" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                            &nbsp;
                                                      <asp:Label ID="lblLegend3" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgGreenImage3" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="lblProcessValue3" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgRedImage3" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="lblSetValue3" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation3" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="graph4" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven4" runat="server" Text="Oven4:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName4" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue4" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue4" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblGraphTempProcessValue4" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue4" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                      <asp:Label ID="lblAlert4" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                            &nbsp;
                                                      <asp:Label ID="lblLegend4" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgGreenImage4" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="lblProcessValue4" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgRedImage4" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="lblSetValue4" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation4" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="View3" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td>
                                            <div class="graph5" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">

                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven5" runat="server" Text="Oven5:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName5" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue5" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue5" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                      <asp:Label ID="lblGraphTempProcessValue5" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue5" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                       <asp:Label ID="lblAlert5" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                            &nbsp;
                                                      <asp:Label ID="lblLegend5" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgGreenImage5" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="lblProcessValue5" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgRedImage5" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="lblSetValue5" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation5" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="graph6" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">

                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven6" runat="server" Text="Oven6:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName6" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue6" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue6" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblGraphTempProcessValue6" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue6" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblAlert6" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>

                                                            &nbsp;
                                                      <asp:Label ID="lblLegend6" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgGreenImage6" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="lblProcessValue6" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgRedImage6" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="lblSetValue6" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation6" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>

                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="View4" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td>
                                            <div class="graph7" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven7" runat="server" Text="Chamber1:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName7" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTMSetValue7" runat="server" Text="TM SP :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue7" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTASetValue7" runat="server" Text="TA SP :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTASetValue7" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempProcessValue7" runat="server" Text="T PV :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue7" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphHMSetValue7" runat="server" Text="HM SP :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphHMSetValue7" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphHASetValue7" runat="server" Text="HA SP :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphHASetValue7" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphHumidityProcessValue7" runat="server" Text="H PV :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphHumidityProcessValue7" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblAlert7" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>

                                                            &nbsp;
                                                      <%--<asp:Label ID="lblLegend7" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgGreenImage7" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="lblProcessValue7" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgRedImage7" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="lblSetValue7" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation7" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="graph8" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven8" runat="server" Text="Chamber2:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName8" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue8" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue8" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblGraphTempProcessValue8" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue8" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblAlert8" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>

                                                            &nbsp;
                                                      <asp:Label ID="lblLegend8" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgGreenImage8" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="lblProcessValue8" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgRedImage8" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="lblSetValue8" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation8" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>

                                </table>
                            </asp:View>
                            <asp:View ID="View5" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td>
                                            <div class="graph9" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">

                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven9" runat="server" Text="Chamber3:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName9" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue9" runat="server" Text="Set Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue9" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempProcessValue9" runat="server" Text="Process Value :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue9" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                      <asp:Label ID="lblAlert9" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>

                                                            &nbsp;
                                                    <asp:Label ID="lblLegend9" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgGreenImage9" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="lblProcessValue9" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgRedImage9" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="lblSetValue9" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation9" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="graph10" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven10" runat="server" Text="Chamber4:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName10" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempSetValue10" runat="server" Text="T SP :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempSetValue10" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphTempProcessValue10" runat="server" Text="T PV :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphTempProcessValue10" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphHumidityProcessValue10" runat="server" Text="H PV :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphHumidityProcessValue10" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblAlert10" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblLegend10" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgGreenImage10" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="lblProcessValue10" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgRedImage10" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="lblSetValue10" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation10" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>
                                    </tr>
                                </table>
                            </asp:View>
                            <asp:View ID="View6" runat="server">
                                <table style="width: 100%; border-width: 1px; border-color: #666; border-style: solid">
                                    <tr>
                                        <td>
                                            <div class="graph11" style="width: 100%; margin-left: 20px">
                                                <table style="width: 100%">

                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblOven11" runat="server" Text="Chamber5:" Font-Bold="True"></asp:Label>
                                                            <asp:Label ID="lblOvenName11" runat="server"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphCTempValue11" runat="server" Text="C Temp :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphCTempSetValue11" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphCHumidityValue11" runat="server" Text="C Humidity :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphCHumidityValue11" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphHTempValue11" runat="server" Text="H Temp :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphHTempValue11" runat="server" BackColor="Black" Width="2%" ForeColor="Green" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                    <asp:Label ID="lblGraphWTempValue11" runat="server" Text="W Temp :" Font-Bold="True"></asp:Label>
                                                            <asp:TextBox ID="txtGraphWTempValue11" runat="server" BackColor="Black" Width="2%" ForeColor="Red" Enabled="false"></asp:TextBox>
                                                            &nbsp;
                                                     <asp:Label ID="lblAlert11" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                            &nbsp;
                                                    <asp:Label ID="lblLegend11" runat="server" Text="Legend:" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgGreenImage11" runat="server" ImageUrl="~/Images/Grenn.png" />
                                                            <asp:Label ID="lblProcessValue11" runat="server" Text="Process Value" Font-Bold="True"></asp:Label>
                                                            <asp:Image ID="imgRedImage11" runat="server" ImageUrl="~/Images/Red.png" />
                                                            <asp:Label ID="lblSetValue11" runat="server" Text="Set Value" Font-Bold="True"></asp:Label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">
                                                            <svg id="visualisation11" width="1000" height="220"></svg>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>


                                </table>
                            </asp:View>
                        </asp:MultiView>
                    </td>
                </tr>
            </table>
            </div>            
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
