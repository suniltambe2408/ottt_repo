﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   CommonFunctions
Description     :   This class has functions which are commonly used across the project
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Data;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using OTTTBLL;
using OTTTBO;

#endregion " Additional Namespaces "

namespace TTTWeb
{
    public partial class Dashboard : System.Web.UI.Page
    {

        #region " Class Level Variable "

        int iDBOperationCode = 0;
        TestBO oTestBO = null;
        DashboardBLL oDashboardBLL = null;
        List<TestBO> lstTestBO = null;
        TestBLL oTestBLL = null;

        #endregion " Class Level Variable "

        #region " Private Functions "

        #endregion " Private Functions "

        #region " Events "

        ///---------------------------------------------------------------------
        /// <summary>
        /// Loads page content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void Page_Load(object sender, EventArgs e)
        {
            oDashboardBLL = new DashboardBLL();
            try
            {
                lblDateTime.Text = System.DateTime.Now.ToString("MM-dd-yyyy h:mmtt");

                if (!Page.IsPostBack)
                {
                    gvRecentTestResult.DataSource = oDashboardBLL.GetAllRecentTest();
                    gvRecentTestResult.DataBind();

                    gvScheduledTests.DataSource = oDashboardBLL.GetAllScheduledTest();
                    gvScheduledTests.DataBind();

                    gvMachineStatus.DataSource = oDashboardBLL.GetAllMachineStatus();
                    gvMachineStatus.DataBind();

                }
            }
            catch (Exception ex)
            {
                lblMessageError.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessageError.ForeColor = Color.Red;
            }
            finally
            {
                oDashboardBLL = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event update test status Started to AboutToComplete before 10min from EndDateTime
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            oDashboardBLL = new DashboardBLL();

            try
            {

                oDashboardBLL.UpdateStatusToAboutToComplete();

                gvRecentTestResult.DataSource = oDashboardBLL.GetAllRecentTest();
                gvRecentTestResult.DataBind();

                gvScheduledTests.DataSource = oDashboardBLL.GetAllScheduledTest();
                gvScheduledTests.DataBind();

                gvMachineStatus.DataSource = oDashboardBLL.GetAllMachineStatus();
                gvMachineStatus.DataBind();


            }
            catch (Exception ex)
            {
                lblMessageError.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessageError.ForeColor = Color.Red;
            }
            finally
            {
                oDashboardBLL = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event handle the paging in RecentTestResult Test Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void gvRecentTestResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            oDashboardBLL = new DashboardBLL();

            try
            {
                gvRecentTestResult.PageIndex = e.NewPageIndex;
                gvRecentTestResult.DataSource = oDashboardBLL.GetAllRecentTest();
                gvRecentTestResult.DataBind();
            }
            catch (Exception ex)
            {
                lblMessageError.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessageError.ForeColor = Color.Red;
            }
            finally
            {
                oDashboardBLL = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event End test on Recent Test GridView based on TestID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void gvRecentTestResult_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            oDashboardBLL = new DashboardBLL();

            try
            {
                if (e.CommandName == "lnkEndTest")
                {
                    int TestID = Convert.ToInt32(e.CommandArgument);

                    iDBOperationCode = oDashboardBLL.UpdateStatusToComplete(TestID);

                    if (iDBOperationCode == 0)
                    {
                        gvRecentTestResult.DataSource = oDashboardBLL.GetAllRecentTest();
                        gvRecentTestResult.DataBind();

                        gvScheduledTests.DataSource = oDashboardBLL.GetAllScheduledTest();
                        gvScheduledTests.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessageError.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessageError.ForeColor = Color.Red;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        ///  This event change the status colour of Recent Test GridView based on Status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void gvRecentTestResult_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string status = Convert.ToString((e.Row.FindControl("lblStatus") as Label).Text);
                    DateTime current = System.DateTime.Now;
                    DateTime endtime = Convert.ToDateTime((e.Row.FindControl("lblEndDateTime") as Label).Text);

                    if (current > endtime && status == "AboutToComplete")
                    {
                        e.Row.BackColor = Color.FromArgb(250, 128, 114);
                    }
                    if (status == "Complete")
                    {
                        Button btnStartedTest = e.Row.FindControl("btnStartedTest") as Button;
                        btnStartedTest.BackColor = System.Drawing.Color.FromArgb(0, 183, 74);

                        LinkButton lnkEndTest = e.Row.FindControl("lnkEndTest") as LinkButton;
                        lnkEndTest.Enabled = false;

                    }
                    else if (status == "AboutToComplete")
                    {
                        Button btnStartedTest = e.Row.FindControl("btnStartedTest") as Button;
                        btnStartedTest.BackColor = Color.FromArgb(255, 255, 0);
                    }

                    else
                    {
                        Button btn = e.Row.FindControl("btnStartedTest") as Button;
                        btn.BackColor = Color.FromArgb(7, 163, 226);
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessageError.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessageError.ForeColor = Color.Red;
            }
            finally
            {

            }

        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event update test status Scheduled to Started
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void gvScheduledTests_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            oDashboardBLL = new DashboardBLL();
            lstTestBO = new List<TestBO>();
            oTestBLL = new TestBLL();

            try
            {
                if (e.CommandName == "lnkStartTest")
                {

                    int TestID = Convert.ToInt32(e.CommandArgument);

                    lstTestBO = oTestBLL.GetAllMachineNOByStatus(TestID);

                    Control ctrl = e.CommandSource as Control;
                    string MachineNumber = string.Empty;

                    if (ctrl != null)
                    {

                        GridViewRow _currenrtrow = ctrl.Parent.NamingContainer as GridViewRow;

                        //Now you can find control on the row where event is raised by using FindControl method.

                        Control findme = _currenrtrow.FindControl("lblMachineNO");
                        Label MachineNo = (Label)findme;
                        MachineNumber = MachineNo.Text;

                    }

                    //if (lstTestBO.Count == 0)
                    //{

                        iDBOperationCode = oDashboardBLL.UpdateStatusToStarted(TestID);
                        if (iDBOperationCode == 0)
                        {
                            gvRecentTestResult.DataSource = oDashboardBLL.GetAllRecentTest();
                            gvRecentTestResult.DataBind();

                            gvScheduledTests.DataSource = oDashboardBLL.GetAllScheduledTest();
                            gvScheduledTests.DataBind();
                        }

                    //}
                    //else
                    //{
                    //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('A Test is in progress in Machine Number " + MachineNumber + "')", true);
                    //}
                }

                else if (e.CommandName == "lnkDeleteTest")
                {
                    int TestID = Convert.ToInt32(e.CommandArgument);

                    iDBOperationCode = oDashboardBLL.StatusUpdateToDeleted(TestID);

                    if (iDBOperationCode == 0)
                    {
                        gvRecentTestResult.DataSource = oDashboardBLL.GetAllRecentTest();
                        gvRecentTestResult.DataBind();

                        gvScheduledTests.DataSource = oDashboardBLL.GetAllScheduledTest();
                        gvScheduledTests.DataBind();

                        lblMessage.Text = Resources.Resource.msgDeleteSuccess;
                        lblMessage.ForeColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oDashboardBLL = null;
            }

        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event create row editable in Scheduled Test Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void gvScheduledTests_RowEditing(object sender, GridViewEditEventArgs e)
        {
            oDashboardBLL = new DashboardBLL();

            try
            {
                gvScheduledTests.EditIndex = e.NewEditIndex;
                gvScheduledTests.DataSource = oDashboardBLL.GetAllScheduledTest();
                gvScheduledTests.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oDashboardBLL = null;
            }

        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event cancel editable row in Scheduled Test Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void gvScheduledTests_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            oDashboardBLL = new DashboardBLL();

            try
            {
                gvScheduledTests.EditIndex = -1;
                gvScheduledTests.DataSource = oDashboardBLL.GetAllScheduledTest();
                gvScheduledTests.DataBind();


            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oDashboardBLL = null;
            }

        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event update row based on TestID in Scheduled Test Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void gvScheduledTests_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            oTestBO = new TestBO();
            oDashboardBLL = new DashboardBLL();

            try
            {

                oTestBO.TestID = Convert.ToInt32(((Label)gvScheduledTests.Rows[e.RowIndex].FindControl("lblTestID")).Text);
                oTestBO.LIMSNO = Convert.ToString(((Label)gvScheduledTests.Rows[e.RowIndex].FindControl("lblScheduleLIMSNO")).Text);
                oTestBO.ComponentType = Convert.ToString(((TextBox)gvScheduledTests.Rows[e.RowIndex].FindControl("txtScheduleComponentType")).Text);
                oTestBO.TestName = Convert.ToString(((TextBox)gvScheduledTests.Rows[e.RowIndex].FindControl("txtTestName")).Text);
                oTestBO.TestOwner = Convert.ToString(((TextBox)gvScheduledTests.Rows[e.RowIndex].FindControl("txtTestOwner")).Text);

                string[] date = Convert.ToString(((TextBox)gvScheduledTests.Rows[e.RowIndex].Cells[0].FindControl("txtStartDateTime")).Text).Split('/');
                //string time = Convert.ToString(((DropDownList)gvScheduledTests.Rows[e.RowIndex].FindControl("ddlStartTimes")).SelectedItem.Text);
                string StartTimeHH = Convert.ToString(((TextBox)gvScheduledTests.Rows[e.RowIndex].Cells[0].FindControl("txtStartTimeHrs")).Text);
                string StartTimeMM = Convert.ToString(((TextBox)gvScheduledTests.Rows[e.RowIndex].Cells[0].FindControl("txtStartTimeMins")).Text);
                string time = StartTimeHH + ":" + StartTimeMM + ":00";
                
                oTestBO.StartDateTime = Convert.ToDateTime(date[0] + "/" + date[1] + "/" + date[2].Substring(0, 4) + " " + time);

                //oTestBO.Duration = Convert.ToInt32(((TextBox)gvScheduledTests.Rows[e.RowIndex].FindControl("txtDuration")).Text);
                int durationHrs = Convert.ToInt32(((TextBox)gvScheduledTests.Rows[e.RowIndex].Cells[0].FindControl("gvtxtDurationHrs")).Text);
                int durationMins = Convert.ToInt32(((TextBox)gvScheduledTests.Rows[e.RowIndex].Cells[0].FindControl("gvtxtDurationMins")).Text);
                oTestBO.Duration = (durationHrs * 60) + durationMins;
                oTestBO.EndDateTime = oTestBO.StartDateTime.AddHours(durationHrs);
                oTestBO.EndDateTime = oTestBO.EndDateTime.AddMinutes(durationMins);
                oTestBO.MachineID = Convert.ToInt32(((DropDownList)gvScheduledTests.Rows[e.RowIndex].FindControl("ddlMachineNumber")).SelectedValue);

                iDBOperationCode = oDashboardBLL.UpdateScheduledTest(oTestBO);

                if (iDBOperationCode == 0)
                {

                    gvScheduledTests.EditIndex = -1;
                    gvScheduledTests.DataSource = oDashboardBLL.GetAllScheduledTest();
                    gvScheduledTests.DataBind();
                    lblMessage.Text = Resources.Resource.msgSaveSuccess;                    
                    lblMessage.ForeColor = Color.Green;
                    lblMessage.Visible = true;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Saved successfully!')", true);
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Visible = true;
            }
            finally
            {
                oTestBO = null;
                oDashboardBLL = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event handle the paging in Scheduled Test Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void gvScheduledTests_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            oDashboardBLL = new DashboardBLL();

            try
            {
                gvScheduledTests.PageIndex = e.NewPageIndex;
                gvScheduledTests.DataSource = oDashboardBLL.GetAllScheduledTest();
                gvScheduledTests.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oDashboardBLL = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        ///  This event used to bind dropdown in Gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void gvScheduledTests_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            oTestBLL = new TestBLL();
            Label lblStartTime = new Label();
            DropDownList ddlStartTimes = new DropDownList();

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList ddlMachineNumber = (DropDownList)e.Row.FindControl("ddlMachineNumber");
                    if (ddlMachineNumber != null)
                    {
                        ddlMachineNumber.DataSource = null;
                        ddlMachineNumber.DataSource = oTestBLL.GetAllMachineName();
                        ddlMachineNumber.DataValueField = "MachineID";
                        ddlMachineNumber.DataTextField = "Machine";
                        ddlMachineNumber.DataBind();

                        Label lblMachineNumber = (Label)e.Row.FindControl("lblMachineNumber");
                        if (lblMachineNumber.Text != null)
                        {
                            //ddlMachineNumber.SelectedValue = lblMachineNumber.Text;
                            ddlMachineNumber.SelectedIndex = ddlMachineNumber.Items.IndexOf(ddlMachineNumber.Items.FindByText(lblMachineNumber.Text));
                        }
                    }

                    ddlStartTimes = (DropDownList)e.Row.FindControl("ddlStartTimes");
                    if (ddlStartTimes != null)
                    {
                        lblStartTime = (Label)e.Row.FindControl("lblStartTime");
                        if (lblStartTime.Text != null)
                        {
                            ddlStartTimes.SelectedItem.Text = ddlStartTimes.Items.FindByText(lblStartTime.Text).ToString();
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oTestBLL = null;

            }

        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event handle the paging in Machine Status GridView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void gvMachineStatus_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            oDashboardBLL = new DashboardBLL();

            try
            {
                gvMachineStatus.PageIndex = e.NewPageIndex;
                gvMachineStatus.DataSource = oDashboardBLL.GetAllMachineStatus();
                gvMachineStatus.DataBind();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Operation failed.')" + ex.Message, true);
            }
            finally
            {
                oDashboardBLL = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// This event change the status colour of Machine Status GridView based on Status
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void gvMachineStatus_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string status = Convert.ToString((e.Row.FindControl("lblStatus") as Label).Text);

                    Button btnInUSe = e.Row.FindControl("btnInUSe") as Button;

                    if (status == "In Use")
                    {
                        btnInUSe.BackColor = Color.FromArgb(7, 163, 226);
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Operation failed.')" + ex.Message, true);
            }
            finally
            {

            }
        }

        protected void txtStartTimeHrs_TextChanged(object sender, EventArgs e)
        {
            TextBox txtStartTimeHrs = sender as TextBox;
            if (txtStartTimeHrs.Text != "")
            {
                if (Convert.ToInt32(txtStartTimeHrs.Text) > 23 || Convert.ToInt32(txtStartTimeHrs.Text) < 0)
                {
                    lblMessage.Text = "Start time hours cannot be greater than 23 or less than 0";
                    lblMessage.ForeColor = Color.Red;
                    txtStartTimeHrs.Text = Convert.ToString(DateTime.Now.Hour);
                }
            }
            else
            {
                lblMessage.Text = "Start time hours cannot be blank";
                lblMessage.ForeColor = Color.Red;
                txtStartTimeHrs.Text = Convert.ToString(DateTime.Now.Hour);
            }
        }

        protected void txtStartTimeMins_TextChanged(object sender, EventArgs e)
        {
            TextBox txtStartTimeMins = sender as TextBox;
            if (txtStartTimeMins.Text != "")
            {
                if (Convert.ToInt32(txtStartTimeMins.Text) > 59 || Convert.ToInt32(txtStartTimeMins.Text) < 0)
                {
                    lblMessage.Text = "Start time minutes cannot be greater than 59 or less than 0";
                    lblMessage.ForeColor = Color.Red;
                    txtStartTimeMins.Text = Convert.ToString(DateTime.Now.Minute);
                }
            }
            else
            {
                lblMessage.Text = "Start time minutes cannot be blank";
                lblMessage.ForeColor = Color.Red;
                txtStartTimeMins.Text = Convert.ToString(DateTime.Now.Minute);
            }
        }

        #endregion " Events "

        protected void gvtxtDurationHrs_TextChanged(object sender, EventArgs e)
        {
            TextBox gvtxtDurationHrs = sender as TextBox;
            if (gvtxtDurationHrs.Text != "")
            {
                if (Convert.ToInt32(gvtxtDurationHrs.Text) < 0)
                {
                    lblMessage.Text = "Duration hours cannot be less than 0";
                    lblMessage.ForeColor = Color.Red;
                    gvtxtDurationHrs.Text = "01";
                }
            }
            else
            {
                lblMessage.Text = "Duration hours cannot be blank";
                lblMessage.ForeColor = Color.Red;
                gvtxtDurationHrs.Text = "01";
            }
        }

        protected void gvtxtDurationMins_TextChanged(object sender, EventArgs e)
        {
            TextBox gvtxtDurationMins = sender as TextBox;
            if (gvtxtDurationMins.Text != "")
            {
                int durationMins = Convert.ToInt32(gvtxtDurationMins.Text);
                if (durationMins > 59 || durationMins < 0)
                {
                    lblMessage.Text = "Duration minutes cannot be less than 0 or greater than 59";
                    lblMessage.ForeColor = Color.Red;
                    gvtxtDurationMins.Text = "00";
                }
            }
            else
            {
                lblMessage.Text = "Duration minutes cannot be blank";
                lblMessage.ForeColor = Color.Red;
                gvtxtDurationMins.Text = "00";
            }
        }
    }
}