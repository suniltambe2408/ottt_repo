﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/HomeMaster.Master" CodeBehind="TestTracking.aspx.cs" Inherits="OTTTWeb.TestTracking" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">
        .link_Under_line_Blue {
            text-decoration: none;
            color: blue;
        }

        .border {
            border: thin solid #ADD8E6;
        }
    </style>

    <script type="text/javascript">


        $(document).ready(function () {

            $('#<%=btnSave.ClientID %>').click(function () {

                $('#<%=lblTestNameError.ClientID %>').html("");
                $('#<%=lblTestOwnerError.ClientID %>').html("");
                $('#<%=lblComponentTypeError.ClientID %>').html("");
                $('#<%=lblTestConditionError.ClientID %>').html("");
                $('#<%=lblLIMSNOError.ClientID %>').html("");
                $('#<%=lblDepartmentError.ClientID %>').html("");
            });

            $('#<%=btnSearchLIMS.ClientID %>').click(function () {
                $('#<%=lblMessage.ClientID %>').html("");
            });

            $('#<%=txtStartDate.ClientID %>').blur(function () {
                $('#<%=lblStartDateError.ClientID %>').html("");
            });


        });

        function ClearStartDateTimeError() {
            $('#<%=lblMessage.ClientID %>').html("");
            $('#<%=lblStartDateError.ClientID %>').html("");
        }

        function ConfirmDelete() {

            if (confirm('Are you sure you want to delete this test ?') == true)
                return true;
            else
                return false;
        }

        function isName(evt) {
            if (evt != null) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if ((charCode >= 65 && charCode <= 90)
                    || (charCode >= 97 && charCode <= 122)
                    || (charCode >= 48 && charCode <= 57)
                    || (charCode == 45) || (charCode == 95)) {
                    return true;
                }

                else {

                    return false;
                }

            } else {

                return false;
            }
        }

        function isNumberQuantity(evt) {

            if (null != evt) {
                evt = (evt) ? evt : window.event;

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    if (charCode == 47) {
                        return true;
                    }
                    $('#<%=lblMessage.ClientID %>').html("Enter number only");
                    $('#<%=txtLimsNoRequired.ClientID %>').html("");



                    return false;
                }
                else {
                    $('#<%=lblMessage.ClientID %>').html("");
                }

            }
            return true;

        }




        function isAlphabet(evt) {
            if (null != evt) {
                evt = (evt) ? evt : window.event;

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {

                    return false;
                }

            }
            return true;
        }

        function isAlphabetWithSpace(evt) {
            if (null != evt) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {


                    if (charCode == 32) {
                        return true;
                    }
                    $('#<%=lblMessage.ClientID %>').html("Enter alphabets only");


                    return false;
                }
                else {
                    $('#<%=lblMessage.ClientID %>').html("");
                }

            }
            return true;
        }


        function isAlphabetWithSpaceTestOwner(evt) {
            if (null != evt) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {


                    if (charCode == 32) {
                        return true;
                    }
                    // $('#<%=lblTestOwnerError.ClientID %>').html("Please Enter Alphabets Only");


                    return false;
                }
                else {
                    //  $('#<%=lblTestOwnerError.ClientID %>').html("");
                }

            }
            return true;
        }

        function isAlphabetWithSpaceTestName(evt) {
            if (null != evt) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 48 || charCode > 57) && (charCode < 97 || charCode > 122)) {

                    if (charCode == 32) {
                        return true;
                    }
                    $('#<%=lblTestNameError.ClientID %>').html("Enter alpha-numeric value");


                    return false;
                }
                else {
                    $('#<%=lblTestNameError.ClientID %>').html("");
                }

            }
            return true;
        }

        function isAlphabetWithSpaceTestNames(evt) {
            if (null != evt) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 48 || charCode > 57) && (charCode < 97 || charCode > 122)) {


                    if (charCode == 32) {
                        return true;
                    }
                    $('#<%=lblTestNameError.ClientID %>').html("Enter alpha-numeric value");
                    $('#<%=txtTestNameRFV.ClientID %>').html("");


                    return false;
                }
                else {
                    $('#<%=lblTestNameError.ClientID %>').html("");
                }

            }
            return true;
        }

        function isAlphabetWithSpaceTestOwners(evt) {
            if (null != evt) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {


                    if (charCode == 32) {
                        return true;
                    }
                    $('#<%=lblTestOwnerError.ClientID %>').html("Enter alphabets only");
                    $('#<%=txtTestOwnerRFV.ClientID %>').html("");


                    return false;
                }
                else {
                    $('#<%=lblTestOwnerError.ClientID %>').html("");
                }

            }
            return true;
        }
        function isAlphabetWithSpaceComponentTypes(evt) {
            if (null != evt) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 48 || charCode > 57) && (charCode < 97 || charCode > 122)) {


                    if (charCode == 32) {
                        return true;
                    }
                    $('#<%=lblComponentTypeError.ClientID %>').html("Enter alpha-numeric value");
                    $('#<%=txtComponentTypeRFV.ClientID %>').html("");


                    return false;
                }
                else {
                    $('#<%=lblComponentTypeError.ClientID %>').html("");
                }

            }
            return true;
        }

        function isAlphabetWithSpaceDepartment(evt) {
            if (null != evt) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 48 || charCode > 57) && (charCode < 97 || charCode > 122)) {


                    if (charCode == 32) {
                        return true;
                    }
                    $('#<%=lblDepartmentError.ClientID %>').html("Enter alpha-numeric value");
                    $('#<%=txtDepartmentRFV.ClientID %>').html("");


                    return false;
                }
                else {
                    $('#<%=lblDepartmentError.ClientID %>').html("");
                }

            }
            return true;
        }

        function isNumberQuantityTestCondition(evt) {

            if (null != evt) {
                evt = (evt) ? evt : window.event;

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
                    if (charCode == 32) {
                        return true;
                    }
                    $('#<%=lblTestConditionError.ClientID %>').html("Enter number and alphabets only");

                    return false;
                }
                else {
                    $('#<%=lblTestConditionError.ClientID %>').html("");
                }

            }
            return true;

        }



        function onKeyDown(event) {

            if (null != event) {

                $('#<%=lblStartDateError.ClientID %>').html("Select date from calendar.");
                $('#<%=txtStartDateRFV.ClientID %>').html("");

                return false;
            }

        }

        function onKeyDownDate(event) {

            if (null != event) {

                $('#<%=lblMessage.ClientID %>').html("Select date from calendar.");
                return false;
            }
        }

        function checkDate(sender, args) {
            var yesterdayDate = new Date();
            //alert(yesterdayDate);
            yesterdayDate.setDate(yesterdayDate.getDate() - 1);
            // alert(yesterdayDate);
            if (sender._selectedDate < yesterdayDate) {
                alert("Start Date cannot be less than today's date!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }

        function checkValidHours(event) {
            var startHours = parseInt($('#<%=txtStartTimeHrs.ClientID %>').text());
            if (startHours > 23 || startHours < 0) {
                alert("Hour value cannot be less than 0 or greater than 23!");
                //$('#<%=txtStartTimeHrs.ClientID %>').text('')
                return false;
            } else {
                return true;
            }
        }

        function checkValidMinutes(event) {
            var startMins = parseInt($('#<%=txtStartTimeMins.ClientID %>').text());
            if (startMins > 59 || startMins < 0) {
                alert("Minute value cannot be less than 0 or greater than 59!");
                return false;
            } else {
                return true;
            }
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     
    <table style="margin-top: 30px; width: 100%">
        <tr align="right">
            <td>
                <asp:Image ID="imgPinIcon" runat="server" Width="20" Height="20" ImageUrl="~/Images/ico_mand.png" />
                <asp:Label ID="Label15" runat="server" Text="*" ForeColor="Red"></asp:Label>
                <asp:Label ID="lblManField" runat="server" Text="Mandatory Fields"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                 <asp:UpdatePanel ID="upValSummary" runat="server">
                                    <ContentTemplate>
                                        <asp:ValidationSummary ID="ValSumAppAccessRequ" runat="server"
                                            ValidationGroup="saveValidate" ForeColor="#FF3300"  />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSave" />
                                    </Triggers>
                                </asp:UpdatePanel>


            </td>
        </tr>
        <tr>
            <td align="center" style="width: 20%; background-color: #07538B; font-size: 20px; color: White; font-family: Calibri;">
                <b>Test Tracking</b>
            </td>
        </tr>
    </table>

    <asp:UpdatePanel ID="UpdateTemplate1" runat="server">
        <ContentTemplate>

            <div style="margin-top: 5px">
                <fieldset style="padding: 5px; width: 88%; margin-left: 7%; height: auto; border: thin solid #ADD8E6">

                    <legend style="font-size: small; color: #000000;"><strong>Search Test</strong> </legend>
                    <div align="center" style="height: 16px">
                        <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                        <asp:RequiredFieldValidator ID="txtLimsNoRequired" runat="server" ControlToValidate="txtLimsNo"
                            ForeColor="Red" ErrorMessage="Enter LIMS number" ValidationGroup="searchValidate" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>

                    <table style="margin-top: 5px; width: 100%;">
                        <tr>
                            <td style="width: 200px" align="right" class="border">
                                <asp:Label ID="lblLimsNo" runat="server" Text="LIMS Number"></asp:Label>
                                <asp:Label ID="lblLIMSNoStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td style="width: 3%;" class="border">
                                <asp:TextBox ID="txtLimsNo" runat="server" Width="160px" CssClass="uppercase" onkeypress="return isName(event)" MaxLength="20"></asp:TextBox>
                            </td>

                            <td style="width: 1%" align="left" class="border">
                                <asp:Image ID="imgLIMSNo" runat="server" Height="19px" ImageAlign="Middle"
                                    ImageUrl="~/Images/tooltip.png" ToolTip="Enter LIMS number. Example: 15_L_16_01280" />
                            </td>
                            <td style="width: 14%" align="center" class="border">

                                <asp:Button ID="btnSearchLIMS" runat="server" Text="Search" BackColor="#03518B"
                                    ForeColor="White" OnClick="btnSearchLIMS_Click" ValidationGroup="searchValidate" />

                                <asp:Button ID="btnClear" runat="server" Text="Clear" BackColor="#03518B"
                                    ForeColor="White" OnClick="btnClear_Click" />
                            </td>

                            <td style="width: 15%;" align="right" class="border">
                                <asp:Label ID="lblComponenttype" runat="server" Text="Component Type"></asp:Label>
                            </td>
                            <td align="left" class="border">
                                <asp:TextBox ID="txtComponentType" runat="server" Width="125px" MaxLength="2"
                                    onkeypress="return isAlphabetWithSpace(event)" Enabled="false"></asp:TextBox>
                            </td>
                            <td style="width: 3%" align="center" class="border">
                                <asp:Image ID="imgcomponenttype" runat="server" Height="19px" ImageAlign="Middle"
                                    ImageUrl="~/Images/tooltip.png" ToolTip="Component type" />
                            </td>
                            <td style="width: 15%;" align="right" class="border">
                                <asp:Label ID="lblTestOwner" runat="server" Text="Test Owner"></asp:Label>
                            </td>
                            <td align="left" class="border">
                                <asp:TextBox ID="txtTestOwner" runat="server" Width="125px" MaxLength="2"
                                    onkeypress="return isAlphabetWithSpace(event)" Enabled="false"></asp:TextBox>
                            </td>
                            <td style="width: 3%" align="center" class="border">
                                <asp:Image ID="imgTestOwnerName" runat="server" Height="19px" ImageAlign="Middle"
                                    ImageUrl="~/Images/tooltip.png" ToolTip="Test owner name" />
                            </td>

                        </tr>
                        <tr>
                            <td colspan="10" style="height: 15px">
                                <asp:Label ID="lblGridValidate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10" align="center">
                                <asp:Label ID="tempLabel" runat="server" Visible="false"></asp:Label>
                                <asp:GridView ID="gvTestInformation" runat="server" Width="100%" DataKeyNames="TestID" AutoGenerateColumns="false"
                                    OnRowUpdating="gvTestInformation_RowUpdating" OnRowCancelingEdit="gvTestInformation_RowCancelingEdit" AllowPaging="true" PageSize="4"
                                    OnRowEditing="gvTestInformation_RowEditing" OnRowCommand="gvTestInformation_RowCommand" OnRowDataBound="gvTestInformation_RowDataBound"
                                    CssClass="border" OnPageIndexChanging="gvTestInformation_PageIndexChanging">
                                    <%--  <AlternatingRowStyle BackColor="White" ForeColor="#284775" />--%>
                                    <EditRowStyle BackColor="White" />
                                    <FooterStyle BackColor="#5D7B9D" ForeColor="White" />
                                    <HeaderStyle BackColor="White" ForeColor="White" />
                                    <PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White" />
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="White" ForeColor="Black" />
                                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />

                                    <Columns>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Height="20px">
                                            <ItemStyle Width="100px" Height="20px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" AutoPostBack="true" runat="server" OnCheckedChanged="chkSelect_CheckedChanged" />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LIMS No" HeaderStyle-Height="20px">
                                            <ItemStyle Width="100px" Height="20px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblgvLIMSNo" runat="server" Text='<%# Eval("LIMSNO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Test ID" HeaderStyle-Height="20px">
                                            <ItemStyle Width="100px" Height="20px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblgvTestID" runat="server" Text='<%# Eval("UniqueTestID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Test Name" HeaderStyle-Height="20px">
                                            <ItemStyle Width="160px" Height="20px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestName" runat="server" Text='<%# Eval("TestName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Label ID="lblTestID" runat="server" Visible="false" Text='<%#Eval("TestID") %>'></asp:Label>
                                                <asp:TextBox ID="txtTestName" runat="server" Text='<%#Eval("TestName") %>' Width="130px" onkeypress="return isAlphabetWithSpace(event)"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvgvTestName" runat="server" ErrorMessage="*" ForeColor="Red" ValidationGroup="gvrfvValidate"
                                                    ControlToValidate="txtTestName"></asp:RequiredFieldValidator>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Test Condition">
                                            <ItemStyle Width="100px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTestCondition" runat="server" Text='<%# Eval("TestCondition") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTestCondition" runat="server" Text='<%# Eval("TestCondition") %>' Width="80px" CssClass="uppercase"
                                                    onkeypress="return isName(event)" MaxLength="15"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date & Time">
                                            <ItemStyle Width="250px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblStartDateTime" runat="server" Text='<%# Eval("StartDateTime") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtStartDateTime" runat="server" Text='<%# Eval("StartDate") %>' Width="70px" onkeypress="return onKeyDownDate(event)"
                                                    onblur="ClearStartDateTimeError();"></asp:TextBox>
                                                <asp:ImageButton ID="imgCalender" runat="server" ImageUrl="~/Images/calendar.jpg" />

                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server"
                                                    TargetControlID="txtStartDateTime" PopupButtonID="imgCalender" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate">
                                                </cc1:CalendarExtender>
                                                <asp:TextBox ID="gvtxtStartTimeHrs" runat="server" MaxLength="2" AutoPostBack="true" Text='<%# Eval("StartTimeHh") %>' OnTextChanged="gvtxtStartTimeHrs_TextChanged" Width="20px" TabIndex="9"></asp:TextBox>
                                                <asp:Label ID="lblHrMinSeperator" runat="server" Text=":"></asp:Label>
                                                <asp:TextBox ID="gvtxtStartTimeMins" runat="server" MaxLength="2" AutoPostBack="true" Text='<%# Eval("StartTimeMm") %>' OnTextChanged="gvtxtStartTimeMins_TextChanged" Width="20px" TabIndex="9"></asp:TextBox>

                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date & Time">
                                            <ItemStyle Width="200px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblEndDateTime" runat="server" Text='<%# Eval("EndDateTime") %>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Duration">
                                            <ItemStyle Width="80px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblDuration" runat="server" Text='<%# string.Concat( Eval("DurationHh"), ":", Eval("DurationMm"))%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="gvtxtDurationHrs" runat="server" MaxLength="4" Width="20px" Height="15px" AutoPostBack="true" OnTextChanged="gvtxtDurationHrs_TextChanged" Text='<%# Eval("DurationHh") %>'></asp:TextBox>
                                                <asp:Label ID="gvlblDurationHhMmSeperator" runat="server" Text=":"></asp:Label>
                                                <asp:TextBox ID="gvtxtDurationMins" runat="server" MaxLength="2" AutoPostBack="true" Height="15px" OnTextChanged="gvtxtDurationMins_TextChanged" Width="20px" Text='<%# Eval("DurationMm") %>'></asp:TextBox>
                                            </EditItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemStyle Width="80px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                            </ItemTemplate>


                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Machine NO">
                                            <ItemStyle Width="110px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblMachineNO" runat="server" Text='<%#Eval("Machine") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlMachineNumber" runat="server" Width="110px"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvgvMachineNumber" runat="server" ErrorMessage="*" ForeColor="Red" ValidationGroup="gvrfvValidate"
                                                    InitialValue="0" ControlToValidate="ddlMachineNumber"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lblMachineNumber" runat="server" Text='<%#Eval("MachineID") %>' Style="display: none"></asp:Label>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemStyle Width="150px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit"></asp:LinkButton>&nbsp;&nbsp;
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandName="lnkDelete" CommandArgument='<%#Eval("TestID") %>'
                                                    OnClientClick="return ConfirmDelete()"></asp:LinkButton>
                                            </ItemTemplate>
                                            <EditItemTemplate >
                                                <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CommandName="Update"
                                                    ToolTip="Update Record" ValidationGroup="gvrfvValidate"></asp:LinkButton>
                                                &nbsp;
                                                <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="Cancel"
                                                    ToolTip="Cancel Editing"></asp:LinkButton>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>

                                    <HeaderStyle BackColor="#5D7B9D" ForeColor="White" />
                                </asp:GridView>

                            </td>
                        </tr>
                    </table>
                    <div style="margin-left: 86%; margin-top: 5px;">
                        <asp:Button ID="btnTestAnalysisReport" runat="server" BackColor="#03518B"
                            ForeColor="White" Text="Test Analysis Report" OnClick="btnTestAnalysisReport_Click"  />
                    </div>
                </fieldset>
            </div>

            <div>

                <fieldset style="padding: 5px; width: 75%; margin: 2% 0 0 10%; height: 280px; border: thin solid #ADD8E6">
                    <legend style="font-size: small; color: #000000;"><strong>Add New Test</strong> </legend>
                    <div align="center" style="height: 20px">
                        <asp:Label ID="lblAddTestError" runat="server" ForeColor="Red"></asp:Label>
                    </div>

                    <table style="width: 100%; margin-left: 0%; border: thin solid #ADD8E6;" class="">
                     
                        <tr style="border-color: #ADD8E6" class="border">
                            <td colspan="8" width="20%" align="center" bgcolor="#07538B" style="font-size: 20px; color: White; font-family: Calibri;">New Test Details
                            </td>
                        </tr>
                        <tr class="border">
                            <td align="right" style="width: 130px" class="border">
                                <asp:Label ID="lblLIMSNumber" runat="server" Text="LIMS Number"></asp:Label>
                                <asp:Label ID="lblLIMSNumberStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td class="border">
                                <asp:TextBox ID="txtLimsNumber" runat="server" MaxLength="20" Width="160px" TabIndex="1" CssClass="uppercase" onkeypress="return isName(event)" OnTextChanged="txtLimsNumber_TextChanged" AutoPostBack="true"></asp:TextBox>
                            </td>
                            <td align="center" style="width: 30px" class="border">
                                <asp:Image ID="imgLims" runat="server" Height="19px" ImageAlign="Middle"
                                    ImageUrl="~/Images/tooltip.png" ToolTip="Enter LIMS number. Example: 15_L_16_01280" />
                            </td>
                            <td style="width: 170px" class="border">
                                <asp:RequiredFieldValidator ID="txtLIMSNORFV" runat="server"
                                     ErrorMessage="Enter LIMS number" Text="*"
                                    ControlToValidate="txtLimsNumber" ForeColor="Red" 
                                    ValidationGroup="saveValidate" ></asp:RequiredFieldValidator>
                                <asp:Label ID="lblLIMSNOError" runat="server" ForeColor="Red" ></asp:Label>
                            </td>
                            <td align="right" style="width: 130px" class="border">
                                <asp:Label ID="lblUniqueTestId" runat="server" Text="Test ID"></asp:Label>
                                <asp:Label ID="lblUniqueTestIdStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td class="border">
                                <asp:TextBox ID="txtUniqueTestId" runat="server" Width="160px" TabIndex="2" Enabled="false"></asp:TextBox>
                            </td>
                            <td align="center" style="width: 30px" class="border">
                                <asp:Image ID="imgTestId" runat="server" Height="19px" ImageAlign="Middle"
                                    ImageUrl="~/Images/tooltip.png" ToolTip="Unique ID for test. Auto populated field." />


                            </td>
                            <td style="width: 170px" class="border">
                                <asp:RequiredFieldValidator ID="txtUniqueTestIdRFV" runat="server" ErrorMessage="Enter test id"
                                    ForeColor="Red" ControlToValidate="txtUniqueTestId" ValidationGroup="saveValidate" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
                               
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="border">
                                <asp:Label ID="lblTestOwners" runat="server" Text="Test Owner"></asp:Label>
                                <asp:Label ID="lblTestOwnerStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td style="width: 160px" class="border">
                                <asp:TextBox ID="txtTestOwners" runat="server" MaxLength="20" onkeypress="return isAlphabetWithSpaceTestOwners(event)" Width="160px" TabIndex="3"></asp:TextBox>
                            </td>
                            <td align="center" style="width: 30px" class="border">
                                <asp:Image ID="imgTestOwner" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                    ToolTip="Enter test owner name. Example: Will Smith" />

                            </td>
                            <td class="border">
                                <asp:RequiredFieldValidator ID="txtTestOwnerRFV" runat="server" ErrorMessage="Enter test owner name"
                                    ForeColor="Red" ControlToValidate="txtTestOwners"
                                     ValidationGroup="saveValidate" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
                             
                                <asp:Label ID="lblTestOwnerError" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                            <td align="right" style="width: 130px" class="border">
                                <asp:Label ID="lblTestName" runat="server" Text="Test Name"></asp:Label>
                                <asp:Label ID="lblTestNameStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td class="border">
                                <asp:TextBox ID="txtTestName" runat="server" onkeypress="return isAlphabetWithSpaceTestNames(event)" MaxLength="100" Width="160px" TabIndex="2"></asp:TextBox>
                            </td>
                            <td align="center" style="width: 30px" class="border">
                                <asp:Image ID="imgTestName" runat="server" Height="19px" ImageAlign="Middle"
                                    ImageUrl="~/Images/tooltip.png" ToolTip="Enter test name. Example: Thermal Test" />
                            </td>
                            <td style="width: 170px" class="border">
                                <asp:RequiredFieldValidator ID="txtTestNameRFV" runat="server" ErrorMessage="Enter test name"
                                    ForeColor="Red" ControlToValidate="txtTestName"
                                    ValidationGroup="saveValidate" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
                                <asp:Label ID="lblTestNameError" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="border">
                                <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                                <asp:Label ID="lblDepartmentStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td class="border">
                                <asp:TextBox ID="txtDepartment" runat="server" onkeypress="return isAlphabetWithSpaceDepartment(event)" Width="160px" TabIndex="5"></asp:TextBox>
                            </td>
                            <td align="center" class="border">
                                <asp:Image ID="Image5" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                    ToolTip="Enter department name. Example: Quality" />

                            </td>
                            <td class="border">

                                <asp:RequiredFieldValidator ID="txtDepartmentRFV" runat="server" ErrorMessage="Enter department " ControlToValidate="txtDepartment"
                                    ForeColor="Red" ValidationGroup="saveValidate" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
                                <asp:Label ID="lblDepartmentError" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                            <td align="right" class="border">
                                <asp:Label ID="lblMachineNo" runat="server" Text="Machine Number"></asp:Label>
                                <asp:Label ID="lblMachineStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td class="border">
                                <asp:DropDownList ID="ddlMachineNo" runat="server" Width="165px" TabIndex="4">
                                </asp:DropDownList>
                            </td>
                            <td align="center" class="border">
                                <asp:Image ID="imgMachineNo" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                    ToolTip="Select machine number" />


                            </td>
                            <td class="border">
                                <asp:RequiredFieldValidator ID="ddlMachineNoRFV" runat="server" ErrorMessage="Select machine number" ControlToValidate="ddlMachineNo"
                                    ForeColor="Red" InitialValue="0" ValidationGroup="saveValidate" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
                            </td>


                        </tr>
                        <tr>
                            <td align="right" class="border">
                                <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center"></asp:Label>
                                <asp:Label ID="lblCostCenterStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td class="border">
                                <asp:DropDownList ID="ddlCostCenter" runat="server" Width="165px" TabIndex="8">
                                    <asp:ListItem Text="Select" Value="0"></asp:ListItem>

                                </asp:DropDownList>
                            </td>
                            <td align="center" class="border">
                                <asp:Image ID="Image6" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                    ToolTip="Select Cost Center" />


                            </td>
                            <td class="border">
                                <asp:RequiredFieldValidator ID="CostCenterRFV" runat="server" ErrorMessage="Select cost center" ControlToValidate="ddlCostCenter"
                                    ForeColor="Red" InitialValue="0" ValidationGroup="saveValidate" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
                            </td>

                            <td align="right" class="border">
                                <asp:Label ID="lblComponent" runat="server" Text="Component Type"></asp:Label>
                                <asp:Label ID="lblComponentStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td class="border">
                                <asp:TextBox ID="txtComponentTypes" runat="server" onkeypress="return isAlphabetWithSpaceComponentTypes(event)" Width="160px" TabIndex="5"></asp:TextBox>
                            </td>
                            <td align="center" class="border">
                                <asp:Image ID="imgComponent" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                    ToolTip="Enter component type. Example: Side Glass" />

                            </td>
                            <td class="border">

                                <asp:RequiredFieldValidator ID="txtComponentTypeRFV" runat="server" ErrorMessage="Enter component type" ControlToValidate="txtComponentTypes"
                                    ForeColor="Red" ValidationGroup="saveValidate" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
                                <asp:Label ID="lblComponentTypeError" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="border">
                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date"></asp:Label>
                                <asp:Label ID="lblStartDateStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td class="border">
                                <asp:TextBox ID="txtStartDate" runat="server" Width="140px" onkeypress="return onKeyDown(event)" TabIndex="7"></asp:TextBox>
                                <asp:ImageButton ID="imgCalender" runat="server" ImageUrl="~/Images/calendar.jpg" />


                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server"
                                    TargetControlID="txtStartDate" PopupButtonID="imgCalender" PopupPosition="BottomLeft" OnClientDateSelectionChanged="checkDate">
                                </cc1:CalendarExtender>
                            </td>
                            <td align="center" class="border">
                                <asp:Image ID="Image2" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                    ToolTip="Select start date from calendar" />
                            </td>
                            <td class="border">
                                <asp:RequiredFieldValidator ID="txtStartDateRFV" runat="server" ErrorMessage="Select start date" ControlToValidate="txtStartDate"
                                    ForeColor="Red" ValidationGroup="saveValidate" Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
                                <asp:Label ID="lblStartDateError" runat="server" ForeColor="Red" ></asp:Label>
                            </td>
                            <td align="right" class="border">
                                <asp:Label ID="lblStartTime" runat="server" Text="Start Time"></asp:Label>
                                <asp:Label ID="lblStartTimeStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td class="border">

                                <asp:TextBox ID="txtStartTimeHrs" runat="server" MaxLength="2" AutoPostBack="true" OnTextChanged="txtStartTimeHrs_TextChanged" Width="35px" TabIndex="9"></asp:TextBox>
                                <asp:Label ID="lblHrMinSeperator" runat="server" Text=":"></asp:Label>
                                <asp:TextBox ID="txtStartTimeMins" runat="server" MaxLength="2" AutoPostBack="true" OnTextChanged="txtStartTimeMins_TextChanged" Width="35px" TabIndex="9"></asp:TextBox>
                                <asp:Label ID="lblStartTimeAmPm" runat="server" Text=":"></asp:Label>
                                <asp:DropDownList ID="ddlStartTimeAmPm" runat="server" Width="60px" TabIndex="4">
                                    <asp:ListItem>AM</asp:ListItem>
                                    <asp:ListItem>PM</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="center" class="border">
                                <asp:Image ID="Image1" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                    ToolTip="Select start time" />


                            </td>
                            <td class="border">
                                <%--<asp:RequiredFieldValidator ID="txtStartTimeHrsRFV" runat="server" ErrorMessage="Enter hours" ControlToValidate="txtStartTimeHrs"
                                    ForeColor="Red" ValidationGroup="saveValidate" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="txtStartTimeMinsRFV" runat="server" ErrorMessage="Enter minutes" ControlToValidate="txtStartTimeMins"
                                    ForeColor="Red" ValidationGroup="saveValidate" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="border">
                                <asp:Label ID="lblDuration" runat="server" Text="Duration (HH:MM)"></asp:Label>
                                <asp:Label ID="lblDurationStar" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td class="border">
                                <%--<asp:TextBox ID="txtDuration" runat="server" MaxLength="3" onkeypress="return isNumberQuantityDuration(event)" Width="160px" TabIndex="9"></asp:TextBox>--%>
                                <asp:TextBox ID="txtDurationHrs" runat="server" MaxLength="4" AutoPostBack="true" OnTextChanged="txtDurationHrs_TextChanged" Width="35px" TabIndex="9"></asp:TextBox>
                                <asp:Label ID="lblDurationHhMmSeperator" runat="server" Text=":"></asp:Label>
                                <asp:TextBox ID="txtDurationMins" runat="server" MaxLength="2" AutoPostBack="true" OnTextChanged="txtDurationMins_TextChanged" Width="35px" TabIndex="9"></asp:TextBox>
                            </td>
                            <td align="center" class="border">
                                <asp:Image ID="Image3" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                    ToolTip="Enter duration in hh:mm" />
                            </td>
                            <td class="border"></td>
                            <td align="right" class="border">
                                <asp:Label ID="lblTestCondition" runat="server" Text="Test Condition"></asp:Label>
                            </td>
                            <td class="border">
                                <asp:TextBox ID="txtTestCondition" runat="server" CssClass="uppercase" onkeypress="return isName(event)" MaxLength="15" Width="160px" TabIndex="6"></asp:TextBox>
                            </td>
                            <td align="center" class="border">
                                <asp:Image ID="Image4" runat="server" Height="19px" ImageAlign="Middle" ImageUrl="~/Images/tooltip.png"
                                    ToolTip="Enter test condition. Example: LW250" />
                            </td>
                            <td class="border">
                                <asp:Label ID="lblTestConditionError" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr align="right" class="border" colspan="2">

                            <td colspan="8" align="center" class="border">
                                <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="#03518B" ForeColor="White" ValidationGroup="saveValidate" OnClick="btnSave_Click" />
                                <asp:Button ID="btnClearFields" runat="server" Text="Clear" BackColor="#03518B" ForeColor="White" OnClick="btnClearFields_Click" />
                            </td>
                        </tr>

                    </table>

                </fieldset>
                <asp:Label ID="lblTestID" runat="server" Visible="false"></asp:Label>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearchLIMS" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
