﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="OTTTWeb.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <table width="100%" style="margin: 50px 50px 0 40px">


        <tr>

            <td>
                <asp:Panel ID="Panel1" runat="server" Font-Bold="False"
                     Width="90%">
                    <table width="100%">

                        <tr>
                            <td width="60%" align="justify">
                                <p style="padding-left: 20px; padding-right: 10px">
                                    Online Test Tracking Tool (OTTT) is an intranet application for tracking the progress of tests being performed in Central Lab(CL). 
                                    OTTT is developed with the aim of utilizing data stored by SCADA application, to generate real-time graphs for live test monitoring and track the progress of tests and generate reports for those tests as and when required by user.  

                                </p>
                                <p style="padding-top: 10px; padding-left: 20px; padding-right: 10px">
                                    VWIPL developed SCADA, an application for storing test data, i.e., Set Value (SV) & Process Value (PV), which is connected directly to the test machines i.e., Chambers & Ovens, located in different Test Rooms. 
                                    The data stored by SCADA will be used by OTTT to generate graphs and display those on a 40 inch LCD Screen in Central Lab. 
                                    The graphs will display alerts when the Chamber or Oven stops sending data or when the Process Value (PV) goes beyond Set Value (SV) of the machine. 
                                    This will make Test Tracking fast and easy as User will not have to go the test room and check equipment time to time to assure that the test are running fine.
                                </p>
                               <p style="padding-top: 10px; padding-left: 20px; padding-right: 10px">
                                   OTTT also generates reports such as Test Analysis Report and Machine Tests Report. Test Analysis Report is a detailed report of a test with test details and data visualized in graph using a line chart. 
                                   This will help User in analyzing the trend of data during test. 
                                   Machine Test Report is report where User selects a Machine and gets the list of all test performed on the Machine till date. In this report, User has the provision to add filters such as Test Owner, Start Date / End Date, Department & test status to get list of specific tests.
                                </p>
                                 <br />
                                 <br />
                                 <br />
                                 <br />
                            </td>
                            <td></td>
                            <td align="right"  width="40%" height="30%" >
                                <img id="Img_home" src="Images/Lab image.jpg" width="100%"  />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>


            </td>
        </tr>
    </table>
</asp:Content>


