﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Web.Services;
using OTTTWeb.UserAuthenticationServiceRef;
using Common;
namespace OTTTWeb
{
    public partial class HomeMaster : System.Web.UI.MasterPage
    {
        static TraceUtility oTraceUtility = new TraceUtility();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                oTraceUtility.TraceService("HomeMaster.Master: Page_Load");

                //lblUserName.Text = Convert.ToString(Session["UserFristName"]); //Get another way to display user first name from chandan
                lblUserName.Text = Convert.ToString(Session["UserID"]);
                // lblUserName.Text = "Welcome!" + System.DirectoryServices.AccountManagement.UserPrincipal.Current.DisplayName;
                //oTraceUtility.TraceService("HomeMaster.Master: User First Name: " + Convert.ToString(Session["UserFristName"]));
                oTraceUtility.TraceService("HomeMaster.Master: User ID: " + Convert.ToString(Session["UserID"]));

                //Check if authorization is on. Must be set to "True" in web.config file on Production deployment
                if (Convert.ToBoolean(ConfigurationManager.AppSettings.Get("EnableWindowsAuthentication")))
                {
                    oTraceUtility.TraceService("HomeMaster.Master: EnableWindowsAuthentication is True in Web.config");

                    if (Convert.ToInt32(Session["IsValidUser"]) == 1)
                    {

                        oTraceUtility.TraceService("HomeMaster.Master: Page_Load: User is valid");

                        lnkNewUser.Visible = false;

                        oTraceUtility.TraceService("HomeMaster.Master: Page_Load:Start of enable/disable functionality according to role");

                        //Authorization - Removes "Admin" functionality if scanning operator has logged in

                        #region "BusinessAdmin"

                        if (Convert.ToInt32(Session["UserRoleID"]) == Convert.ToInt32(ConfigurationManager.AppSettings.Get("BusinessAdmin")))
                        {
                            //Shows all menu options available. This is just a placeholder
                        }
                        #endregion "BusinessAdmin"

                        #region "ITAdmin"

                        else if (Convert.ToInt32(Session["UserRoleID"]) == Convert.ToInt32(ConfigurationManager.AppSettings.Get("ITAdmin")))
                        {
                            NavigationMenu.Items.RemoveAt(1);

                        }
                        #endregion "ITAdmin"

                        #region "MaterialEnginner"

                        else if (Convert.ToInt32(Session["UserRoleID"]) == Convert.ToInt32(ConfigurationManager.AppSettings.Get("MaterialEnginner")))
                        {
                            //NavigationMenu.Items.RemoveAt(1);
                        }

                        #endregion "MaterialEnginner"

                        #region "Monitoring"

                        else if (Convert.ToInt32(Session["UserRoleID"]) == Convert.ToInt32(ConfigurationManager.AppSettings.Get("Monitoring")))
                        {
                            NavigationMenu.Items.RemoveAt(0);
                            NavigationMenu.Items.RemoveAt(1);
                            NavigationMenu.Items.RemoveAt(1);
                            NavigationMenu.Items.RemoveAt(1);
                            NavigationMenu.Items.RemoveAt(1);
                        }

                        #endregion "Monitoring"


                        oTraceUtility.TraceService("HomeMaster.Master: Page_Load: End of enable/disable functionality according to role");
                    }
                    else
                    {
                        NavigationMenu.Visible = false;
                        oTraceUtility.TraceService("HomeMaster.Master: User is not valid");
                    }
                }
                else
                {
                    if (Convert.ToInt32(Session["IsValidUser"]) == 1)
                    {

                        oTraceUtility.TraceService("HomeMaster.Master: Page_Load: User is valid");

                        lnkNewUser.Visible = false;

                        oTraceUtility.TraceService("HomeMaster.Master: Page_Load:Start of enable/disable functionality according to role");

                        //Authorization - Removes "Admin" functionality if scanning operator has logged in

                        #region "BusinessAdmin"

                        if (Convert.ToInt32(Session["UserRoleID"]) == Convert.ToInt32(ConfigurationManager.AppSettings.Get("BusinessAdmin")))
                        {
                            //Shows all menu options available. This is just a placeholder
                        }
                        #endregion "BusinessAdmin"

                        #region "ITAdmin"

                        else if (Convert.ToInt32(Session["UserRoleID"]) == Convert.ToInt32(ConfigurationManager.AppSettings.Get("ITAdmin")))
                        {
                            //NavigationMenu.Items.RemoveAt(1);
                        }
                        #endregion "ITAdmin"

                        #region "MaterialEnginner"

                        else if (Convert.ToInt32(Session["UserRoleID"]) == Convert.ToInt32(ConfigurationManager.AppSettings.Get("MaterialEnginner")))
                        {
                            //NavigationMenu.Items.RemoveAt(1);
                        }

                        #endregion "MaterialEnginner"

                        #region "Monitoring"

                        else if (Convert.ToInt32(Session["UserRoleID"]) == Convert.ToInt32(ConfigurationManager.AppSettings.Get("Monitoring")))
                        {
                            NavigationMenu.Items.RemoveAt(0);
                            NavigationMenu.Items.RemoveAt(1);
                            NavigationMenu.Items.RemoveAt(1);
                            NavigationMenu.Items.RemoveAt(1);
                            NavigationMenu.Items.RemoveAt(1);
                        }

                        #endregion "Monitoring"



                        oTraceUtility.TraceService("HomeMaster.Master: Page_Load: End of enable/disable functionality according to role");
                    }
                    else
                    {
                        NavigationMenu.Visible = false;
                        oTraceUtility.TraceService("HomeMaster.Master: User is not valid");
                    }
                    oTraceUtility.TraceService("HomeMaster.Master: EnableWindowsAuthentication is False in Web.config");
                    // NavigationMenu.Visible = false;
                }

                // NavigationMenu.Visible = false;
                lblFooter.Text = ConfigurationManager.AppSettings.Get("CopyrightMsg");
                lblSupportDetails.Text = string.Format("If you have queries or facing any error in this application, please email to <a href=mailto:{0}> Help Desk </a>(Phone: {1}) with application name, error description & screenshot", ConfigurationManager.AppSettings.Get("ApplicationAdminContactEmail"), ConfigurationManager.AppSettings.Get("ApplicationAdminContactMobile"));

            }
            catch (Exception ex)
            {
                //oTraceUtility.TraceService("HomeMaster.Master.cs - Page_Load Exception : " + ex.Message);
                //lblMessageHomeMaster.Text = Resources.Resource.lblCatchMsg + ex.Message;
                //lblMessageHomeMaster.ForeColor = Color.Red;
                throw ex;
            }
        }
        protected void lnkNewUser_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(ConfigurationManager.AppSettings.Get("UserRegitrationPagePath") + "?AppShortName=" + ConfigurationManager.AppSettings.Get("AppShortName"));
                //Context.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                //lblMessageHomeMaster.Text = Resources.Resource.lblCatchMsg + ex.Message;
                //lblMessageHomeMaster.ForeColor = Color.Red;
                throw ex;
            }
        }
        protected void lnkFeedback_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(ConfigurationManager.AppSettings.Get("FeedbackPagePath"));
            }
            catch (Exception ex)
            {
                //lblMessageHomeMaster.Text = Resources.Resource.lblCatchMsg + ex.Message;
                //lblMessageHomeMaster.ForeColor = Color.Red;
                throw ex;
            }
        }
    }
}