﻿
#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
#endregion

#region " Additional Namespaces "
using OTTTBLL;
using OTTTBO;
using System.Configuration;
using System.Drawing;
#endregion

namespace OTTTWeb.Reports
{
    public partial class TestAnalysisReport : System.Web.UI.Page
    {
        #region Class Level Variable

        string sTestID;
        TestTrackingBLL oTestTrackingBLL = null;

        string ReportURL = ConfigurationManager.AppSettings["ReportUrl"];
        string ReportPath = ConfigurationManager.AppSettings["TestAnalysisReport"];

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                oTestTrackingBLL = new TestTrackingBLL();
                if (!IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["LIMSNO"]))
                    {
                        string[] values = Convert.ToString(Request.QueryString["LIMSNO"]).Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            txtLIMSNo.Text = values[0];
                            sTestID = values[1];
                        }

                        ddlLimsID.Items.Clear();
                        ddlLimsID.DataSource = oTestTrackingBLL.SearchLimsData(Convert.ToString(txtLIMSNo.Text));
                        ddlLimsID.DataValueField = "UniqueTestID";
                        ddlLimsID.DataTextField = "UniqueTestID";
                        ddlLimsID.DataBind();
                        ddlLimsID.Items.Insert(0, new ListItem(" Select "));
                        ddlLimsID.Items[0].Value = "0";

                        if (!string.IsNullOrEmpty(Convert.ToString(sTestID)))
                        {
                            ddlLimsID.SelectedValue = Convert.ToString(sTestID);
                        }
                        else
                        {
                            ddlLimsID.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        ddlLimsID.Items.Insert(0, new ListItem(" Select "));
                        ddlLimsID.Items[0].Value = "0";
                        ddlLimsID.SelectedIndex = 0;
                    }
                    
                }


            }
            catch (Exception ex)
            {
                lblMessage.Text =  ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oTestTrackingBLL = null;
            }
        }

        protected void btnViewReport_Click(object sender, EventArgs e)
        {
          
            try
            {
                ReportParameter[] parameter = new ReportParameter[2];
               
                rsTestAnalysisReport.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                rsTestAnalysisReport.ServerReport.ReportServerUrl = new Uri(ReportURL);
                rsTestAnalysisReport.ServerReport.ReportPath = ReportPath;

                rsTestAnalysisReport.ServerReport.Refresh();

                parameter[0] = new ReportParameter("LIMSNumber", txtLIMSNo.Text);
                parameter[1] = new ReportParameter("UniqueTestID", ddlLimsID.SelectedValue);

                rsTestAnalysisReport.ServerReport.SetParameters(parameter);
                rsTestAnalysisReport.ShowParameterPrompts = false;
                rsTestAnalysisReport.ServerReport.Refresh();
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                rsTestAnalysisReport = null;
            }
        }

        #endregion Events

        protected void txtLIMSNo_TextChanged(object sender, EventArgs e)
        {
            oTestTrackingBLL = new TestTrackingBLL();
            try
            {
                if (!string.IsNullOrEmpty(txtLIMSNo.Text))
                {
                    List<TestBO> TestList = oTestTrackingBLL.GetTestIdListForLimsNumber(Convert.ToString(txtLIMSNo.Text));
                    if (TestList.Count > 0)
                    {
                        ddlLimsID.Items.Clear();
                        ddlLimsID.DataSource = TestList;
                        ddlLimsID.DataValueField = "UniqueTestID";
                        ddlLimsID.DataTextField = "UniqueTestID";
                        ddlLimsID.DataBind();
                        ddlLimsID.Items.Insert(0, new ListItem(" Select "));
                        ddlLimsID.Items[0].Value = "0";
                        ddlLimsID.SelectedIndex = 0;
                        ddlLimsID.Enabled = true;
                        lblMessage.Text = "";
                    }
                    else
                    {
                        ddlLimsID.Enabled = false;
                        lblMessage.Text = Resources.Resource.msgNoTestForLimsNumber;
                        lblMessage.ForeColor = Color.Red;
                    }
                    
                    
                }
                else
                {
                    lblMessage.Text = Resources.Resource.msgEnterLIMSNO;
                    lblMessage.ForeColor = Color.Red;
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oTestTrackingBLL = null;
            }



        }
    }
}