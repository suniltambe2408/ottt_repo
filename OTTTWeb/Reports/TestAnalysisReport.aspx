﻿<%@ Page Language="C#" MasterPageFile="~/HomeMaster.Master" AutoEventWireup="true" CodeBehind="TestAnalysisReport.aspx.cs" Inherits="OTTTWeb.Reports.TestAnalysisReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery-1.11.3.js" type="text/javascript"></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            var evt = null;
            isNumbeOnly(evt);

        });
        function isNumbeOnly(evt) {
            if (null != evt) {
                evt = (evt) ? evt : window.event;

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {

                    return false;
                }

            }
            return true;
        }
        function isName(evt) {
            if (evt != null) {

                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if ((charCode >= 65 && charCode <= 90)
                    || (charCode >= 97 && charCode <= 122)
                    || (charCode >= 48 && charCode <= 57)
                    || (charCode == 45) || (charCode == 95)) {
                    return true;
                }

                else {

                    return false;
                }

            } else {

                return false;
            }
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <fieldset style="margin-top: 40px">
        <legend>Test Analysis Report</legend>

        <table style="width: 100%">
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblMessage" runat="server" Height="12px" Text=""></asp:Label>
                     <asp:UpdatePanel ID="upValSummary" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValSumAppAccessRequ" runat="server"
                                ValidationGroup="AppAccessRequ" ForeColor="#FF3300" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnViewReport" />
                        </Triggers>
                    </asp:UpdatePanel>

                </td>
            </tr>
           
            <tr>
                <td style="width: 100px"  align="right">
                    <asp:Label ID="lblLimsNo" runat="server">LIMS Number</asp:Label>

                </td>
                <td align="left" style="width: 170px">
 
                    <asp:TextBox ID="txtLIMSNo" runat="server" 
                        onkeypress="return isName(event)" MaxLength="15" OnTextChanged="txtLIMSNo_TextChanged" AutoPostBack="true"></asp:TextBox>
             <asp:RequiredFieldValidator
                        ID="txtLIMSNoRFV" runat="server"
                        ErrorMessage="Please enter LIMS number" ControlToValidate="txtLIMSNo"
                        ValidationGroup="AppAccessRequ" Text="*"
                         Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                             </td>
              
                <td style="width: 50px" align="right" >
                    <asp:Label ID="lblLIMSID" runat="server">LIMS ID</asp:Label>
                </td>
                <td align="left" style="width:100px">
                    <asp:DropDownList ID="ddlLimsID" runat="server"></asp:DropDownList>
                     <asp:RequiredFieldValidator
                        ID="ddlLimsIRFV" runat="server"
                        ErrorMessage="Please select test id" ForeColor="Red" Text="*"
                        ValidationGroup="AppAccessRequ" ControlToValidate="ddlLimsID" InitialValue="0">
                    </asp:RequiredFieldValidator>
                </td>
             
            
                <td>
                    <asp:Button ID="btnViewReport" runat="server" Text="View Report" BackColor="#03518B" ValidationGroup="AppAccessRequ" ForeColor="White" OnClick="btnViewReport_Click" />
                </td>

            </tr>
        </table>


        <div style="width: 100%">

            <rsweb:ReportViewer ID="rsTestAnalysisReport" Width="1290px" Height="50%" runat="server"></rsweb:ReportViewer>

        </div>

    </fieldset>

</asp:Content>

