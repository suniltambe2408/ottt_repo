﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   <name of the class>
Description     :   <class description>
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
#endregion

#region " Additional Namespaces "
using OTTTBLL;
using OTTTBO;
using System.Configuration;
using System.Drawing;
#endregion

namespace OTTTWeb.Reports
{
    public partial class AllMachineTestReport : System.Web.UI.Page
    {
        #region Class Level Variable

        AllMachineReportBO oAllMachineReportBO = null;
        AllMachineTestReportBLL oAllMachineTestReportBLL = null;

        string ReportURL = ConfigurationManager.AppSettings["ReportUrl"];
        string ReportPath = ConfigurationManager.AppSettings["AllMachineTestReport"];

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            oAllMachineTestReportBLL = new AllMachineTestReportBLL();
            try
            {
                if (!IsPostBack)
                {
                    ddlMachineNo.Items.Clear();
                    ddlMachineNo.DataSource = oAllMachineTestReportBLL.GetDistinctMachineNumbers();
                    ddlMachineNo.DataValueField = "MachineID";
                    ddlMachineNo.DataTextField = "MachineNoLong";
                    ddlMachineNo.DataBind();
                    ddlMachineNo.SelectedIndex = 0;

                    ddlTestOwner.Items.Clear();
                    ddlTestOwner.DataSource = oAllMachineTestReportBLL.GetDistinctTestOwnerName();
                    ddlTestOwner.DataValueField = "TestOwner";
                    ddlTestOwner.DataTextField = "TestOwner";
                    ddlTestOwner.DataBind();
                    ddlTestOwner.Items.Insert(0, new ListItem(" Select ", "All"));
                    ddlTestOwner.Items[0].Value = "All";
                    ddlTestOwner.SelectedIndex = 0;

                    ddlDepartment.Items.Clear();
                    ddlDepartment.DataSource = oAllMachineTestReportBLL.GetDistinctDepartment();
                    ddlDepartment.DataValueField = "Department";
                    ddlDepartment.DataTextField = "Department";
                    ddlDepartment.DataBind();
                    ddlDepartment.Items.Insert(0, new ListItem(" Select ", "All"));
                    ddlDepartment.Items[0].Value = "All";
                    ddlDepartment.SelectedIndex = 0;

                    ddlStatus.Items.Clear();
                    ddlStatus.DataSource = oAllMachineTestReportBLL.GetDistinctMachineStatus();
                    ddlStatus.DataValueField = "Status";
                    ddlStatus.DataTextField = "Status";
                    ddlStatus.DataBind();
                    ddlStatus.Items.Insert(0, new ListItem(" Select ", "All"));
                    ddlStatus.Items[0].Value = "All";
                    ddlStatus.SelectedIndex = 0;
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
            }
        }

       

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                ReportParameter[] parameter = new ReportParameter[6];

                rvAllMachineTest.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                rvAllMachineTest.ServerReport.ReportServerUrl = new Uri(ReportURL);
                rvAllMachineTest.ServerReport.ReportPath = ReportPath;
                rvAllMachineTest.ServerReport.Refresh();

                parameter[0] = new ReportParameter("MachineID", ddlMachineNo.SelectedItem.Value);
                parameter[1] = new ReportParameter("FromDate", txtFromDate.Text); 
                parameter[2] = new ReportParameter("ToDate", txtToDate.Text);
                parameter[3] = new ReportParameter("TestOwner", ddlTestOwner.SelectedItem.Value);
                parameter[4] = new ReportParameter("Department", ddlDepartment.SelectedItem.Value);
                parameter[5] = new ReportParameter("Status", ddlStatus.SelectedItem.Value);

                rvAllMachineTest.ServerReport.SetParameters(parameter);
                rvAllMachineTest.ShowParameterPrompts = false;
                rvAllMachineTest.ServerReport.Refresh();
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                rvAllMachineTest = null;
            }
        }

        #endregion Events
    }
}