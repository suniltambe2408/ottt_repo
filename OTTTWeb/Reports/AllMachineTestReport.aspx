﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HomeMaster.Master" AutoEventWireup="true" CodeBehind="AllMachineTestReport.aspx.cs" Inherits="OTTTWeb.Reports.AllMachineTestReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery-1.11.3.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            var evt = null;
            isNumbeOnly(evt);

        });
        function isNumbeOnly(evt) {
            if (null != evt) {
                evt = (evt) ? evt : window.event;

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {

                    return false;
                }

            }
            return true;
        }

        function checkDate(sender, args) {
            if (sender._selectedDate < new Date()) {
                alert("From date must be greater than today!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
                $('#MainContent_txtToDate').Text = "";
            }
        }

        function checkEndDate(sender, args) {
            var fromDate = document.getElementById('<%= txtFromDate.ClientID %>').value;
            
            fromDate = new Date(fromDate);
            var nextDate = fromDate.getDate() + 1;
            
            if (sender._selectedDate < fromDate) {
                alert("To date must be greater than or equal to from date!");
                sender._selectedDate = new Date(fromDate.setDate(nextDate));
                // set the date back to the date after from date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
            }
        }

    </script>
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table width="100%" style="margin-top: 20px">
            <tr>
                <td align="center" style="height: 20px">
                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>

        <br />
        <fieldset class="fieldSet">
            <legend class="legend">All Tests on Machine Report</legend>
            <br />
            <table width="35%">
                <tr height="20px">
                    <td width="40%" align="right" style="padding-right: 10px">

                        <asp:Label ID="lblMachineNo" runat="server" Text="Machine No."
                            Font-Names="Segoe UI"></asp:Label>
                    </td>
                    <td align="left">
                        <%--<asp:TextBox ID="txtMachineno" runat="server" onkeypress="return isNumbeOnly(event)" MaxLength="10"></asp:TextBox>--%>
                        <asp:DropDownList ID="ddlMachineNo" runat="server"
                            Width="170px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="ddlMachinenoRFV" InitialValue="0" runat="server"
                            ErrorMessage="**" ForeColor="Red" ControlToValidate="ddlMachineNo" ValidationGroup="ValidateRFV"></asp:RequiredFieldValidator>

                    </td>
                </tr>




                <tr height="20px">
                    <td width="40%" align="right" style="padding-right: 10px">

                        <asp:Label ID="lblFromDate" runat="server" Text="From Date"
                            Font-Names="Segoe UI"></asp:Label>
                    </td>
                    <td colspan="2" align="left" width="35%">
                        <asp:TextBox ID="txtFromDate" runat="server" Width="70px"></asp:TextBox>
                        <asp:ImageButton ID="imgBtnFromDate" runat="server"
                            ImageUrl="~/Images/calendar.jpg"
                            CausesValidation="false" Width="16px" />

                        <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtFromDate"
                            Format="MM/dd/yyyy" PopupPosition="BottomLeft"
                            PopupButtonID="imgBtnFromDate">
                        </asp:CalendarExtender>
                       <%--  <asp:RequiredFieldValidator
                            ID="RequiredFieldValidator1"
                            runat="server"
                            ErrorMessage="enter date"
                            ControlToValidate="txtFromDate" SetFocusOnError="True" ForeColor="Red" 
                            ToolTip="From date is required for search" 
                            ValidationGroup="ValidateRFV">**</asp:RequiredFieldValidator>
                   
                            <asp:RegularExpressionValidator
                            ID="RegularExpressionValidator1"
                            runat="server"
                            ControlToValidate="txtFromDate"
                            ErrorMessage="Invalid date format"
                            ValidationExpression="\d\d/\d\d/\d{4}" SetFocusOnError="True" 
                            ForeColor="Red" ToolTip="Invalid date format" 
                            ValidationGroup="ValidateRFV">**</asp:RegularExpressionValidator>--%>

                    </td>

                     
                   
                </tr>
                <tr height="20px">
                       <td width="40%" align="right" style="padding-right: 10px">

                        <asp:Label ID="lblToDate" runat="server" Text="To Date"
                            Font-Names="Segoe UI"></asp:Label>
                    </td>
                    <td colspan="2" align="left" width="35%">
                        <asp:TextBox ID="txtToDate" runat="server" Width="70px"></asp:TextBox>
                        <asp:ImageButton ID="imgBtnToDate" runat="server"
                            ImageUrl="~/Images/calendar.jpg"
                            CausesValidation="false" Width="16px" />

                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtToDate"
                            Format="MM/dd/yyyy" PopupPosition="BottomLeft"
                            PopupButtonID="imgBtnToDate" OnClientDateSelectionChanged="checkEndDate">
                        </asp:CalendarExtender>
                         <%--  <asp:RequiredFieldValidator
                            ID="RequiredFieldValidator2"
                            runat="server"
                            ErrorMessage="enter date"
                            ControlToValidate="txtToDate" SetFocusOnError="True" ForeColor="Red" 
                            ToolTip="From date is required for search" 
                            ValidationGroup="ValidateRFV">**</asp:RequiredFieldValidator>
                   
                            <asp:RegularExpressionValidator
                            ID="RegularExpressionValidator2"
                            runat="server"
                            ControlToValidate="txtToDate"
                            ErrorMessage="Invalid date format"
                            ValidationExpression="\d\d/\d\d/\d{4}" SetFocusOnError="True" 
                            ForeColor="Red" ToolTip="Invalid date format" 
                            ValidationGroup="ValidateRFV">**</asp:RegularExpressionValidator>--%>
                    </td>
                 
                 </tr>
                <tr height="20px">
                    <td width="40%" align="right" style="padding-right: 10px">

                        <asp:Label ID="Label1" runat="server" Text="Test Owner Name"
                            Font-Names="Segoe UI"></asp:Label>
                    </td>
                    <td align="left">

                        <asp:DropDownList ID="ddlTestOwner" runat="server"
                            Width="170px">
                        </asp:DropDownList>

                    </td>



                </tr>
                <tr height="20px">

                    <td width="40%" align="right" style="padding-right: 10px">

                        <asp:Label ID="lblDepartment" runat="server" Text="Department"
                            Font-Names="Segoe UI"></asp:Label>
                    </td>
                    <td align="left">

                        <asp:DropDownList ID="ddlDepartment" runat="server"
                            Width="170px">
                        </asp:DropDownList>

                    </td>
                </tr>
                      <tr height="20px">

                    <td width="40%" align="right" style="padding-right: 10px">

                        <asp:Label ID="lblStatus" runat="server" Text="Status"
                            Font-Names="Segoe UI"></asp:Label>
                    </td>
                    <td align="left">

                        <asp:DropDownList ID="ddlStatus" runat="server"
                            Width="170px">
                        </asp:DropDownList>

                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td colspan="2" align="left">
                        <asp:Button ID="btnSubmit" runat="server" BackColor="#03518B" ForeColor="White"  Text="Search" ValidationGroup="ValidateRFV" OnClick="btnSubmit_Click" />
                    </td>
                    <td></td>
                </tr>
            </table>
            <br />
            <br />

           <div style="width: 100%">
            <rsweb:ReportViewer ID="rvAllMachineTest" runat="server" Width="1020px" Height="50%" ></rsweb:ReportViewer>
               </div>
            <br />
        </fieldset>
    </div>
</asp:Content>
