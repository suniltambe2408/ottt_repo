﻿#region " Class Description "
/*-------------------------------------------------------------------------------------------------------------------------
Class           :   CommonFunctions
Description     :   This class has functions which are commonly used across the project
Author          :    
Created By      :   <employee id>
Creation Date   :   <date in format DD.MM.YYYY> ex. 01.04.2014
Notes           :   <any important information like interdependency>

Modified Details:
Modified By                Modified Date               Modified Reason
<employee id>               DD.MM.YYYY                 <one or two line reason>
---------------------------------------------------------------------------------------------------------------------------*/
#endregion

#region " Primary Namespaces "

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

#endregion " Primary Namespaces "

#region " Additional Namespaces "

using OTTTBLL;
using OTTTBO;

#endregion " Additional Namespaces "

namespace TTTWeb
{
    public partial class MachineMaster : System.Web.UI.Page
    {

        #region " Class Level Variable "

        int iDBOperationCode = 0;
        MachineMasterBO oMachineMasterBO = null;
        MachineMasterBLL oMachineMasterBLL = null;
        List<MachineMasterBO> lstMachineMasterBO = null;

        #endregion " Class Level Variable "

        #region " Events "

        ///---------------------------------------------------------------------
        /// <summary>
        /// Loads page content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///---------------------------------------------------------------------

        protected void Page_Load(object sender, EventArgs e)
        {
            oMachineMasterBLL = new MachineMasterBLL();

            try
            {
                if (!Page.IsPostBack)
                {
                    gvMachineMaster.DataSource = oMachineMasterBLL.GetAllMachineDetails();
                    gvMachineMaster.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oMachineMasterBLL = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Insert MachineMaster Details
        /// </summary>
        /// <returns>DB values</returns>
        ///---------------------------------------------------------------------

        protected void btnMachineSave_Click(object sender, EventArgs e)
        {
            oMachineMasterBO = new MachineMasterBO();
            oMachineMasterBLL = new MachineMasterBLL();

            try
            {
                lblMachineNoError.Text = string.Empty;
                if (lblMachineID.Text == string.Empty)
                {
                    oMachineMasterBO.MachineID = 0;
                }
                else
                {
                    oMachineMasterBO.MachineID = Convert.ToInt32(lblMachineID.Text);
                }
                string MachineNumber = txtMachineNo.Text;
                oMachineMasterBO.MachineNO = Convert.ToInt32((txtMachineNo.Text).Substring(MachineNumber.Length - 3));
                oMachineMasterBO.MachineNoLong = (Convert.ToString(MachineNumber)).ToUpper().ToString();
                oMachineMasterBO.MachineName = Convert.ToString(txtMachineName.Text);
                oMachineMasterBO.MachineType = Convert.ToString(txtMachineType.Text);
                oMachineMasterBO.RoomName = Convert.ToString(txtRoomName.Text);
                oMachineMasterBO.IsActive = Convert.ToBoolean(chkIsActive.Checked);

                if (lblDuplidate.Text == string.Empty)
                {
                    iDBOperationCode = oMachineMasterBLL.SaveMachine(oMachineMasterBO);
                    lblMessage.Text = Resources.Resource.msgSaveSuccess;
                    lblMessage.ForeColor = Color.Green;
                    gvMachineMaster.DataSource = oMachineMasterBLL.GetAllMachineDetails();
                    gvMachineMaster.DataBind();
                    Clear();
                }
                else
                {
                    lblDuplidate.Text = Resources.Resource.msgDuplicateMachine;
                    lblDuplidate.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oMachineMasterBO = null;
                oMachineMasterBLL = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Select the row based on MachineID
        /// </summary>
        /// <returns>DB values</returns>
        ///---------------------------------------------------------------------

        protected void gvMachineMaster_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = lblDuplidate.Text = string.Empty;

                lblMachineID.Text = (gvMachineMaster.SelectedRow.FindControl("lblMachineID") as Label).Text;
                txtMachineNo.Text = (gvMachineMaster.SelectedRow.FindControl("lblMachineNO") as Label).Text;
                txtMachineName.Text = (gvMachineMaster.SelectedRow.FindControl("lblMachineName") as Label).Text;
                txtMachineType.Text = (gvMachineMaster.SelectedRow.FindControl("lblMachineType") as Label).Text;
                txtRoomName.Text = (gvMachineMaster.SelectedRow.FindControl("lblRoomName") as Label).Text;
                chkIsActive.Checked = (gvMachineMaster.SelectedRow.FindControl("gvIsActive") as CheckBox).Checked;

                foreach (GridViewRow row in gvMachineMaster.Rows)
                {
                    if (row.RowIndex == gvMachineMaster.SelectedIndex)
                    {
                        row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                    }
                    else
                    {
                        row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;

            }
            finally
            {

            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Clear all fields
        /// </summary>
        /// <returns>Textbox values</returns>
        ///---------------------------------------------------------------------

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                lblMessage.Text = string.Empty;
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {

            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Paging event
        /// </summary>
        /// <returns> </returns>
        ///---------------------------------------------------------------------

        protected void gvMachineMaster_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            oMachineMasterBLL = new MachineMasterBLL();

            try
            {
                gvMachineMaster.PageIndex = e.NewPageIndex;
                //gvMachineMaster.DataBind();

                gvMachineMaster.DataSource = oMachineMasterBLL.GetAllMachineDetails();
                gvMachineMaster.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oMachineMasterBLL = null;
            }
        }

        ///---------------------------------------------------------------------
        /// <summary>
        /// Search Machine based on Machine No
        /// </summary>
        /// <returns> </returns>
        ///---------------------------------------------------------------------

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            oMachineMasterBLL = new MachineMasterBLL();
            lstMachineMasterBO = new List<MachineMasterBO>();

            try
            {
                lstMachineMasterBO = oMachineMasterBLL.SearchMachine(Convert.ToString(txtSearch.Text));
                if (lstMachineMasterBO.Count != 0)
                {
                    gvMachineMaster.DataSource = lstMachineMasterBO;
                    gvMachineMaster.DataBind();
                }
                else
                {
                    lblMessage.Text = Resources.Resource.msgNoRecord;
                    lblMessage.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oMachineMasterBLL = null;
            }
        }

        #endregion " Events "

        #region " Public Functions "

        ///---------------------------------------------------------------------
        /// <summary>
        /// Clear all fields
        /// </summary>
        /// <returns>Textbox values</returns>
        ///---------------------------------------------------------------------

        public void Clear()
        {
            oMachineMasterBO = new MachineMasterBO();
            oMachineMasterBLL = new MachineMasterBLL();

            try
            {
                txtMachineNo.Text = txtMachineName.Text = txtMachineType.Text = txtRoomName.Text = txtSearch.Text = lblDuplidate.Text = string.Empty;
                chkIsActive.Checked = false;
                txtMachineNo.Focus();

                gvMachineMaster.DataSource = oMachineMasterBLL.GetAllMachineDetails();
                gvMachineMaster.DataBind();

                lblMachineID.Text = string.Empty;

            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oMachineMasterBO = null;
                oMachineMasterBLL = null;
            }
        }

        #endregion

        protected void txtMachineNo_TextChanged(object sender, EventArgs e)
        {
            oMachineMasterBLL = new MachineMasterBLL();
            lblMessage.Text = string.Empty;
            try
            {
                string MachineNoLong = Convert.ToString(txtMachineNo.Text);

                iDBOperationCode = oMachineMasterBLL.CheckDuplicateMachineNO(MachineNoLong);
               
                if (iDBOperationCode != 0)
                {
                    lblDuplidate.Text = Resources.Resource.msgDuplicateMachine;
                }
                else
                {
                    lblDuplidate.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = Resources.Resource.lblCatchMsg + ex.Message;
                lblMessage.ForeColor = Color.Red;
            }
            finally
            {
                oMachineMasterBLL = null;
            }
        }

    }
}