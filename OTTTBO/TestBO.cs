﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OTTTBO
{
    public class TestBO
    {
        public int TestID { get; set; }
        public string LIMSNO { get; set; }
        public string TestName { get; set; }
        public string TestOwner { get; set; }
        public string ComponentType { get; set; }
        public int MachineID { get; set; }
        public string TestCondition { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int Duration { get; set; }
        public string Status { get; set; }

        public string Machine { get; set; }
        public DateTime StartDate { get; set; }
        public string StartTime { get; set; }
        public string Department { get; set; }
        public int CostCenterID { get; set; }
        public string UniqueTestID { get; set; }
        public string DurationHh { get; set; }
        public string DurationMm { get; set; }
        public string StartTimeHh { get; set; }
        public string StartTimeMm { get; set; }
        public string StartTimeAmPm { get; set; }
    }
}
