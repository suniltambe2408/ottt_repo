﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OTTTBO
{
    public class MonitoringBO
    {
        public int MachineNumber { get; set; }
        public String MachineType { get; set; }
        public String TestName { get; set; }
        public DateTime TestStartTime { get; set; }
        public DateTime TestEndTime { get; set; }
        public String AlertMessage { get; set; }
        public bool IsAlertRequired { get; set; }
        public String MachineName { get; set; }
        public String MachineNoLong { get; set; }
        public List<MonitoringDataBO> lstMonitoringDataBO { get; set; }        
    }    
}
