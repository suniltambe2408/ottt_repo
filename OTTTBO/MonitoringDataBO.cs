﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OTTTBO
{
    public class MonitoringDataBO
    {
        public string DateTimes { get; set; }
        public double C_Temp_602 { get; set; }
        public double C_Humidity_602 { get; set; }
        public double H_Temp_602 { get; set; }
        public double W_Temp_602 { get; set; }
        public double T_SP_603 { get; set; }
        public double T_PV_603 { get; set; }
        public double T_SP_604 { get; set; }
        public double T_PV_604 { get; set; }
        public double T_SP_605 { get; set; }
        public double T_PV_605 { get; set; }
        public double T_SP_606 { get; set; }
        public double T_PV_606 { get; set; }
        public double T_SP_704 { get; set; }
        public double T_PV_704 { get; set; }
        public double T_PV_706 { get; set; }
        public double TM_SP_706 { get; set; }
        public double TA_SP_706 { get; set; }
        public double H_PV_706 { get; set; }
        public double HM_SP_706 { get; set; }
        public double HA_SP_706 { get; set; }
        public double T_SP_707 { get; set; }
        public double T_PV_707 { get; set; }
        public double H_PV_707 { get; set; }
    }
}
