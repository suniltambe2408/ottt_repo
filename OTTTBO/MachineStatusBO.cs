﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OTTTBO
{
    public class MachineStatusBO
    {
        public int MachineID { get; set; }
        public int MachineNO { get; set; }
        public string MachineName { get; set; }
        public string  Status { get; set; }
        public string AvailableDateTime { get; set; }
        public string MachineNoLong { get; set; }
    }
}
