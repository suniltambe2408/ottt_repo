﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OTTTBO
{
    public class MachineMasterBO
    {
        public int MachineID { get; set; }
        public int MachineNO { get; set; }
        public string MachineNoLong { get; set; }
        public string MachineName { get; set; }
        public string MachineType { get; set; }
        public string RoomName { get; set; }
        public bool IsActive { get; set; }

        public string Machine { get; set; }
    }
}
